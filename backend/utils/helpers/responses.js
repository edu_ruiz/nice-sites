const serverResponse = {
    sendSuccess: (res, message, data = null) => {
        const responseMessage = {
            code: message.code ? message.code : 500,
            success: message.success,
            message: message.message,
        };
        if (data) { responseMessage.data = data; }
        return res.status(message.code).json(responseMessage);
    },
    sendError: (res, error) => {

        const responseMessage = {
            code: error.code ? error.code : 500,
            success: false,
            message: error.message
        };

        return res.status(error.code ? error.code : 500).json(responseMessage); //message: error.message,
    },
    sendErrorList: (res, error, e) => {
        var str = '...';
        for (let key in e){
            str += e[key].properties.message + '..., ';
          }
        const responseMessage = {
            code: error.code ? error.code : 500,
            success: false,
            message: str,
            errors: e
        };
        //responseMessage.data.message = 'mal';
        //console.log(responseMessage)
        return res.status(error.code ? error.code : 500).json(responseMessage); //message: error.message,
    },
};

module.exports = serverResponse;