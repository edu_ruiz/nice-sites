/**
 * Created by Syed Afzal
 */
require("./config/config");

const express = require("express");
const session = require("express-session");
const path = require("path");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./db");

//const {detail} = require("./middleware/urlDetail")
//const {serverTime} = require("./middleware/serverTime");
//const {sanitizer} = require("./middleware/sanitizer")

const app = express();

    //connection from db here
db.connect(app);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(session({
  secret: '2C44-4D44-WppQ38S',
  resave: true,
  saveUninitialized: true
}));

//app.use(detail);
//app.use(serverTime);
//app.use(sanitizer);
//  adding routes
// protect !!, private, local
require("./routes/index")(app); // categories, labels, nav-links
require("./routes/contents")(app); // text-contents
require("./routes/seeder")(app);
require("./routes/auth")(app);
require("./routes/users")(app) // only develop, prod. to .gitignore
require("./routes/search")(app); // Feed ResourceList and MatchSuggest from SearchBar+Filters query Params
require("./routes/forms")(app); // label-attributes configs for build forms
require("./routes/resources")(app); // URL collection
require("./routes/wishes")(app); // Custom resource lists
require("./routes/ratings")(app); // URL collection
require("./routes/certificates")(app);
require("./routes/blacklists")(app);

// New clean structure
//require("./routes/index.n")(app);
//require("./routes/resources.routes")(app); // URL collection

// public
//require("./routes/api")(app); // public endpoints to get whitelists

app.on("ready", () => {
  app.listen(3000, () => {
    console.log("Server is up on port", 3000);
  });
});

module.exports = app;
