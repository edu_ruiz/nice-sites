const resourceRepository = require("../.del.repositories/ResourceRepository");


const getAllResources = async (filter) => {

    const resources = await resourceRepository.getAllResources(filter);

    if( ! resources ) {

        throw new Error( "No items found");
    }

    return resources;
}

const getAllVerifiedResources = async (filter) => {

    const resources = await resourceRepository.getAllVerifiedResources(filter);

    if( ! resources ) {

        throw new Error( "No items found");
    }

    return resources;
}

const getResourceById = async (id) => {

    const resource = await resourceRepository.getResourceById(id);

    if( ! resource ) {

        throw new Error( "No items matched");
    }

    return resource;
}

const createResource = async (data, userId) => {

    const resource = await resourceRepository.createResource(data);

    if( ! resource ) {

        throw new Error( "Error while creating resource process");
    }

    return resource;
}

const updateResource = async (id, data, userId) => {

    const resource = await resourceRepository.getResourceById(id);

    if( ! resource ) {

        throw new Error( "No items matched");
    }

    const updated = await resourceRepository.updateResource(id, data);

    return updated;

}

const deleteResource = async (id, userId) => {

    const resource = await resourceRepository.deleteResource(id);

    if( ! resource ) {

        throw new Error( "No items matched");
    }
    
    return resource;
}

modules.exports = {
    getAllResources,
    getAllVerifiedResources,
    getResourceById,
    createResource,
    updateResource,
    deleteResource
}