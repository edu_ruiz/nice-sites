const userRepository = require("../.del.repositories/UserRepository");


const getAllUsers = async (filter) => {

    const users = await userRepository.getAllUsers(filter);
}
const getUserById = async (id) => {}
const createUser = async (data) => {}
const deleteUser = async (id) => {}
const updateUser = async (id, data) => {}


modules.exports = {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}