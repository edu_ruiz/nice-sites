const resourceService = require("../services/ResourceService");
const serverResponses = require("../utils/helpers/responses");


const getAllResources = async (req, res) => {

    let filter = {};

    try {
        const resources = await resourceService.getAllResources(filter);

        serverResponses.sendSuccess(res, messages.SUCCESSFUL, resources);     
    }
    catch (err) {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, err.errors);
    }
}

const getAllVerifiedResources = async (req, res) => {

    // Sanear y validar prev en middlew en la ruta ?

    // Preparar objeto filtro y objeto respuestas

    let filter = {};

    try {
        const resources = await resourceService.getAllVerifiedResources(filter);

        serverResponses.sendSuccess(res, messages.SUCCESSFUL, resources);     
    }
    catch (err) {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, err.errors);
    }
}

const getResourceById = async (req, res) => {

    try {
        const resource = await resourceService.getResourceById(req.params.id);

        serverResponses.sendSuccess(res, messages.SUCCESSFUL, resources);   
    }
    catch (err) {

        serverResponses.sendErrorList(res, messages.BAD_REQUEST, err.errors);
    }
}

const createResource = async (req, res) => {}

const updateResource = async (req, res) => {

    try {
        const resource = await resourceService.updateResource(req.body.id, req.body.userId);

        serverResponses.sendSuccess(res, messages.SUCCESSFUL, resources);   
    }
    catch (err) {

        serverResponses.sendErrorList(res, messages.BAD_REQUEST, err.errors);
    }
}

const deleteResource = async (req, res) => {

    try {
        const resource = await resourceService.deleteResource(req.params.id, req.body.userId);

        serverResponses.sendSuccess(res, messages.SUCCESSFUL, resources);   
    }
    catch (err) {

        serverResponses.sendErrorList(res, messages.BAD_REQUEST, err.errors);
    }
}


modules.exports = {
    getAllResources,
    getAllVerifiedResources,
    getResourceById,
    createResource,
    updateResource,
    deleteResource
}