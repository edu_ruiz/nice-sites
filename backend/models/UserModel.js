const mongoose = require("mongoose");
const { Profile } = require("./ProfileModel");

//'role': String,
const User = mongoose.model( 'users', {
    'nick': String,
    'email': String,
    'ip': String,
    'passw': String,
    'admin': Boolean,
    'level': Number,
    'profile': { type: mongoose.Types.ObjectId , ref: Profile },
    'createdAt': Date,
    'updatedAt': Date,
    'verifiedAt': Date,
    'verifiedBy': { type: mongoose.Types.ObjectId , ref: 'User' },
})

module.exports = { User };