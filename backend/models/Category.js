const mongoose = require('mongoose');

const Category = mongoose.model('categories', {

    name: String,
    type: String,
    path: String,
    enabled: String
});

module.exports = {Category};