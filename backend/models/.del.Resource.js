/**
 * Created by Syed Afzal
 */
const mongoose = require('mongoose');
const { User } = require("./UserModel");
//const Schema = require('mongoose');
//const { linkSchema } = require("./schema/linkSchema");

const Resource = mongoose.model('resources', {

        // USAR constantes exportables para todo: validar, faker, modelo, middleware, route params
        // guardar en DB constantes configurables en la app y reutilizables,  (p.ej: youtube|facebook|insta... )

        //table DB blacklist(domain)
        // table DB namespaces para los enums de validación, constantes env. para todo lo demás
        // constantes que se vayan a reutilizar y no estén guardadas en 'categories'
        // standarizar todo para poder llamar a constantes y funciones exportables

    // FALTA!! Incorporar nested Schemas al modelo

    url: String,
    detail: Object,
    properties: Object,
    contents: Object,
    feedback: Object,
    timestamps: Object,
    signature: Object,
    stage: String
    
    /*
    head: String,
    url : {
        type: String
    },
    domain: {
        type: String,
        match: /^(?!.*(youtube|facebook|instagram|twitter|wallapop|ebay|spotify|soundcloud).*?)/
    },
    suffix : {
        type: String,
        enum: {
            values: ['com', 'es', 'org', 'edu'],
            message: 'extensión no admitida'
        }
    },
    path: String,
    file: String,
    ext: String,
    title: String,
    text: String,
    subject: String,
    keywords: {
        type: [String]
    },
    section: {
        type: String,
        enum: {
            values: [
                'global',
                'news',
                'science',
                'history',
                'home',
                'arts',
                'music',
                'sports',
                'gaming',
                'coding'
            ],
            message: 'categoría no admitida'
        }
    },
    source: {
        type: String,
        enum: {
            values: [
                'news',
                'blog',
                'community',
                'hosting',
                'bussiness',
                'personal',
                'wiki'
            ],
            message: 'fuente no admitida'
        }
    },
    level: Number,
    qualif: {
        type: Number,
        min: [0, 'la nota es inferior al rango'],
        max: [10, 'la nota es superior al rango'],
        precission: 0.01
    },
    votes: Number,
    mvf: String,
    mvux: String,
    updatedAt: Date,
    createdAt: Date,
    verifiedAt: Date,
    userId: { type: mongoose.Types.ObjectId , ref: User },
    verifiedBy: { type: mongoose.Types.ObjectId, ref: User } //*/
});

module.exports = {Resource};
