const mongoose = require("mongoose");
const { User } = require("./UserModel");
const { Url } = require("./URLModel");

// type: help, title, text
// path = page
// place = distinct sections or blocks
// name = nmemonic alias
// value = String to be placed

const Content = mongoose.model('contents', {
    page: String,
    type: String,
    text: String
    
});

module.exports = {Content};