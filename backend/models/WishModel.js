const mongoose = require("mongoose");
const { User } = require("./UserModel");
const { Url } = require("./URLModel");


const Wish = mongoose.model( 'wishes', {
    'title': String,
    'scope': String,
    'list': [{ type: mongoose.Types.ObjectId , ref: Url }],
    'userId': { type: mongoose.Types.ObjectId , ref: User },
    'createdAt': Date,
    'updatedAt': Date
})

module.exports = { Wish };