const mongoose = require('mongoose');
const { Url } = require('./URLModel');
const { User } = require('./UserModel');

const Certificate = mongoose.model('certificates', 
    /*{
        '_id': {
            'resourceId': { type: mongoose.Types.ObjectId , ref: Url },
            'level': {
                'type': Number,
                'precission': 0.1
            },
            'openedAt': Date
        },
        'closedAt': Date,
        'scores': {
            'contents': {
                'type': Number,
                'precission': 0.5
            },
            'ux': {
                'type': Number,
                'precission': 0.5
            },
        },
        'nft': {
            'id': String,
            'path': String
        }
    }*/
    {
        'resourceId': { type: mongoose.Types.ObjectId , ref: Url },
        'level': {
            'type': Number,
            'precission': 0.1
        },
        'openedAt': Date,
        'closedAt': Date,
        'stage': String,
        'remaining': Number,
        'contents': {
            'type': Number,
            'precission': 0.01
        },
        'ux': {
            'type': Number,
            'precission': 0.01
        },
        'contentsTag': String,
        'uxTag': String,
        'nft': {
            'id': String,
            'path': String
        }
    }
)

module.exports = {Certificate};