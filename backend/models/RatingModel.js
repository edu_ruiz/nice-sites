const mongoose = require('mongoose');
const { Url } = require('./URLModel');
const { User } = require('./UserModel');
const { Certificate } = require('./CertificateModel');

const Rating = mongoose.model('ratings',    
   /* {
        '_id': {
            'userId': { type: mongoose.Types.ObjectId , ref: User },
            'certificateId': 
                { 
                    type: Object,
                    ref: Certificate}
        },
        'contents': Number,
        'ux': Number,
        'contentsTag': String,
        'uxTag': String,
        'createdAt': Date
    }*/

    {
        'userId': { type: mongoose.Types.ObjectId , ref: User },
        'certificateId': { type: mongoose.Types.ObjectId , ref: Certificate },
        'resourceId': { type: mongoose.Types.ObjectId , ref: Url },
        'contents': Number,
        'ux': Number,
        'contentsTag': String,
        'uxTag': String,
        'createdAt': Date,
        'updatedAt': Date
    }
)

module.exports = {Rating};