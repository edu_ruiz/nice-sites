const mongoose = require("mongoose");
const { User } = require("./UserModel");
const { Url } = require("./URLModel");

// type: IP, email, nick, domain
const Blacklist = mongoose.model('blacklists', {
    type: String,
    value: String,
    reason: String,
    description: String,
    createdBy: { type: mongoose.Types.ObjectId , ref: User },
    updatedAt: Date,
    createdAt: Date
});

module.exports = {Blacklist};