const mongoose = require("mongoose");
const { User } = require("./UserModel");
const { Url } = require("./URLModel");

const Profile = mongoose.model('profiles', {
    'section': [String],
    'source': [String],
    'matchOption': [String],
    'qualityOption': [String],
    'parental': [String],
    'searchBy': [String],
    'favouriteIds': [{ type: mongoose.Types.ObjectId , ref: Url }],
    updatedAt: Date,
    createdAt: Date
});

module.exports = {Profile};