const { Resource } = require("../models/Resource");
//const mongoose = require('mongoose');


const getAllResources = async (filterData) => {
    let filter = {};

    if(filterData) {

        filter = { ...filterBase, filterData };
    }

    const resources = await Resource.find(
        filter,
        { '__v': 0, 'signature': 0, 'stage': 0, 'detail': 0  })
    .sort({ 'qualify': 'desc', 'timestamps.updatedAt': 'desc', 'feedback.ratings': 'desc', 'timestamps.createdAt': 'desc' })
    .limit(50)
        // cambiar limit por paginación
    return resources;
}

const getAllVerifiedResources = async (filterData) => {

    let filter = {};
    let filterBase = {
        'stage': { $in: ['enabled', 'locked']},
        'timestamps.verifiedAt': { $not: {$in: ['undefined', false, null, '']}}
    };

    if(filterData) {

        filter = { ...filterBase, filterData };
    }

    const resources = await Resource.find(
        filter,
        { '__v': 0, 'signature': 0, 'stage': 0, 'detail': 0  })
    .sort({ 'qualify': 'desc', 'timestamps.updatedAt': 'desc', 'feedback.ratings': 'desc', 'timestamps.createdAt': 'desc' })
    .limit(50)

    return resources;
}

const getResourceById = async (id) => {

    const resource = await Resource.findOne({id: id});

    return resource;
}

const createResource = async (data, userId) => {

    const resource = await Resource.insertOne(data);

    return resource;
}

const updateResource = async (data, userId) => {

    const date = new Date();
    const params = {};

    // Separar adaptadores/preparadores, llevar al controlador y traer los datos ya masticados, sin objeto params
    for( let key in data) {
        if(key !== 'id' && key !== 'field' && key !== 'verify' && key !== 'userId') {
            params[`${data.field}.${key}`] = data[key];
        }
    }

    if( data.verify ) {

        params[`timestamps.verifiedAt`] = date;
        params[`signature.verifiedBy`] = userId;

    }

    params[`timestamps.updatedAt`] = date;
    params[`signature.updatedBy`] = userId;

    console.log(params);

    const result = await Resource.updateOne(
        {_id : data.id },
        {$set: params }
    );

    return result;
}

const deleteResource = async (id, userId) => {

}

modules.exports = {
    getAllResources,
    getAllVerifiedResources,
    getResourceById,
    createResource,
    updateResource,
    deleteResource
}