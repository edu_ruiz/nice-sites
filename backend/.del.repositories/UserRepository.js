const { User } = require("../models/çUser");
//const mongoose = require('mongoose');


const getAllUsers = async (filterData) => {
    let filter = {};

    if(filterData) {

        filter = { ...filterBase, filterData };
    }

    const Users = await User.find(
        filter,
        { __v: 0 })
    .sort({'timestamps.createdAt': 'desc', 'nick': 'asc' })
    .limit(50)
        // cambiar limit por paginación
    return Users;
}


const getUserById = async (id) => {

    const User = await User.findOne({id: id});

    return User;
}

const createUser = async (data) => {

    const User = await User.insertOne(data);

    return User;
}

const updateUser = async (data) => {

    const date = new Date();
    const params = {};

    // Añadir data desde el controlador y servicio

    params[`timestamps.updatedAt`] = date;

    console.log(params);

    const result = await User.updateOne(
        {_id : data.id },
        {$set: params }
    );

    return result;
}

const deleteUser = async () => {

}


modules.exports = {
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser
}