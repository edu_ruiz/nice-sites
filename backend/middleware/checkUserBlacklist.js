const {Blacklist} = require('../models/BlacklistModel')

const checkUserBlacklist = async (req, res, next) => {

    try {

        let userId = '';

        if( req.session.user ) {
            
            userId = req.session.user;
        }
        else if( req.params.id ) {

            userId = req.params.id;

        }
        else if ( req.query.userId ) {

            userId = req.query.userId;
        }
        else if ( req.body.userId ) {

            userId = req.body.userId;
        }

        const check = await Blacklist.findOne({ type: 'userId', value: userId });

        if ( ! check ) {
            //console.log("method :" + JSON.stringify(check))
            
            next();
        }
        else {
            throw new Error("This user is blocked")
        }
    }
    catch (e) {
        console.log(e)
    }
    
}


module.exports = {checkUserBlacklist};