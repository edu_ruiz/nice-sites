const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");

const { User } = require("../models/UserModel");


const checkIsLogged = async function (req, res, next) {

    try {
    //console.log(req.session)
        if(req.session && req.session.user) {

            const user = await User.findOne({_id: req.session.user});

            if ( user ) {

                next();
            }
            else {
                throw new Error("should login");
            }
            //console.log(req.session.user)
        }
        else {
            throw new Error("forbidden");
        } // */
    }
    catch(e) {
        //serverResponses.sendError(res, messages.BAD_REQUEST, e);
    }

}

module.exports = {checkIsLogged};