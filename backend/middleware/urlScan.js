const nvt = require('node-virustotal');

const serverResponses = require("../utils/helpers/responses");
const {urlDetail} = require("./urlDetail");
const messages = require("../config/messages");

const urlScan = (req, res, next) => {

    //console.log(Object.keys(nvt))
   

    const v3 = nvt.makeAPI(500);
    const api = v3.setKey("8a9e7d74dad493fef9668878f0ae65abf68c18ebffd84286a289eb49fd53602c");
    //console.log(Object.keys(api))
    //console.log(api.getAnalysisInfo())
    //api.setDelay(15000);
    // wadefamilytree.org
// detail.domain + "." + detail.suffix
    const detail = urlDetail(req.query.url);
    const url = detail.domain + "." + detail.suffix;

    const theSameObject = api.domainLookup( url , function(err, resp){
        if (err) {
          console.log('Well, crap.');
          console.log(err);
          //serverResponses.sendError(res, messages.BAD_REQUEST, err);
          req.error = err;
          next();
        }
        else {
        const obj = JSON.parse(resp); 
        let response = [];
        //req.scan = obj.data.attributes.last_analysis_results;    
        const {last_analysis_results} = obj.data.attributes;
        const keys = Object.keys(last_analysis_results);
        
        keys.map( (key, i) => {
            const {category, result} = last_analysis_results[key];
            if( result !== 'clean' && result !== 'unrated' ) {
                response.push({
                    name: key,
                    category: category,
                    result: result
                });
            }      
        })
        
        req.scan = response;
        console.log(response)
        next();
        }
      });
    /*const theSameObject = api.initialScanURL('http://l2.wikipedia.org', function(err, res){
  if (err) {
    console.log('Well, crap.');
    console.log(err);
    return;
  }
  console.log(JSON.stringify(res));
  return;
}); // */
    /*const hashed = nvt.sha256("https://google.com");//req.query.url
    const theSameObject = api.urlLookup(hashed, function(err, res){
        if (err) {
          console.log('Well, crap.');
          console.log(err);
          return;
        }
        console.log(JSON.stringify(res));
        return;
      }); // */

    //console.log(hashed)

}


module.exports = {urlScan};