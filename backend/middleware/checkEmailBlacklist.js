const {Blacklist} = require('../models/BlacklistModel')

const checkEmailBlacklist = async (req, res, next) => {

    try {

        let email = '';

        if( req.params.email ) {

            email = req.params.email;

        }
        else if ( req.query.email ) {

            email = req.query.email;
        }
        else if ( req.body.email ) {

            email = req.body.email;
        }

        const check = await Blacklist.findOne({ type: 'email', value: email });

        if ( ! check ) {
            //console.log("method :" + JSON.stringify(check))
            
            next();
        }
        else {
            throw new Error("This user is blocked")
        }
    }
    catch (e) {
        console.log(e)
    }
    
}


module.exports = {checkEmailBlacklist};