const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { User } = require("../models/UserModel");

const checkIsAdmin = async function (req, res, next) {

    try {
        if(req.session.user) {

            const user = await User.findOne({_id: req.session.user});

            if ( user && user.admin ) {

                next();
            }
            //console.log(req.session.user)
        }
        else {
            throw new Error("forbidden");
        }
        

        next();
    }
    catch(e) {
        //serverResponses.sendError(res, messages.BAD_REQUEST, "forbidden");
    }
}

module.exports = {checkIsAdmin};