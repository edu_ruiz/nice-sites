

const serverTime = function (req, res, next) {
    req.serverTime = new Date();
    console.log("Middle servertime: " + req.serverTime)
    next();
  };

  module.exports = {serverTime};