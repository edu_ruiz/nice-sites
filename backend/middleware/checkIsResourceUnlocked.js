const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");

const { Url } = require("../models/URLModel");

const checkIsResourceUnlocked = async function (req, res, next) {

    try {
        let resource = {};

        if(req.query.resourceId) {

            resource = await Url.findOne({_id: req.query.resourceId});
        }
        else if(req.body.resourceId) {
            resource = await Url.findOne({_id: req.body.resourceId});
        }

        if ( resource && resource.stage !== 'protected' ) {

            next();
        }
        else {
            throw new Error("forbidden");
        }
        
        next();
    }
    catch(e) {
        //serverResponses.sendError(res, messages.BAD_REQUEST, e);
    }
}

module.exports = {checkIsResourceUnlocked};