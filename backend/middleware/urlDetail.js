

const urlDetail = (url) => {
    
    const headerSplit = url.split(':');

    const header = headerSplit[0];
    
    const domainSplit = headerSplit[1].split('//')

    const domain = domainSplit[1].split('.')[0];
    const extensionSplit = domainSplit[1].split('.');
    const extension = extensionSplit[extensionSplit.length - 1];
    const suffixSplit = domainSplit[1].split('.')[1];
    // faltaría adaptar a sufijos compuestos .gov.es .com.ar , etc
    const suffix = suffixSplit.split('/')[0];

    const pathSplit = suffixSplit.split('/');

    let path = '/';

    pathSplit.map( (part, i) => {
        if ( i > 0 && i < pathSplit.length -1 ) {
            path += part + '/';
        }
    })

    const fileSplit = pathSplit[pathSplit.length - 1];
    
    const file = fileSplit.split('.')[0];

    const detail = {
        header: header,
        domain: domain,
        suffix: suffix,
        path: path,
        file: file,
        extension: extension
    }

    return detail;
}


module.exports = {urlDetail};