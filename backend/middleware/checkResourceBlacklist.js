const {Blacklist} = require('../models/BlacklistModel');
const { urlDetail } = require('./urlDetail');

const checkResourceBlacklist = async (req, res, next) => {

    console.log(req.body)
    try {

        let url = '';

        if( req.params.url ) {

            url = req.params.url;

        }
        else if ( req.query.url ) {

            url = req.query.url;
        }
        else if ( req.body.url ) {

            url = req.body.url;
        }

        
        const detail = urlDetail(url);

        const domain = detail.domain;

        const check = await Blacklist.findOne({ type: 'domain', value: domain });

        if ( ! check ) {
            //console.log("method :" + JSON.stringify(check))
            
            next();
        }
        else {
            throw new Error("Can't publish anything from this domain");
        }
    }
    catch (e) {
        //serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
    }
    
}


module.exports = {checkResourceBlacklist};