const session = require('express-session');
const serverResponses = require("../utils/helpers/responses");

exports.userAuth = (req, res, next) => {

    if (req.session && req.session.user === "amy" && req.session.admin) {

        return next();
    }   
    else {
        return res.sendStatus(401);
    }
    
    /*const token = req.cookies.jwt
    if (token) {
      jwt.verify(token, jwtSecret, (err, decodedToken) => {
        if (err) {
          return res.status(401).json({ message: "Not authorized" })
        } else {
          if (decodedToken.role !== "user") {
            return res.status(401).json({ message: "Not authorized" })
          } else {
            next()
          }
        }
      })
    } else {
      return res
        .status(401)
        .json({ message: "Not authorized, token not available" })
    } // */
  }