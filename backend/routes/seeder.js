const express = require("express");

const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");

const { faker } = require('@faker-js/faker');
const { Category } = require("../models/Category");
const { Rating } = require("../models/RatingModel");
const { User } = require("../models/UserModel");
const { Url } = require("../models/URLModel");
const { Wish } = require("../models/WishModel");
const { Certificate } = require('../models/CertificateModel');

const { Profile } = require("../models/ProfileModel");
const { Blacklist } = require("../models/BlacklistModel");
const { Content } = require("../models/ContentModel");

const routes = (app) => {
  const router = express.Router();

  // SEED DB

  router.get("/drop",(req,res) => {
    const er = new Url({});
    const conn = er.db;
    conn
      .dropDatabase()
      .then( (result) => {
        serverResponses.sendSuccess(res, messages.SUCCESSFUL , "dropped database");
      })
      .catch( (e) => {
        console.log(e);
      });
  })

  router.get("/wishes", async (req,res) => {
    let rows = [];
    const userIds = await User.find({},{"_id": 1}).distinct("_id");
    const resources = await Url.find({},{"_id": 1}).distinct("_id");
    const list = [];
    for ( let i= 0; i < Math.floor(Math.random()*60); i++) {
      list.push(resources[Math.floor(Math.random() * resources.length)])
    }
    
    for( let i = 0; i < 50; i++ ) {

       rows.push(new Wish({
        'title': faker.lorem.sentence(),
        'userId': userIds[Math.floor(Math.random() * userIds.length)],
        'scope': faker.helpers.arrayElement(['public', 'private', 'open']),
        'list': faker.helpers.arrayElements(faker.helpers.multiple(list)),
        'createdAt': faker.date.past(),
        'updatedAt': faker.date.recent()
     }))
    }
    
    Wish
    .insertMany(rows)
    //.createIndex({'url': 'text', 'title': 'text', 'text': 'text' })
    .then((result) => {;
      console.log("Wishes Added")
        
    })
    .catch((e) => {
      console.log(e.errors)
    })
  })

  router.get("/profiles", (req, res) => {
    const profile = {
      'userId': '65c151d6d19fbab1411b6b6a',
      'section': ['coding', 'gaming', 'news'],
      'source': ['hosting', 'official', 'personal'],
      'searchBy': 'text',
      'matchOption': 'free',
      'quality': 'acceptable',
      'favouriteIds': ['65c151d7d19fbab1411b6c53']
    }

    Profile.insertMany([profile])
    .then( (res) => {
      console.log("profile added")
    })
    .catch( (e) => {
      console.log(e)
      serverResponses.sendErrorList(res, messages.BAD_REQUEST, e)
    })
  })

  router.get("/categories", (req, res) => {

// modificar cats education (solo nivel, sin indicar materia)
    const config = [

    {
      filters: ['section','source','matchOption', 'searchBy', 'parental', 'quality']
    },
    {
      radio: ['searchBy', 'parental', 'quality', 'matchOption' ]
    },
    {
      searchSteps: ['section', 'source','suffix','updated','quality','parental','extension']
    },
    {
      suffix: [
        'edu',
        'org',
        'com',
        'net',
        'es',
        'cat',
        'uk',
        'br',
        'onion',
        'po',
        'ci',
        'it',
        'fr',
        'uk',
        'in'
      ]
    },
    {
      extension: [
        'html',
        'xml',
        'png',
        'jpeg',
        'csv',
        'xls',
        'pdf',
        'doc',
        'webm',
        'mp3',
      ]
    },
      {section : [
        'global',
        'news',
        'education',
        'science',
        'history',
        'home',
        'arts',
        'music',
        'sports',
        'gaming',
        'coding',
        'humour',
        'sites'
      ]},
      {source: [
        'news',
        'blog',
        'community',
        'hosting',
        'bussiness',
        'personal',
        'wiki',
        'forums',
        'official',
        'institution'
    ]},
    {
      rating: [
      'contents',
      'ux',
      'parental'
    ]},
    {
      quality: [
        'awesome',
        'nice',
        'acceptable',
        'poor',
        'awful'
      ]
    },
    {
      education: [
        'none',
        'kinder',
        'primary',
        'high school',
        'university',
        
      ]
    },
    {
      subject: [
        'none',
        'maths',
        'computer',
        'geography',
        'biology',
        'botanical',
        'drama',
        'music',
        'economics',
        'psychology',
        'social',
        'commerce',
        'medicine',
        'science',
        'physics',
        'chemistry',
        'history',
        'humanities',
        'arts',
        'philosophy',
        'literature',
        'language',
        'communication',
        'physical edu',
        'sexual edu',
        'other subject'
      ]
    },
    {
      parental: [
        'safe',
        'child',
        'teen',
        'adult',
        'unsafe'
      ]
    },
    {
      searchBy: [
      'title',
      'text',
      'url',
      'keywords',
      'subject'
    ]},
    {
      matchOption: [
        'free',
        'exact'
    ]},
    {updated: [
      'unknown',
      'few years',
      'last year',
      'few months',
      'last month',
      'few weeks',
      'last week',
      'today'
    ]},
    {
      bottomNav: [
        'export',
        'home',
        'back'
      ]
    },
    {
      mainNav :[
        'home',
        'search',
        'export',
      ]
    },
    {
      loginNav: [
        'login',
        'register',
        'remember'
      ]
    },
    {
      userNav: [
        'profile',
        'new resource',
        'verify',
      ]
    },
    {
      popularNav: [
        'help',
        'recent',
        'education',
        'parental',
        'news',
        'sports',
        'gaming',
        'music',
        'arts',
        'random'
      ]
    }
    ]
      
    const rows = [];
    config.map( (category, i) => {

        const keys = Object.keys(category);
//console.log(keys)
        const values = Object.values(category);
//console.log(values);
        values[0].map( (cat, j) => {
          //if( keys[0] === 'mainNav' || keys[0] === 'resourcesNav')
          let path = '';
          switch (cat) {
            case 'home': path = ''; break;
            case 'new resource' : path = 'add'; break;
            //case 'tree': path = 'resources'; break;
            //case 'forms': path = 'add'; break;
            default: path = cat ;
          }
          rows.push( {
            'name': cat,
            'type': keys[0],
            'path': `/${path}`,
            'enabled': true
          }
        )
      });

    });
        
//console.log(list)
  Category
      .insertMany(rows)
      //.createIndex({'url': 'text', 'title': 'text', 'text': 'text' })
      .then((result) => {;
        console.log("Categories Added")
        serverResponses.sendSuccess(res, messages.SUCCESSFUL , "categories added");
          
      })
      .catch((e) => {
        console.log(e.errors)
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
      })

  })

  router.get("/users", (req, res) => {
  
    //SEED DB 
    let rows = [];

    for( let i = 0; i < 50; i++ ) {

       rows.push(new User({
        'nick': faker.person.firstName(),
        'email': faker.internet.email(),
        'ip': faker.internet.ip(),
        'role': faker.helpers.arrayElement(['admin', 'editor', 'user']),
        'passw': '1234',
        'createdAt': faker.date.past(),
        'verifiedAt': faker.date.recent()
     }))
    }
    
    User
    .insertMany(rows)
    //.createIndex({'url': 'text', 'title': 'text', 'text': 'text' })
    .then((result) => {;
      console.log("USERS Added")
      serverResponses.sendSuccess(res, messages.SUCCESSFUL , "users added");
    })
    .catch((e) => {
      console.log(e.errors)
      serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
    })
  });

  router.get("/resources", async (req, res) => {
    const rows = [];
    const userIds = await User.find({},{"_id": 1}).distinct("_id")

    for (let i = 0; i < 900; i++) {
      const head = 'https://';
      const domain = faker.company.buzzNoun();
      const suf = faker.helpers.arrayElement(['com', 'es', 'org', 'edu']);
      const path  = faker.system.directoryPath();
      const file = faker.internet.domainWord();
      const ext = faker.helpers.arrayElement(['xml', 'html', 'json', 'net', 'jpeg', 'png' ]);
      const qlf1 = faker.number.float({ min: 0, max: 5, precision: 0.01 });
      const qlf2 = faker.number.float({ min: 0, max: 5, precision: 0.01 });
      // Adaptar a aggregate from Ratings table
      const votes = faker.number.int(9000000);
      const lev = Math.round(Math.log10(votes)); //faker.number.int(10); //
      const dt = faker.date.recent();
      const kw = faker.helpers.arrayElements(faker.helpers.multiple(faker.lorem.word));
      // SUSTITUIR ENUM POR Listados de un query a categories 'section'
      const sect = faker.helpers.arrayElement(
        ['global', 'news', 'education', 'science','history','home','arts','music','sports','gaming', 'coding', 'humour']
      );
      let education = 'none';
      let subject = 'none';
      
      if( sect === 'education' ) {
        subject = faker.helpers.arrayElement(
          [
            'global',
            'maths',
            'biology',
            'medicine',
            'research',
            'chemistry',
            'filosophy',
            'literature',
            'languages',
            'communication',
            'other'
          ]
        );

        education = faker.helpers.arrayElement(
          [
            'kinder',
            'primary',
            'secondary',
            'high school',
            'vocational',
            'bachelor',
            'university',
            'master',
            'research',
            'doctorate',
            'professor',
            'professional'
          ]
        )
      }
      const src = faker.helpers.arrayElement(
        ['news','blog','community', 'wiki', 'hosting','bussiness','forums','personal', 'official', 'institution']
      );
      const verifBy = [ userIds[ Math.floor( Math.random() * userIds.length ) ], '' ] ;
      const verifAt = [ dt, ''] ;
      const index = Math.round(Math.random()) ;

      const verifiedBy = verifBy[index];
      const verifiedAt = verifAt[index];

      rows
        .push( new Url 
        ({
          url :  head + domain + '.' + suf +  path + '/' + file + '.' + ext,
          detail: {
            head: head,
            domain : domain,
            suffix : suf,
            path  : path,
            file : file,
            extension : ext,
          },
          contents: {
            title: faker.lorem.sentence(),
            text: faker.lorem.sentences(),
            subject: faker.lorem.sentence(),
            keywords: kw,
          },
          properties: {
            section: sect,
            source: src,
            parental: faker.helpers.arrayElement(
              [ 'adult', 'teen', 'child', 'safe', 'unsafe' ]
            ),
            education: education,
            subject: subject,
            updated: faker.helpers.arrayElement([
              'unknown',
              'few years',
              'last year',
              'few months',
              'last month',
              'few weeks',
              'last week',
              'today'
            ])
          },     
          feedback: {
            certificates: [],
            level: lev,
            ratings: votes,
            contents: qlf1,
            ux: qlf2,
            quality: faker.helpers.arrayElement(
              [ 'awesome', 'nice', 'acceptable', 'poor', 'awful' ]
            ),
            contentsTag: faker.word.adjective(),
            uxTag: faker.hacker.adjective()
          },
          timestamps: {
            verifiedAt: verifiedAt,
            updatedAt: faker.date.recent(),
            createdAt: faker.date.past()  
          },
          signature: {
            userId: userIds[Math.floor(Math.random() * userIds.length)],
            updatedBy: verifiedBy,
            verifiedBy: verifiedBy
          },  
          stage: faker.helpers.arrayElement(
            [ 'protected', 'enabled', 'locked', 'disabled', 'blacklist', 'delete' ]
          )            
        })
      );
    }

    Url
    .insertMany(rows)
    //.createIndex({'url': 'text', 'title': 'text', 'text': 'text' })
    .then((result) => {
      console.log("Resources Added")
      serverResponses.sendSuccess(res, messages.SUCCESSFUL , "resources added");

    })
    .catch((e) => {
      //serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      console.log(e.errors )
      serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
    });

  });

  router.get("/certificates", async (req, res) => {
    const rows =  [];

    const urlIds = await Url.find({},{'_id': 1}).distinct('_id');

      urlIds.map( (id, i) => {
        let level = faker.number.float({ min: 1, max: 4, precision: 0.1 });
        rows.push(new Certificate({
          'resourceId': id,
          'level': level,
          'openedAt': faker.date.past(),
          'closedAt': faker.date.past(),
          'remaining': faker.number.int({ min: 0, max: (Math.pow(10, Math.floor(level)/10) )}),
          'stage': faker.helpers.arrayElement(
            [ 'closed' ]
          ),
          'contents': 0,
          'ux': 0,
          'contentsTag': '',
          'uxTag': '',
          
        }));
        level = faker.number.float({ min: 1, max: 10, precision: 0.1 });
        rows.push(new Certificate({
          'resourceId': id,
          'level': level,
          'openedAt': faker.date.past(),
          'closedAt': faker.date.recent(),
          'remaining': faker.number.int({ min: 0, max: (Math.pow(10, Math.floor(level)/10) )}),
          'stage': faker.helpers.arrayElement(
            [ 'closed', 'delete' ]
          ),
          'contents': 0,
          'ux': 0,
          'contentsTag': '',
          'uxTag': '',
        }));
        level = faker.number.float({ min: 1, max: 10, precision: 0.1 });
        rows.push(new Certificate({
          'resourceId': id,
          'level': level,
          'openedAt': faker.date.recent(),
          'closedAt': faker.date.future(),
          'remaining': Math.pow(10, Math.floor(level)) ,
          'stage': faker.helpers.arrayElement(
            [ 'opened','delete' ]
          ),
          'contents': 0,
          'ux': 0,
          'contentsTag': '',
          'uxTag': ''
        }));
      });

      Certificate
        .insertMany(rows)
        .then((result) => {;
          console.log("Certs added ...ratings will take a while...")
          //res.send(200);
          serverResponses.sendSuccess(res, messages.SUCCESSFUL , "certs added");
        })
        .catch((e) => {
          console.log("error: ",e)
          serverResponses.sendError(res, messages.BAD_REQUEST, e);
        });
    
  })

  router.get("/ratings", async (req,res) => {
    const rows = [];

    const certs = await Certificate.find({},{'_id': 1, 'level': 1, 'resourceId': 1});
    const userIds = await User.find({},{'_id': 1}).distinct('_id');

    // solo hay un certificate abierto a la vez (los demás estarán cerrados)
    // obtener level de certIds, y emitir nº votos arreglo a esto 

    // Buscar forma de no duplicar votos: mismo user-mismo cert (solo uno por cert-user)
    certs.map( async (cert, k) => {
      const {_id, level, resourceId} = cert;
      const nRatings = Math.round(Math.random()* (Math.pow(2, (Math.floor(level)))));
      //console.log(resourceId)
      for(let i = 0; i < nRatings; i++) {
        const dt = [faker.date.recent(), faker.date.past()]
        //const resource = await Url.findOne({'_id': resourceId});
        rows.push(new Rating({
          'userId': userIds[Math.floor(Math.random() * userIds.length)],
          'certificateId': _id,
          'resourceId': resourceId,
          'contents': faker.number.float({ min: 1, max: 5, precision: 0.5 }),
          'ux':  faker.number.float({ min: 1, max: 5, precision: 0.5 }),
          'contentsTag': faker.word.adjective(),
          'uxTag': faker.hacker.adjective(),
          'createdAt': faker.helpers.arrayElement(dt),
          'updatedAt': faker.date.recent()  
        }))
      }
      
    })

    Rating
    .insertMany(rows)
    //.createIndex({'url': 'text', 'title': 'text', 'text': 'text' })
    .then((result) => {;
      console.log("RATINGS ADDED")
      serverResponses.sendSuccess(res, messages.SUCCESSFUL , "ratings added");
    })
    .catch((e) => {
      console.log(e.errors)
      serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
    }); // */
  })

  router.get("/keys", async (req, res) => {

    //let ids = Url
      //.find({},{'_id':1})
      //.distinct('_id').exec()
      const userIds = await User.find({},{"_id": 1}).distinct("_id")
      const urlIds = await Url.find({},{"_id": 1}).distinct("_id")

    Rating.updateMany(
        {'userId': undefined }, 
        { 'userId': userIds[Math.floor(Math.random() * userIds.length)],
          'resourceId': urlIds[Math.floor(Math.random() * urlIds.length)]
        }
      ).then( (rsc) => {
        console.log("UPDATED Rating KEYS")
        serverResponses.sendSuccess(res, messages.SUCCESSFUL , "keys added");
        
      })
      .catch( (e) => {
        console.log(`ERROR: ${e}`);
      })

  })

  /*
  router.get("/indexes", async (req, res) => {

    Url
    .createIndex({'url': 'text'}) //, 'contents.title': 'text', 'contents.text': 'text' })
    .then((result) => {;
      console.log("INDEX ADDED")
      serverResponses.sendSuccess(res, messages.SUCCESSFUL , "indexes added");
    })
    .catch((e) => {
      console.log(e.errors)
      serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
    });

  }) // */

  router.get("/blacklists", (req, res) => {

    const rows = [
      {
        type: 'domain',
        value: 'facebook',
        reason: 'values'
      },
      {
        type: 'domain',
        value: 'instagram',
        reason: 'values'
      },
      {
        type: 'domain',
        value: 'tiktok',
        reason: 'values'
      },
      {
        type: 'domain',
        value: 'twitter',
        reason: 'values'
      },
      {
        type: 'domain',
        value: 'youtube',
        reason: 'values'
      },
      {
        type: 'domain',
        value: 'wallapop',
        reason: 'sales'
      },
      {
        type: 'IP',
        value: '162.168.168.0',
        reason: 'malware'
      },
      {
        type: 'email',
        value: 'forbidden@email.com',
        reason: 'bot'
      }
    ]

    Blacklist
      .insertMany(rows)
      .then( (result) => {
        console.log("Blacklist Added, Seeding complete! Reload browser tab, please")
        serverResponses.sendSuccess(res, messages.SUCCESSFUL , "blacklist added");
      })
      .catch( (e) => {
        console.log("Error on create blacklist")
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
      })

  })

  router.get("/contents", (req, res) => {

    const rows = [
      {
        page: 'verify',
        type: 'mainHeader',
        text: 'Verify Page'
      },
      {
        page: 'verify',
        type: 'welcomeText',
        text: 'We have some links to verify. Your help would be very useful.'
      },
      {
        page: 'verify',
        type: 'listHeader',
        text: 'Untrusted links'
      },
      {
        page: 'verify',
        type: 'formHeader',
        text: 'Please, verify this resource as follows'
      },
      {
        page: 'verify',
        type: 'helpText',
        text: 'You should click once at least on each link before proceed, to check it. Then, proceed to confirm/update its properties. Last, submit.'
      },
      {
        page: 'verify',
        type: '',
        text: ''
      }
    ]

    Content
      .insertMany(rows)
      .then( (result) => {
        console.log("Contents seed")
        serverResponses.sendSuccess(res, messages.SUCCESSFUL , "contents added");
      })
      .catch( (e) => {
        console.log(e)
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e);
      })
  })

  app.use("/seed", router);

};
module.exports = routes;
