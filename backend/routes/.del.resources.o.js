
const express = require("express");
const math = require("mathjs")
const { faker } = require('@faker-js/faker');
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Rating } = require("../models/RatingModel");
const { User } = require("../models/UserModel");
const { Url } = require("../models/URLModel");

//ROUTES PATH: /resources/* 
const routes = (app) => {
    const router = express.Router();

   /* router.param('id', function (req, res, next, id) {
        // Localizar el Resource en DB 
        // para extraer datos de los params
        //guardar en objeto res el objecto completo, para todos los verbos
        Url
            .findOne({'_id': id})
            .then( (resource) => {
                req.resource = resource;
            })
            .catch( (e) => {
console.log(`Resources getting by :id param  -->${e}`)
            })
        
        //req.id = {
        //  'id': id
        //}
        next();
    })

    router.route("/:id")
    
        .all( function (req, res, next) {
            // middleware for all verbs
            next()
        })
        .get(function (req, res, next) {
            //res.json(req.user)
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, req.resource );
        })
        .put(function (req, res, next) {
            // just an example of maybe updating the user
            req.user.name = req.params.name
            // save user ... etc
            res.json(req.user)
        })
        .post(function (req, res, next) {
            next(new Error('not implemented'))
        })
        .delete(function (req, res, next) {
            next(new Error('not implemented'))
        })
    // List All Resources (make paginate, scroll on demmand)
*/
    // Include :id, :params routes (?verified=false),
    // ENCADENAR POST Y DELETE
    const resourceId = '';
    router
    .all( function (req, res, next) {
        // middleware for all verbs this router
        next();
    })
   /* .param('id', function (req, res, next, id) {
        resourceId = id;
        next();
    })*/
    .get("/", (req, res) => {
        
        Url
        .find( {'verifiedAt': { $not: {$in: [undefined, null, ''] }}},{ __v: 0}
        )
        .then((urls) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });
    })

    router
    .get("/by-ids", (req,res) => {
        console.log("Params ids: " + req.query.ids)
        Url.find( {'_id': {$in: req.query.ids}},
            {__v: 0}
        )
        .then((urls) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });

    })//*/


    router
    .all( function (req, res, next) {
        // middleware for all verbs
        next()
    })
    .get("/verified", (req, res) => {
    
        Url
        .find(
            {level: { $gte: '2' }, 'verifiedAt': { $not: {$in: [undefined,null,'']}}},
            { __v: 0 })
        .sort({ qualif: 'desc',  updated: 'desc', created: 'desc' })
        .limit(50) // 50 popular
        .then((urls) => {
            
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);     
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });
    })
   
    router.post("/create", (req, res) => {
        // AÑADIR: revisar si el url es el home page, y restringir formulario a categoría 'sites'


    // AÑADIR: [keywords], text (textarea crear)
    // keywords: req.query.keywords,

    // Tratamiento previo de datos, trim(), descodificación cuando se aplique
    // array keywords toArray?

    // Manejo de email, IP, userId, añadir al model y adaptar a claves foráneas

//console.log(req.body);
        const date = new Date();

        const resource = new Url ({
        url : req.body.url,
        title: req.body.title,
        subject: req.body.subject,
        section: req.body.section,
        source: req.body.source,
        mvf: req.body.contents,
        mvux: req.body.ux,
        verified: date,
        updated: date,
        created: date
        })

        resource
            .save(resource)
            .then((result) => {
                serverResponses.sendSuccess(res, messages.SUCCESSFUL , result);
                //console.log(result)
            })
            .catch((e) => {
                //serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
                //console.log(e.errors )
            });
    });

    router.post("/edit/rating", (req, res) => {


        console.log(`POST Rating: ${req.body.contents} + ${req.body.ux}`)

        const rating = new Rating ({
            userId: req.body.userId,
            resourceId: req.body.resourceId,
            contents: req.body.contents,
            ux: req.body.ux,
            createdAt: new Date(),
        });

        rating
            .save()
            .then( (result) => {
                serverResponses.sendSuccess(res, messages.SUCCESSFUL , result);
            })
            .catch((e) => {
                //serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
                console.log(e.errors)
            });

    });

    app.use("/resources", router);

};
module.exports = routes;

