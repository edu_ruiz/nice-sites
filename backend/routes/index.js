const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
// const { User } = require("../models/UserModel");
const { Url } = require("../models/URLModel");
const { Category } = require("../models/Category");
//const { Rating } = require("../models/RatingModel");

const { serverTime } = require("../middleware/serverTime");

// ROUTES PATH: /index/*
const routes = (app) => {
  const router = express.Router();

  router.get("/", (req, res) => {

    Category.find(
      {})
      .then((data) => {   
        let types = [];
        let siteObject = {};data.map( (row, i) => {
          if( ! types.includes(row.type) ) {
            types.push(row.type);
          }
        })
    
        types.map( (type, i) => {
          siteObject[type] = [];
        })
    
        data.map( (row, i) => {
          siteObject[row.type].push(row.name);
        })
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, siteObject);
      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });


  // ROUTES: Feed Filter inputs, Select options, Labels, MatchSuggest >>distinct()<< from URL collection
  router.get("/nav/main", (req, res) => {

    Category.find(
      {'type': 'mainNav', 'enabled': true},
      {})
      .then((list) => {
//console.log(list)      
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/nav/popular", (req, res) => {

    Category.find(
      {'type': 'popularNav', 'enabled': true},
      {})
      .then((list) => {
//console.log(list)      
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/nav/resources", (req, res) => {

    Category.find(
      {'type': 'resourcesNav', 'enabled': true},
      {})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/nav/forms", (req, res) => {

    Category.find(
      {'type': 'formsNav', 'enabled': true},
      {})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/nav/user", (req, res) => {

    Category.find(
      {'type': 'userNav', 'enabled': true},
      {})
      .then((list) => {
//console.log(list)      
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/nav/login", (req, res) => {

    Category.find(
      {'type': 'loginNav', 'enabled': true},
      {})
      .then((list) => {
//console.log(list)      
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/sections", (req, res) => {

    Category.find(
      {'type': 'section', 'enabled': true},
      {})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/sources", (req, res) => {

    Category.find(
      {'type': 'source', 'enabled': true},
      {})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/search-by", (req, res) => {

    Category.find(
      {'type': 'searchBy', 'enabled': true},
      {})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/subjects", (req, res) => {

    Url.find(
      {},
      {'subject' : 1})
      .distinct('subject')
      .sort({'subject': 'asc'})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/tags/contents", (req, res) => {

    Url.find(
      {},
      {'contentsTag' : 1})
      .distinct('contentsTag')
      .sort({'contentsTag': 'asc'})
      .then((titles) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, titles);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/tags/ux", (req, res) => {

    Url.find(
      {},
      {'ux' : 1})
      .distinct('ux')
      .sort({'ux': 'asc'})
      .then((list) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  app.use("/index", router);

};
module.exports = routes;
