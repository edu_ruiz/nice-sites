const express = require("express");
const math = require("mathjs")
const { faker } = require('@faker-js/faker');
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Rating } = require("../models/RatingModel");
const { User } = require("../models/UserModel");
const { Url } = require("../models/URLModel");

const routes = (app) => {
  const router = express.Router();

  // public API EndPoints 
  router.get("/", (req, res) => {
    
    const test = {
        message: 'Info: welcome, timestamps, version, isReady, help'
    }

    serverResponses.sendSuccess(res, messages.SUCCESSFUL, test);

  });

  app.use("/api", router);

};
module.exports = routes;
