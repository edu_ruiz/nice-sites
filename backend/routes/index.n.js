const express = require("express");
const resourcesRoutes = require("./resources.routes");
const usersRoutes = require("./users.routes");


const router = express.Router();


//router.use("/site", siteRoutes);
//router.use("/resources", resourcesRoutes);
//router.use("/search", searchRoutes);
router.use("/users", usersRoutes);
//router.use("/ratings", ratingRoutes);

module.exports = router;