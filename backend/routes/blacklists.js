const express = require("express");

const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Blacklist } = require("../models/BlacklistModel");
const { urlDetail } = require("../middleware/urlDetail");
const { serverTime } = require("../middleware/serverTime");
const { checkIsLogged } = require("../middleware/checkIsLogged");
const { checkIsAdmin } = require("../middleware/checkIsAdmin");
const { checkUserBlacklist } = require("../middleware/checkUserBlacklist");



const routes = (app) => {
    const router = express.Router();

    router.get("/", (req,res) => {
        Blacklist
            .find({}).sort({type: 1})
            .then((list) => {
                //console.log(user)
                serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);
            })
            .catch((e) => {
                serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })

    router.post("/", (req,res) => {
        const date = new Date();
        const item = new Blacklist(
            {
                type: req.query.type,
                value: req.query.value,
                reason: req.query.reason,
                description: req.query.description,
                updatedAt: date,
                createdAt: date
            }
        )

        Blacklist
            .insert(item)
            .then((domain) => {
                //console.log(user)
                serverResponses.sendSuccess(res, messages.SUCCESSFUL, domain);
            })
            .catch((e) => {
                serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })

    router.post("/user", checkIsLogged, checkUserBlacklist, checkIsAdmin, serverTime, (req, res) => {

        console.log("block user",req.query.blockUserId)
        // TODO update resources from blocked user (stage= disabled) 

        const date = req.serverTime;

        const item = new Blacklist(
            {
                type: 'user',
                value: req.query.blockUserId,
                reason: 'bad use or abuse',
                description: '',
                createdBy: req.session.user,
                updatedAt: date,
                createdAt: date
            }
        )

        item
            .save()
            .then( (result) => {
                serverResponses.sendSuccess(res, messages.SUCCESSFUL, result);
            })
            .catch((e) => {
                console.log(e)
                //serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })

    router.get("/domains", (req,res) => {
        Blacklist
            .find({type: 'domain'},{value: 1}).distinct('value')
            .then((list) => {
                //console.log(user)
                serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);
            })
            .catch((e) => {
                serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })

    router.get("/domains/:value", (req,res) => {
        Blacklist
            .findOne({type: 'domain', value: req.params.value})
            .then((domain) => {
                //console.log(user)
                serverResponses.sendSuccess(res, messages.SUCCESSFUL, domain);
            })
            .catch((e) => {
                serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })

    router.get("/check-domain", (req,res) => {
        const detail = urlDetail(req.query.url);
        Blacklist
            .findOne({type: 'domain', value: detail.domain})
            .then((domain) => {
                //console.log(user)
                serverResponses.sendSuccess(res, messages.SUCCESSFUL, domain);
            })
            .catch((e) => {
                serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })


    app.use("/blacklists", router);

}

module.exports = routes;