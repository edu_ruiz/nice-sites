const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { User } = require("../models/UserModel");


const { checkIsLogged } = require("../middleware/checkIsLogged");
const { serverTime } = require("../middleware/serverTime");
const { checkIsAdmin } = require("../middleware/checkIsAdmin");
const { checkEmailBlacklist } = require("../middleware/checkEmailBlacklist");

const routes = (app) => {
  const router = express.Router();
  //const user = new URL();

  // SEED DB

  // Agregar middleware y rutas completas, con :id param común
    router.get("/", checkIsLogged, checkIsAdmin, (req, res) => {
        User.find({})
        .then((users) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, users);
        })
        .catch((e) => {
            serverResponses.sendError(res, messages.BAD_REQUEST, e);
        });// */
    })

  // Adaptar ruta a parámetro común y a middlewares EXPRESS Docs 
  router.get("/:id", checkIsLogged, (req, res) => {
   
    // Usar populate(), agregar Profile
    User.findOne({'_id': req.params.id})
    .populate('profile')
    .then((user) => {
//console.log(user)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, user);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

  router.post("/", checkEmailBlacklist, serverTime, (req, res) => {
 
    console.log(JSON.stringify(req.body))

    const user = new User({
      nick: req.body.nick,
      passw: req.body.password,
      email: req.body.email,
      admin: true,
      createdAt: req.serverTime,
      updatedAt: req.serverTime,
    });

    user
      .save()
      .then( (user) => {
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, user);
      })
      .catch( (e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      })

  })

  app.use("/users", router);

}
module.exports = routes;
