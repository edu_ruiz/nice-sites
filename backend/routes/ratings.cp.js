const express = require("express");
const math = require("mathjs")
const { faker } = require('@faker-js/faker');
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Category } = require("../models/Category");
const { Rating } = require("../models/RatingModel");
const { User } = require("../models/UserModel");
const { Url } = require("../models/URLModel");
const { Profile } = require("../models/ProfileModel");
const { Certificate } = require("../models/CertificateModel");

const routes = (app) => {
  const router = express.Router();
  //const user = new URL();

  // SEED DB

  // Agregar middleware y rutas completas, con :id param común
    router.get("/", (req, res) => {
        Rating.find({})
        .then((rows) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });// */
    })

    router.post("/", async(req, res) => {
        const date = new Date();

        // Manejar bien las Promise, dan resultados inesperados
        // ENCADENAR PROMISES ???

        const last = await Certificate.findOne({'resourceId': req.body.resourceId, 'closedAt': {$lt: date}, 'stage' : {$in:['closed','locked']}}).sort({'closedAt': -1});
        let cert = await Certificate.findOne({'resourceId': req.body.resourceId, 'closedAt': {$gt: date}, 'stage' : {$in:['opened']}});     
        let levelIncrease = 0;
        let lastAverage = [{}];

        if( last && last._id ) {
            lastAverage = await Rating.aggregate([
                { $match: {'certificateId': last._id } },
                { $group: {_id: '$certificateId', average: {$avg: {$avg: '$contents', $avg: '$ux'} }}},
                { $project: {
                    average: {$trunc: ['$average', 2]}
                }}
            ])
        }
        else {
            lastAverage[0].average = 5.0;
        }
        

        if ( lastAverage[0].average < 5 ) {
            levelIncrease = -0.1;
        }
        else {
            levelIncrease = 0.1;
        }

        let oldRating = {};
        let newLevel = 1;
        if ( last && last.level ) {
            newLevel = parseFloat(last.level + levelIncrease).toPrecision(2);

        }
        
        const newCert = new Certificate({
            'resourceId': req.body.resourceId,
            'level': newLevel,
            'openedAt': date,
            'closedAt': date.setMonth(date.getMonth() + 3),
            'stage': 'opened',
            'remaining': Math.pow(10, Math.floor(newLevel))/10,
            'scores': {},
            'nft': {}
        })
        let rating = {};
//console.log("FORM: " + rating)
        if ( cert ) {  
console.log("REMAINING :" + cert.remaining)
            oldRating = await Rating.findOne({'userId': req.body.userId, 'certificateId': cert._id})  
        
            rating = new Rating({
                'userId': req.body.userId,
                'resourceId': req.body.resourceId,
                'certificateId': cert._id,
                'contents': req.body.contents,
                'ux': req.body.ux,
                'contentsTag': req.body.contentsTag,
                'uxTag': req.body.uxTag,
                'createdAt': date
            });    

            if( ! oldRating || ! oldRating.userId ) {
            // consultar si hay remaining
                if( cert.remaining > 0 ) {
                    //console.log("remaining: " + cert.remaining)
                    let remaining = cert.remaining;
                    remaining -= 1;
                    rating
                        .save()
                        .then( (response) => {
                            console.log("Response: " + response)
                            // UPDATE remaining
                            // // FALTA RESOLVER REMAINING
                            Certificate.updateOne({_id: cert._id}, {$set: {remaining: remaining}})
                            serverResponses.sendSuccess(res, messages.SUCCESSFUL, response);
            
                        })
                        .catch( (e) => {
                            console.log(e)
                        }) 
                    
                }
                else {
                    // cerrar certificate y abrir nuevo, agregar nuevo Rating y actualizar
                    // update
                    
                    const locked = await Certificate.updateOne({_id: cert._id}, {$set: {stage: 'locked'}})
                    
                    cert = await newCert.save();
                    if(cert) {
                        console.log("closed: " + locked._id)
                        if ( true) {

                            
                        }
                        console.log("opened: " + cert._id)
                        rating.certificateId = cert._id;
                        let remaining = cert.remaining;
                        remaining -= 1;
                        rating
                            .save()
                            .then( (response) => {
                                console.log("Response: " + response)
                                // FALTA RESOLVER REMAINING

                                Certificate.updateOne({_id: cert._id}, {$set: {remaining: remaining}})
                                serverResponses.sendSuccess(res, messages.SUCCESSFUL, response);
            
                            })
                            .catch( (e) => {
                                console.log(e)
                            }) 
                    }
                    
                }
            }
            else {
                console.log("prev there: " + oldRating)
                //serverResponses.sendSuccess(res, messages.SUCCESSFUL, oldRating);
                Rating
                    .updateOne(
                        {'certificateId': cert._id, 'userId': req.body.userId},
                        {$set: {
                            'userId': req.body.userId,
                            'certificateId': cert._id,
                            'contents': req.body.contents,
                            'ux': req.body.ux,
                            'contentsTag': req.body.contentsTag,
                            'uxTag': req.body.uxTag,
                            'updatedAt': new Date()
                        }}
                    )
                    .then( (response) => {
                        // En este caso NO ALTERAR REMAINING
                        serverResponses.sendSuccess(res, messages.SUCCESSFUL, response);
    
                    })
                    .catch( (e) => {
                        console.log(e)
                    }) 
            }
            
        }
        else {
            cert = newCert.save();
            if (cert) {
                rating = new Rating({
                    'userId': req.body.userId,
                    'resourceId': req.body.resourceId,
                    'certificateId': cert._id,
                    'contents': req.body.contents,
                    'ux': req.body.ux,
                    'contentsTag': req.body.contentsTag,
                    'uxTag': req.body.uxTag,
                    'createdAt': date
                });  
                rating.certificateId = cert._id;
                let remaining = cert.remaining;
                remaining -= 1;
                rating
                .save()
                .then( (response) => {

                    Certificate.updateOne({_id: cert._id}, {$set: {remaining: remaining}})
                    console.log("Response: " + response)
                    // FALTA RESOLVER REMAINING
                    serverResponses.sendSuccess(res, messages.SUCCESSFUL, response);

                })
                .catch( (e) => {
                    console.log(e)
                }) 
                }
        }
        console.log("post-remaining :" + cert.remaining)
        
        // DB ops previas a respuesta: 
        //sumar un voto a la cuenta particular de resource y a la del certificate restar en remaining

        // DB crontab: 
        // -eliminar duplicados stage=enabled (copiar diferencias antes de borrar), 
        // -actualizar medias, 
        // -ajustar errores sumas de votos en resources, y remaining, periódicamente (semestral, quincenal)
        // -totales de los certificados finalizados o en cuarentena (locked), y una vez hecho, cambiar stage='closed'
    })

  // All Ratings for given resourceId
  router.get("/for/:id", async (req, res) => {

    const cert = await Certificate.findOne({'resourceId': req.params.id});

//console.log(cert)
     Rating
    .find({'certificateId': cert._id})
    .then((rows) => {
        //console.log(rows)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

  // count all Ratings for a given resource
  router.get("/for/:id/count", async (req, res) => {

    const certs = await Certificate.find({'resourceId': req.params.id}, {'_id' : 1}).distinct('_id');
     
    Rating
        .find({'certificateId': {$in: certs}})
        .countDocuments()
        .then((counts) => {
            console.log(certs) ;
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, counts);

      })
      .catch((e) => {
        
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
})

// average for given resource :id, all ratings any certificate
router.get("/for/:id/average", async (req, res) => {

    const certs = await Certificate.find({'resourceId': req.params.id},{'_id':1}).distinct('_id');
    
    Rating.aggregate([
        { $match: {'certificateId': {$in: certs} } },
        { $group: {_id: '$resourceId', average: {$avg: {$avg: '$contents', $avg: '$ux'} }}},
        { $project: {
            average: {$trunc: ['$average', 2]}
        }}
    ])
    .then((counts) => {
//console.log(counts)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, counts);

    })
    .catch((e) => {
        
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    });
})


// average for given resource :id, only within the current Certificate opened
    router.get("/for/:id/current/average", async (req, res) => {
        const date = new Date();

        // Tal vez estas operaciones se pueden hacer por terminal en crontab,
        // pensar cuáles van a intervenir en el UI 

        const cert = await Certificate.findOne({'resourceId': req.params.id, 'closedAt': {$lt: date}});
        //console.log("NEW DATE: " + date + "res DAte: " + cert.closedAt)     
        Rating.aggregate([
            {$match: {'certificateId': cert._id} },
            { $group: {_id: cert._id, average: {$avg: {$avg: '$contents', $avg: '$ux'} }}},
            { $project: {
                average: {$trunc: ['$average', 2]}
            }}
        ])
        .then((counts) => {
    //console.log(counts)
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, counts);

        })
        .catch((e) => {
            
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });
    })

  // count ratings for given :id, only within the current Certificate opened
  router.get("/for/:id/current/count", async (req, res) => {
    const date = new Date();
    const cert = await Certificate.findOne({'resourceId': req.params.id, 'closedAt': {$gt: date}, 'stage': 'opened'});
    
    Rating.aggregate([
        {$match: {'certificateId': cert._id} },
        { $group: {_id: cert._id, count: {$sum: 1} }}
      ])
      .then((counts) => {
//console.log(counts)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, counts);

      })
      .catch((e) => {
        
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
})

// To know if given user has previous|duplicated ratings within current certificate for given resource
router.get("/for/:id/current/by/:user", async (req, res) => {
    const date = new Date();

    const cert = await Certificate.findOne({'resourceId': req.params.id, 'closedAt': {$gt: date}, 'stage': 'opened'});
    
    if( cert ) {
        Rating.findOne({'userId': req.params.user, 'certificateId': cert._id})
        .then((row) => {
  console.log(row)
          serverResponses.sendSuccess(res, messages.SUCCESSFUL, row);
  
        })
        .catch((e) => {
          
          serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });
    }
    
})

// All ratings for resource given, group and count by different certificateId
// ESTO Sería más una función de certificates, calcular votos, sumas y medias por recurso??

router.get("/for/:id/certificates/count", async (req, res) => {

    const certs = await Certificate.find({'resourceId': req.params.id}, {'_id' : 1}).distinct('_id');
    
    // Añadir datos: Media de cada certificado, level y fecha de cierre
    
    Rating.aggregate([
        { $match: {'certificateId': { $in: certs }} },
        { $group: {_id: '$certificateId', count: {$sum: 1} }}
      ])
      .then((counts) => {
        console.log(certs) ;
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, counts);

      })
      .catch((e) => {
        
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
})
// FAKER duplica votos en el mismo resultado (aprovechar para probar scripts de limpieza?)
// De momento, utilizar como está para desarrollo de reglas, cálculos y muestra

  // All ratings by given user
  router.get("/by/:id", (req, res) => {
    Rating
    .find({'userId': req.params.id})
    .then((rows) => {
        //console.log(rows)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
    // PRIMERO sin restricciones de tiempo, validez, etc
  })

  // Get previous ratings from same user for same certificate
  // (Validation before insert)
  router.get("/by/:id/for/:idf", (req, res) => {
    Rating.find({})
    .then((rows) => {
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    });// */

    // Global mean, all (valid) ratings are included, no time params
    router.get("/for/:id/mean", (req, res) => {
        Rating.find({})
        .then((rows) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });// */
    })
})



  app.use("/ratings", router);

}
module.exports = routes;
