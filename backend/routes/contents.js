const express = require("express");

const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Content } = require("../models/ContentModel");



const routes = (app) => {
    const router = express.Router();

    router.get("/verify", (req,res) => {
        let obj = {};
//console.log("query")
        Content
            .find({'page': 'verify'})
            .then((rows) => {
                //console.log("query contents verify");
               

                rows.map( (row) => {
                    switch (row.type) {
                        case 'mainHeader': obj.mainHeader=row.text; break;
                        case 'welcomeText': obj.welcomeText=row.text; break;
                        case 'listHeader': obj.listHeader=row.text; break;
                        case 'formHeader': obj.formHeader=row.text; break;
                        case 'success': obj.success=row.text; break;
                        case 'aborted': obj.aborted=row.text; break;
                        default: obj.unknown=row.text;
                    }
                })

                serverResponses.sendSuccess(res, messages.SUCCESSFUL, obj);
            })
            .catch((e) => {
                console.log(e)
                serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
            }); // */
    })

    app.use("/contents", router);
}

module.exports = routes;