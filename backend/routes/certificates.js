const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Rating } = require("../models/RatingModel");
const { Certificate } = require("../models/CertificateModel");

const routes = (app) => {
  const router = express.Router();

    router.get("/", (req, res) => {
        Rating.find({})
        .then((rows) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });// */
    })

  // All Certificates for given resourceId
  router.get("/for/:id", async (req, res) => {

    const cert = await Certificate.findOne({'resourceId': req.params.id});

//console.log(cert)
     /*Rating
    .find({'certificateId': cert._id})
    .then((rows) => {
        //console.log(rows)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

  // just the one currently opened, and accepting new ratings
  router.get("/for/:id/current", async (req, res) => {

    const cert = await Certificate.findOne({'resourceId': req.params.id});

//console.log(cert)
   /*  Rating
    .find({'certificateId': cert._id})
    .then((rows) => {
        //console.log(rows)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

// FAKER duplica votos en el mismo resultado (aprovechar para probar scripts de limpieza?)
// De momento, utilizar como está para desarrollo de reglas, cálculos y muestra

  // Last closed process, to check recent scores
  router.get("/for/:id/last", (req, res) => {
    Rating
    .find({'userId': req.params.id})
    .then((rows) => {
        console.log(rows)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
    // PRIMERO sin restricciones de tiempo, validez, etc
  })

  // Mean by partials for resourceId, all certificates results included
  router.get("/for/:id/mean", (req, res) => {
    Rating.find({})
    .then((rows) => {
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, rows);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    });// */
})

  app.use("/certificates", router);

}
module.exports = routes;
