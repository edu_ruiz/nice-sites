const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
// const { User } = require("../models/UserModel");
const { Url } = require("../models/URLModel");

// ROUTES PATH: /search/*
const routes = (app) => {
  const router = express.Router();

  const getResourceFieldNames = () => {
    return (
      {
        'source': 'properties.source' ,
        'section': 'properties.section',
        'parental': 'properties.parental',
        'education': 'properties.education',
        'updated': 'properties.updated',
        'title': 'contents.title',
        'text': 'contents.text',
        'subject': 'contents.subject',
        'keywords': 'contents.keywords',
        'certificates': 'feedback.certificates',
        'quality': 'feedback.quality',
        'updatedAt': 'timestamps.updatedAt',
        'verifiedAt': 'timestamps.verifiedAt',
        'suffix': 'detail.suffix',
        'extension': 'detail.extension',
        'domain': 'detail.domain',
        'stage': 'stage'
      }
    )

  }

  

  router
    .get("/", (req, res) => {
// ADAPTAR query a namespace 
   /* const sections = req.query.section;
    const sources = req.query.source;
    const searchBy = req.query.searchBy;
    //const matchOption = req.query.matchOption;
    const parental = req.query.parental; */

    let searchBy = 'text';
    const filterObj = {};
/// REVISAR !!! Búsqueda no resuelve bien, 
    const fieldNames = getResourceFieldNames();
    const queryKeys = Object.keys(req.query);
    queryKeys.map( (param, i) => {
//console.log("query: " + queryKeys)
// ELIMINAR field results del query

      if( param !== 'matchOption' && param !== 'textInput' && param !== 'searchBy' && param !== 'results') {
        if( req.query[param] ) {
          filterObj[fieldNames[param]] = { $in: req.query[param] };
        }
        else {
          filterObj[fieldNames[param]] = {$not: { $in: [undefined,null,''] }};
        }
      }
      else if( param === 'searchBy') {
        if( req.query.searchBy[0] === 'url') {
          searchBy = 'url';
        }
        else {
          searchBy = `contents.${req.query.searchBy[0]}`;
        }
      }
    });
//console.log("extended")
    let regExp = RegExp('');

    if ( req.query.matchOption[0] === 'exact' ) {
      regExp = RegExp( `.*${req.query.textInput}.*$` , 'i') ;
    }
    else if ( req.query.matchOption[0] === 'free' ) {

      console.log('match: ' + searchBy + ' ' + req.query.textInput)
      const words = 
            req.query.textInput
            .toLowerCase()
            .replaceAll(/[^a-z0-9\s]/g, ' ')
            .replaceAll('  ', ' ')
            .trim()
            .split(' ')

        const str = `(${words.toString().replaceAll(',', '|')})`;

        regExp = RegExp(str, 'gi')
    }

    console.log(filterObj)
    Url.find(
        filterObj,
        { __v: 0 }
      )
      .regex( searchBy, regExp)
      .then((urls) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);
//console.log(urls)
      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      }); // */
  });


  // Feed MatchSuggest Component with Values
  router.get("/texts", (req, res) => {

    Url.find(
      {},
      {'text' : 1})
      .sort({'title': 'asc'})
      .then((titles) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, titles);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  router.get("/titles", (req, res) => {

    Url.find(
      {},
      {'title' : 1})
      .sort({'title': 'asc'})
      .then((titles) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, titles);

      })
      .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  // To check if an url exists
  router.get("/url", (req, res) => {

    Url.findOne(
      { 'url': req.query.url },
      { 'url': 1 })
      .then((url) => {
        
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, url);

      })
      .catch((e) => {
        
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });
  
  router.get("/count", (req, res) => {
//console.log(JSON.stringify(filterObject))
    // middleware filterObj que construye el objeto?
    /*const filterObj = {};
    const keys = Object.keys(req.query);
    //console.log(keys)
    keys.map( (key, i) => {
      filterObj[key] = req.query[key];
    })*/

//onsole.log(filterObject)
    const filterObject = {};

    const fieldNames = getResourceFieldNames();
    const queryKeys = Object.keys(req.query);
    queryKeys.map( (param, i) => {

      if( param !== 'fieldSelected') {
        filterObject[fieldNames[param]] = req.query[param];
      }
    })

    //console.log("FILTER: " + JSON.stringify(filterObject))

    Url.find(filterObject)
      .countDocuments()
      .then((count) => {
//console.log("count: " + count)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, count);

      })
      .catch((e) => {
//console.log(e)
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });

  // intentar obtener listado de counts
  router.get("/count/collect", (req, res) => {

    // middleware filterObj que construye el objeto?
    /*const filterObj = {};
    const keys = Object.keys(req.query);
    //console.log(keys)
    keys.map( (key, i) => {
      filterObj[key] = req.query[key];
    })*/

//console.log(filterObj)
    const filterObject = {};

    const fieldNames = getResourceFieldNames();
    const queryKeys = Object.keys(req.query);
    queryKeys.map( (param, i) => {

      if( param !== 'fieldSelected') {
        filterObject[fieldNames[param]] = req.query[param];
      }
    })

    Url.find(filterObject)
      .then((count) => {
//console.log(count)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, count);

      })
      .catch((e) => {
//console.log(e)
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });


  router.get("/count/values", (req, res) => {

    // PARA Reutilizar en todas las rutas, usar un middleware que asigne los namespaces a cada field
    // REPASAR escritura de middleware en Express 

    const filterObj = {};

    const fieldNames = getResourceFieldNames();
    const queryKeys = Object.keys(req.query);
    queryKeys.map( (param, i) => {
//console.log("query: " + queryKeys)
      if( param !== 'fieldSelected') {
        filterObj[fieldNames[param]] = req.query[param];
      }
    })

    const field = `$${fieldNames[req.query.fieldSelected]}`;   

    Url.aggregate([
        {$match: filterObj },
        { $group: {_id: field, count: {$sum: 1} }}
      ])
      .then((counts) => {
//console.log(counts)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, counts);

      })
      .catch((e) => {
        
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
      });
  });


  app.use("/search", router);
};
module.exports = routes;
