
const express = require("express");
const vt = require('node-virustotal');

const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");

const { Rating } = require("../models/RatingModel");
const { Url } = require("../models/URLModel");
const { Blacklist } = require("../models/BlacklistModel");

const { urlDetail } = require("../middleware/urlDetail");
const { checkUserBlacklist } = require("../middleware/checkUserBlacklist");
const { checkIsLogged } = require("../middleware/checkIsLogged");
const { serverTime } = require("../middleware/serverTime");
const { urlScan } = require("../middleware/urlScan");
const { checkIsAdmin } = require("../middleware/checkIsAdmin");

const { checkResourceBlacklist } = require("../middleware/checkResourceBlacklist");
const { checkIsResourceUnlocked } = require("../middleware/checkIsResourceUnlocked");

//ROUTES PATH: /resources/* 
const routes = (app) => {
    const router = express.Router();


    router
    .get("/verified", (req, res) => {
        let dt = new Date();
        dt = dt.setMonth(dt.getMonth() -1);
        let date = new Date(dt);
        //console.log("DATE: " + dt)
        let filter = {
            'stage': { $in: ['enabled', 'locked']},
            'timestamps.verifiedAt': { $not: {$in: [undefined, null, '']}}
        };
        //console.log("QUERY: " + JSON.stringify(req.query))
        if( Object.keys(req.query).length > 0 ) {
            const {section, subject, level} = req.query;
            //console.log("section: " + section)
            switch(section) {
                case 'recent': filter['timestamps.verifiedAt'] = {$gte: date }; break;
                case 'education': {
                    filter['properties.section'] = 'education';
                    filter['properties.education'] = level;
                    filter['properties.subject'] = subject;
                    break;
                }
                case 'parental': filter['properties.parental'] = subject; break;
                default: filter['properties.section'] = section; 
            }
        }

       //console.log("FILTER: " + JSON.stringify(filter))

        Url
        .find(
            filter,
            { '__v': 0, 'stage': 0, 'detail': 0  })
        .sort({ 'qualify': 'desc', 'timestamps.updatedAt': 'desc', 'feedback.ratings': 'desc', 'timestamps.createdAt': 'desc' })
        .limit(50) // 50 popular
        .then((urls) => {
            
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);     
        })
        .catch((e) => {
            serverResponses.sendError(res, messages.BAD_REQUEST, e);
        });
    })


    .get("/untrusted", (req, res) => {
    
        Url
        .find(
            {
                'timestamps.verifiedAt': {$in: [null,'']},
                'signature.verifiedBy': {$in: [null,'']},
                'stage': { $in: ['locked']}
            },
            { '__v': 0, 'signature': 0, 'stage': 0, 'detail': 0  })
        .sort({'timestamps.createdAt': 'asc' })
        .limit(10) // Paginate !! FALTA
        .then((urls) => {
            
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);     
        })
        .catch((e) => {
            serverResponses.sendError(res, messages.BAD_REQUEST, e);
        });
    })
    
    router
    .get("/by-ids", (req,res) => {
        console.log("Params ids: " + req.query.ids)
        Url.find( {'_id': {$in: req.query.ids}},
            {__v: 0}
        )
        .then((urls) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });

    })

    // CRUD 
    
    router
    .get("/", (req, res) => {

        Url
        .find( {'timestamps.verifiedAt': { $not: {$in: [undefined, null, ''] }}},{ __v: 0}
        )
        .then((urls) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, urls);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });
    })
    .get("/:id", (req, res) => {

        Url
        .findOne( {'_id': req.params.id},{ __v: 0}
        )
        .then((resource) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, resource);
        })
        .catch((e) => {
            serverResponses.sendError(res, messages.BAD_REQUEST, "Bad result");
        });
    })
    .put("/", checkIsLogged,(req, res) => {
//  checkUserBlacklist, checkResourceBlacklist, checkIsResourceUnlocked, 
        //console.log(JSON.stringify(req.body));

        /*const params = JSON.parse(JSON.stringify(req.body), function (key, value) {
            if(key !== 'id' && key !== 'field') {
                return value;
            }
        }) // */

        const date = new Date();
        const params = {};

        for( let key in req.body) {
            if(key !== 'id' && key !== 'field' && key !== 'verify'  && key !== 'reject' && key !== 'userId') {
                params[`${req.body.field}.${key}`] = req.body[key];
            }
        }

        if( req.body.verify ) {

            params[`timestamps.verifiedAt`] = date;
            params[`signature.verifiedBy`] = req.body.userId;
            params[`stage`] = 'enabled';
        }
        else if (req.body.reject) {
            params[`timestamps.verifiedAt`] = null;
            params[`signature.verifiedBy`] = req.body.userId;
            params[`stage`] = 'delete';
        }

        params[`timestamps.updatedAt`] = date;
        params[`signature.updatedBy`] = req.body.userId;

        console.log(params)
        Url.updateOne(
            {_id : req.body.id },
            {$set: params }
        )
        .then( (result) => {
            
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, result.acknowledge);  
        })
        .catch( (e) => {
            serverResponses.sendError(res, messages.BAD_REQUEST, "Something was wrong");
        })
    })
    // checkIsLogged, checkUserBlacklist, checkResourceBlacklist, 
    .post("/", checkIsLogged, checkUserBlacklist, checkResourceBlacklist, (req, res) => {
        // checkResourceBlacklist,
        try {
        
        const date = new Date();
        const url = req.body.url;
        let parental = req.body.parental;
        let education = req.body.education;
        let section = req.body.section;
        //let spaced = req.body.title.split(' ')
        //let words = spaced.split('\s');
        //console.log(spaced)
        const detail = urlDetail(url);
        const keywords= req.body.keywords.split(',');

        if( req.body.section !== 'education' ) {
            education = null;
        }

        if( ! detail.path || ! detail.file ) {
            section = 'sites';
        }

        if( detail.suffix === 'onion' ) {
            parental = 'adult';
        }

        const resource = new Url ({
        url : req.body.url,
        detail: detail,
        properties: {
            section: section,
            source: req.body.source,
            parental: parental,
            education: education,
            updated: req.body.updated
        },
        contents: {
            title: req.body.title,
            text: '',
            subject: req.body.subject,
            keywords: keywords
        },
        feedback: {
            certificates: [],
            ratings: 0,
            contents: req.body.contents,
            ux: req.body.ux,
            contentsTag: req.body.contentsTag,
            uxTag: req.body.uxTag
        },
        timestamps: {
            verifiedAt: null,
            updatedAt: date,
            createdAt: date
        },
        signature: {
            userId: req.body.userId,
            verifiedBy: null
        },
        stage: 'locked'
        })

        resource
            .save(resource)
            .then((result) => {
                serverResponses.sendSuccess(res, messages.SUCCESSFUL , result);
                //console.log(result)
            })
            .catch((e) => {
                serverResponses.sendError(res, messages.BAD_REQUEST, e.error);
                //console.log(e.errors )
            }); // */
        }
        catch (e) {
            serverResponses.sendError(res, messages.BAD_REQUEST, e.error);
        }
    })
    .delete("/", checkIsLogged, checkIsAdmin, checkUserBlacklist, (req, res) => {
        
        //cambiar o añadir middleware IsResource
        const resourceId = req.query.resourceId;
        
        Url
            .deleteOne({_id: resourceId})
            .then( (data) => {
                serverResponses.sendSuccess(res, messages.SUCCESSFUL , data);
            })

    })

    app.use("/resources", router);

};
module.exports = routes;

