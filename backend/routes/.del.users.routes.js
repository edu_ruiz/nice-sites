const express = require("express");
const userController = require("../.del.controllers/UserController");


const routes = (app) => {

    const router = express.Router();

    router
        .get("/", userController.getAllUsers)
        .get("/:id", userController.getUserById)
        .post("/", userController.createUser)
        .put("/:id", userController.updateUser)
        .delete("/:id", userController.deleteUser)
}

module.exports = routes;