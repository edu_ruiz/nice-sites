const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");
const { Wish } = require("../models/WishModel");

const routes = (app) => {
  const router = express.Router();

  // Agregar middleware y rutas completas, con :id param común
    router.get("/", (req, res) => {
        Wish.find({'scope':'public'})
        .then((wishes) => {
            serverResponses.sendSuccess(res, messages.SUCCESSFUL, wishes);
        })
        .catch((e) => {
            serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
        });// */
    })

  // Adaptar ruta a parámetro común y a middlewares EXPRESS Docs 
  router.get("/:id", (req, res) => {
   
    // Usar populate(), agregar Profile
    Wish.findOne({'_id': req.params.id})
    .populate('list')
    .then((list) => {
        //console.log(user)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

  router.get("/:id/list", (req, res) => {
   
    // Usar populate(), agregar Profile
    Wish.findOne({'_id': req.params.id},{'list': 1})
    .distinct('list')
    .then((list) => {
        //console.log(list)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

  router.put("/:id", (req, res) => {
   
    // Usar populate(), agregar Profile
    Wish.update({'_id': req.params.id},
    {$set: {'list': req.query.list}}
    )
    .then((list) => {
        //console.log(user)
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, list);
    })
    .catch((e) => {
        serverResponses.sendErrorList(res, messages.BAD_REQUEST, e.errors);
    }); // */
  })

  app.use("/wishes", router);

}
module.exports = routes;
