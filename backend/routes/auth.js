const express = require("express");
const session = require("express-session");
const serverResponses = require("../utils/helpers/responses");

const messages = require("../config/messages");
const { User } = require("../models/UserModel");
const { checkIsLogged } = require("../middleware/checkIsLogged");

const routes = (app) => {
    const router = express.Router();

    router.post('/login', async function (req, res) {
      

      try {
        let user = {};

        if( req.session.user ) {
          console.log(req.session.user)
          user = await User.findOne(
            { 
              _id: req.session.user
            },
            { _v: 0, passw: 0 }  
  
          )
          //throw new Error("Already logged")
        }
        else {
          user = await User.findOne(
            { 
              email: req.body.email,
              passw: req.body.password
            },
            { _v: 0, passw: 0 }  
  
          )
        }

        if ( user ) {
        
          req.session.user = user._id;
          
          //console.log(res.session)
          serverResponses.sendSuccess(res, messages.SUCCESSFUL, user);
        
        }
        else {
          throw new Error("User credentials not found")
        }
        
        
      }
      catch(e) {
        console.log(e)
        serverResponses.sendError(res, messages.BAD_REQUEST, e);
      }
      
    });

    router.get('/logout', checkIsLogged, function (req, res) {
        //console.log(req.session);
        //console.log(res)
        req.session.destroy();
        serverResponses.sendSuccess(res, messages.SUCCESSFUL, "logout");
      });

    app.use("/auth", router);

}
module.exports = routes;