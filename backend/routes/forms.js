const express = require("express");
const serverResponses = require("../utils/helpers/responses");
const messages = require("../config/messages");

// ROUTES PATH /forms/*
const routes = (app) => {
    const router = express.Router();
  
  router.get("/resource", (req, res) => {
    // PROBAR a obtener desde DB, crear tabla aparte, para poder modificar forms sin programar,
    // No tocar hasta insertar history o en cliente usando Context guardar últimos forms

    // se les puede agregar una etiqueta sobre scope (resource, register, login, rating, verify)
    const form = {
        inputs : [
        {
          'type': 'text',
          'label': 'title',
          'placeholder': 'The article title or yours improved',
          'size': 60,
          'maxLength': 60,
          'minLength': 10,
          'pattern': '[^\.\;]*',
          'required': true,
          'tooltipRequired': 'Field required'
        },
        {
          'type': 'text',
          'label': 'subject',
          'placeholder': 'main subject of this article',
          'size': 30,
          'maxLength': 30,
          'minLength': 5,
          'pattern': '[^\.]{10,30}',
          'required': true,
          'tooltipRequired': 'Field required'
        },
        {
          'type': 'text',
          'label': 'keywords',
          'placeholder': 'separated,commas',
          'size': 60,
          'maxLength': 60,
          'minLength': 8,
          'pattern': '.{8,60}',
          'required': true,
          'tooltipRequired': 'Field required'
        },
        {
          'type': 'text',
          'label': 'tag_contents',
          'placeholder': 'contents tag',
          'size': 10,
          'maxLength': 10,
          'minLength': 3,
          'pattern': '[a-z]*',
          'required': true,
          'tooltipRequired': 'Put an adjective about contents such as: truly, wide, big, technical'
        },
        {
          'type': 'text',
          'label': 'tag_ux',
          'placeholder': 'ux tag',
          'size': 10,
          'maxLength': 10,
          'minLength': 3,
          'pattern': '[a-z]*',
          'required': true,
          'tooltipRequired': 'Put an adjective about your user experience like: fast, modern, responsive'
        },
        {
          'type': 'number',
          'label': 'contents',
          'placeholder': '2.5',
          'size': 4,
          'maxLength': 2,
          'minLength': 1,
          'min': 1,
          'max': 5,
          'step': 0.5,
          'pattern': '\d{1,2}',
          'required': false,
          'tooltipRequired': 'From 1 to 5'
        },
        {
          'type': 'number',
          'label': 'ux',
          'placeholder': '2.5',
          'size': 4,
          'maxLength': 2,
          'minLength': 1,
          'min': 1,
          'max': 5,
          'step': 0.5,
          'pattern': '\d{1,2}',
          'required': false,
          'tooltipRequired': 'From 1 to 5'
        },
        {
          'type': 'email',
          'label': 'email',
          'placeholder': 'example@mail.com',
          'size': 30,
          'maxLength': 30,
          'minLength': 10,
          'pattern': '.*',
          'required': true,
          'tooltipRequired': 'Must fill if you are not a logged user'
        }
    ],
    selects: [
        'section',
        'source',
        'parental',
        'education',
        'subject',
        'updated'
    ],
    queryObject: {
      url: '',
      section: 'global',
      source: 'hosting',
      parental: 'adult',
      education: '',
      subject: '',
      updated: 'unknown',
      title: '',
      subject: '',
      keywords: '',
      contents: '2.5',
      ux: '2.5',
      tag_contents: '',
      tag_ux: '',
      email: '',
      userId: ''
    },
    errorsObject: {
      url: [],
      title: [],
      subject: [],
      keywords: [],
      contents: [],
      ux: [],
      tag_contents: [],
      tag_ux: [],
      email: [],
      userId: []
    }
    }
//console.log(inputs)
    serverResponses.sendSuccess(res, messages.SUCCESSFUL, form);
  })
    
    app.use("/forms", router);
}
module.exports = routes;  