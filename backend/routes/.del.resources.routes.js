const express = require("express");
const resourceController = require("../.del.controllers/ResourceController");


const routes = (app) => {

    const router = express.Router();

    router
    .get("/", resourceController.getAllResources)
    .get("/verified", resourceController.getAllVerifiedResources)
    .get("/:id", resourceController.getResourceById)
    .post("/", resourceController.createResource)
    .put("/:id", resourceController.updateResource)
    .delete("/:id", resourceController.deleteResource)

}

module.exports = routes;