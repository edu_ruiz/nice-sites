//customized from https://www.turing.com/kb/canvas-components-in-react

import { useRef, useEffect } from 'react';

export default function CanvasDraw (props) {
const canvasRef = useRef(null);
//console.log(history.location);
const draw = ctx => {
    ctx.fillStyle = '#000000'
    
    
    for ( let i=0; i < 570; i+=10) {
        ctx.beginPath();
        
        
        for (let j=Math.random() * 400; j < 500; j+=5) {
            const x = Math.floor(Math.random() * (j + 10) );
            ctx.lineTo(x+1, i);
            //ctx.moveTo(x, i);
            //ctx.lineTo(x+1, i);
            //ctx.fill();
             

            //const x = Math.floor(Math.random() * (j + 10) );
            //moveTo(x+1,i); 
            ctx.arc(x, i, 1.5 , Math.random() * Math.PI, Math.random()  * Math.PI,false);
            
            
        }
        ctx.fill()
    }
    
    //const circle = new Path2D();
    
  }


  const small = ctx => {
    ctx.fillStyle = '#000000'
    
    
    for ( let i=0 + Math.random() * 10; i < 60; i+=5) {
        ctx.beginPath();
        
        
        for (let j=20 * Math.random() * 10; j < 275; j+=10) {
            const x = Math.floor(Math.random() * (j + 5) );
            //ctx.fillStyle = '#000000'
            ctx.lineTo(x+60, i);
            //ctx.moveTo(x, i);
            //ctx.lineTo(x+1, i);
            //ctx.fill();
             
            //ctx.fillStyle = '#000000'
            //const x = Math.floor(Math.random() * (j + 10) );
            //moveTo(x+1,i); 
            ctx.arc(x, i, 1 , Math.random() * Math.PI, Math.random() * Math.PI,false);
            
            // temporizar hasta completar logo trazo a trazo, primero las verticales
           /* ctx.clearRect(30,10,20,60)
            ctx.clearRect(80,10,20,60)
            ctx.clearRect(130,10,20,60)
            ctx.clearRect(180,10,20,60)
            ctx.clearRect(230,10,20,60)
            ctx.clearRect(30,10,20,60)*/
        }
        ctx.fill()
    }
    
    //const circle = new Path2D();
    
  }

/*function drawTimer () {
    //console.log(canvasRef)

    const canvas = canvasRef.current;
    const context = canvas.getContext('2d');
    context.clearRect(0,0,600,600);
    draw(context);

}*/

/*useEffect(() => {

    const canvas = canvasRef.current;
    const context = canvas.getContext('2d');
    
    //drawTimer();
    //console.log(context)
  
  
  //props.width, props.height
  //context.fillRect(0, 0, "100px", "100px");

  draw(context);

  }, [draw]);*/

  useEffect(() => {
    //history.push("/resources/nfts")
    //console.log(history.location)
    const interval = setInterval(() => {
        // Perform some repeated action
        const canvas = canvasRef.current;
    const context = canvas.getContext('2d');
    context.clearRect(0,0,330,70);
        small(context);
    },100);
    //history.push("/resources/nfts")
    return () => {
        
        clearInterval(interval); // Clean up the interval
    };
    }, []);
  // props.width, props.height
    return (
        <>
            <div className="text-center border "><span></span>
            <canvas  ref={canvasRef} className="border mt-1" width="300px" height="60px" />
            </div>
            <h4>{`Searching...`}</h4>
        </>
    )
 

}