import { useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
//import fs from 'fs';

import { IoMdHeart } from "react-icons/io";
import { exportHtmlToDocx } from 'editor-to-word';
import { saveAs } from 'file-saver';
import exportFromJSON from 'export-from-json'
import ResourcesList from "../lists/ResourcesList";

import {WishListContext} from "../context/WishListContext";
import PageContainer from "../containers/PageContainer";
import ListHeader from "../elements/ListHeader";
import Button from "../buttons/Button";
import HelpContainer from "../containers/HelpContainer";
import Paragraph from "../elements/Paragraph";
import FormLabel from "../labels/FormLabel";
import InlineList from "../lists/InlineList";
import PageHeader from "../elements/PageHeader";

export default function ExportPage (props) {
    const wishList = useContext(WishListContext);
    const [resources, setResources] = useState([]);
    //const resourcesRef = useRef();
    //const [searchParams, setSearchParams] = useSearchParams();
    const [viewFormExport,setViewFormExport] = useState(false);
    
    const [viewMachinesForm,setMachinesForm] = useState(false);
    const [viewHumanForm,setHumanForm] = useState(false);
    const [humanMachines, setHumanMachines] = useState('machines');

    // PRIMERO: Básico: Export, Reset , 
    // Selector de campos a mostrar en la lista (modificar ResourceList,global, igual para todos los items)

    // eliminar item lo saca de la lista setNewList(newList.filter.),
    // Y lo pone en una lista de excluidos, de donde se pueden recuperar otra vez
    // si da tiempo: guardar, cargar, share social, votar
    // Exportar (ExportList?) muestra form: text, csv, xml, json, ipfs, firmado con el url nice-sites
    // El propio cliente maneja el proceso de crear el export ? Se puede crear el archivo o solo parsear?

    // Si se guarda con éxito, actualizar wishListContext con el valor de res.data.data


    //const {state} = useLocation();
    //const [formValues] = useOutletContext();

    // En lugar de obtener por el Outlet probar por contexto también
    const [formValues, setFormValues] = useState({
        matchOption: 'free',
        textInput: '',
        searchBy: 'title'
    })

    let handleExport = (e) => {
        // esta sería la opción de listas JSON de datos para aplicación
        
        let data = '';
        let list = [];
        const fileName = 'wishlist';
        const exportType = e.target.innerHTML;
        
        if( humanMachines === 'machines' ) {
            // FALTA agregar cabecera fecha y firma del doc
            data = resources;
            exportFromJSON({ data, fileName, exportType })
        }
        else {
            // Parser human-readable
            let item = '';    
            let list = '';

            // FALTA adaptar para poder dar diferentes formatos (extraer, añadir campos)
            resources.map( ( rsc, i ) => {
                item = 
                        `\tPage#${i+1}\n` +
                        `Section: ${rsc.section} Source: ${rsc.source}\n` +
                        `Title: ${rsc.title}\n` +
                        `Subject: ${rsc.subject}\n` +
                        `Sample text: ${rsc.text}\n` +
                        `URL: <a href=${rsc.url}>${rsc.url}</a>\n`; 
                list += item;
            }) 

            list += '\n\n\thttps://self-link-path.signed&dated\n';
            const file = new Blob([list], { type: 'text/plain;charset=utf-8' });
            saveAs(file, 'wishlist.text');

            //exportHtmlToDocx(list, 'wishlist'); // */
        }       
    }

    useEffect(() => {
        
    console.log('Wish Page mounted');
        /*setResources(
            () => {
                if ( state && state.results ) {
                    return state.results;
                }
                else {
                    
                    return {'title': 'Resource not found'};
                }
            }
        ) // */
        
            //searchParams.set(wishList);

            axios.get("/resources/by-ids", {params: {'ids': wishList}})
            .then( (res) => {
            //console.log(res)
            setResources(res.data.data);
            })
            .catch( (e) => {
            console.log(e)
            })//*/
           
            
          return () => console.log('Wish Page unmounting...');
      }, []); // <-- add this empty array here

      // Convertir a form stepped, cargar desde BD, txt abre menu human-machines
      /**
       * header={
                <PageHeader>
                    {'Export Page'}
                </PageHeader>
            }
       */
    return (
        <PageContainer
        header={
            <PageHeader styles="" >
                {'Export Page'}
            </PageHeader>
        }
            top={
                <div className="sticky top-20 space-y-3 bg-indigo-200 rounded-lg border-cyan-700 border-1 py-3">
                    <FormLabel label="machines format" styles="text-center mt-3" />
                    <InlineList styles="space-x-4">
                        <NavLink className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4" 
                            onClick={handleExport}>csv</NavLink>
                        <NavLink className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4" 
                            onClick={handleExport}>xml</NavLink>
                        <NavLink className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4" 
                            onClick={handleExport}>json</NavLink>
                    </InlineList>
                    <FormLabel label="human readable" styles="text-center" />
                    <InlineList styles="">
                        <NavLink className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4" 
                            onClick={handleExport}>txt</NavLink>
                    </InlineList>
                </div>
            }
            body={
                <div>
                {
                    ! resources || resources.length === 0 &&
                    <HelpContainer styles="p-2 m-2">
                        <Paragraph styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">
                            {`Empty list...? Don't forget to add some links, first` } <IoMdHeart className="text-red-500 text-3xl" />
                        </Paragraph>
                    </HelpContainer>
                }
                <div className=" my-10 lg:mt-10 bg-emerald-500 border border-1-solid blur:border-slated-600 rounded-t-lg hover:border-blue-300">
                    <ListHeader>
                        {`Wishlist (${resources ? resources.length : 0})`}
                    </ListHeader>
                    <ResourcesList
                        urls={resources} 
                        matchInput={formValues.textInput} 
                        matchOption={formValues.matchOption} 
                        searchBy={formValues.searchBy} 
                        viewText={false}
                    />
                </div>
                </div>
            }
        />
    )
}