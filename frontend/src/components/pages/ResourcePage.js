// Vista completa del modelo URL, redirigir aquí al añadir nuevo recurso
import { useState, useEffect } from 'react';
import { useLocation, useNavigate, Outlet } from 'react-router-dom';
import ListItem from "../lists/ListItem";

export default function ResourcePage (props) {
    const {state} = useLocation();
    let navigate = useNavigate();
    //const params = useParams();
    //console.log(history.location);
    const [resource, setResource] = useState( () => {
        if ( state && state.resource ) {
            return state.resource;
        }
        else {
            
            return {'title': 'Resource not found'};
        }
    })

    let handleDoubleClick = (index, item, e) => {
        // Añadir a lista de deseos    
    
    }

    useEffect(() => {
        //console.log('mounted');
        
        //history.push(`/resources/${resource._id}`) // + objeto de estado
        //console.log(history.location);
        //history.push(`/resources/${resource._id}`)
        return () =>''// console.log('unmounting...');
    }, [])  // <-- add this empty array here
    

    return (

        <>
            {
                resource && 
                <ListItem 
                    item={resource ? resource : props.item } 
                    index={props.index? props.index : 0 }
                    viewText={true}
                    handleDoubleClick={handleDoubleClick}
                    handleWishChange={()=>{}}
                />   
            }  

            <Outlet/>    
        </>
    );
}