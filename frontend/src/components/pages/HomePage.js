import { useState, useEffect, useContext } from "react";
import axios from "axios";
import ResourcesList from "../lists/ResourcesList";

import GridList from "../lists/GridList";
import PageContainer from "../containers/PageContainer";
import PageHeader from "../elements/PageHeader";
import ListHeader from "../elements/ListHeader";
import { SiteDataContext } from "../context/SiteDataContext";

export default function HomePage (props) {
    const siteData = useContext(SiteDataContext);
    //const [outlet] = useOutletContext();
    
    const [popular, setPopular] = useState();
    const [listHeader, setListHeader] = useState();

    const handleList = (name, subject, level) => {
        //console.log(name,subject,level)
        let query = {};
        if(name === '') {
            query = null;
        }
        else {
            query = { category: name, level: level, subject: subject };
        }
        axios.get(`/resources/verified`, query) 
        .then( (res) => {
            setPopular(res.data.data);
            setListHeader(name);
            //console.log(res.data.data);
        })
        .catch( (e) => {
            console.log("error: " + e);
        })
    }

    // Feed Popular Resources List and other sections while SearchBar value === ''
    useEffect(() => {
        //console.log('Home mounted');
        handleList('',null,null);

        
    }, []); // <-- add this empty array here

    // Añadir Listado Populares
    return (
       
        <PageContainer className=""
            header={
                <PageHeader>
                    {'Welcome to the other sites'}
                </PageHeader>
            }
            top={
                <GridList data={siteData.popularNav} handleClick={() => handleList} />
            }
           body= {
                <div className=" my-10 lg:mt-10 bg-emerald-500 border border-1-solid blur:border-slated-600 rounded-t-lg hover:border-blue-300">
                    <ListHeader>
                        {`Popular ${listHeader} links`}
                    </ListHeader>
                    <ResourcesList 
                        urls={popular} 
                        viewType={'tiny'}
                        scrollable={true}
                        error={'No links'}
                    />
                </div>
            }
        />
        
    )
}

// md:mx-5 mt-5 lg:mx-10 2xl:mx-30  