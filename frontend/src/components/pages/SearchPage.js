import { useContext, useEffect, useState } from "react";
import {useLocation, useOutletContext, NavLink} from "react-router-dom";
import ResourcesList from "../lists/ResourcesList";
import SteppedForm from "../forms/SteppedForm";
import { StepSearchContext } from "../context/StepSearchContext";
import { SearchContext } from "../context/SearchContext";
import HelpContainer from "../containers/HelpContainer";
import ListHeader from "../elements/ListHeader";
import PageContainer from "../containers/PageContainer";
import PageHeader from "../elements/PageHeader";
import Paragraph from "../elements/Paragraph";

export default function SearchPage (props) {
    const {state} = useLocation();
    //const [formValues] = useOutletContext();
    const [found] = useOutletContext();
    const [results,setResults] = useState();
    const [viewGuide, setViewGuide] = useState(false);
   const steppedSearch = useContext(StepSearchContext);
   const search = useContext(SearchContext);
   //const [filters, setFilters] = useState({})

    // AÑADIR Params navigate() use Navigate
    // Si hay coincidencias, se navegará aquí con unas props

    const handleStepsCollect = (results) => {
        setViewGuide(false);
        setResults(results);
        window.scrollTo(0,0)
    }

    const handleViewGuide = (e) => {

        setViewGuide(!viewGuide);
        //setResults([])
    }

    useEffect(() => {    
    //console.log('Search Page mounted');
        //setResults(formValues.results);
        //console.log(state.formValues)
        if ( search.results ) {

           /* setFilters((prev) => (
                {
                    ...prev, 
                    ['matchOption']: search.matchOption, 
                    ['textInput']: search.textInput, 
                    ['searchBy']: search.searchBy
                }
            )); // */

            setResults(search.results);
        }
        else {
            //setFilters((prev) => ({...prev, ['matchOption']: 'free', ['textInput']: '', ['searchBy']: 'title'}))
            if( state && state.results ) {
                setResults(state.results);
            }
            else {

                setResults([]);
            }
        }

        
        return () => '';//console.log('Search Page unmounting...');
    }, []); // <-- add this empty array here

    //state.formValues.textInput ? state.formValues.textInput : ''
    // state.formValues.matchOption ? state.formValues.matchOption : 
    // state.formValues.searchBy ? state.formValues.searchBy : 

    // Se podría anular algunas props? ahora van por SearchContext al listItem

    return (
        <PageContainer className=""
            header={
                <PageHeader>
                    {'Welcome to the other sites'}
                </PageHeader>
            }
            top={
                !viewGuide &&
                <HelpContainer styles="p-2 m-2">
                    <Paragraph styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">
                        {'Results were not as expected? You still could try our '}
                        <NavLink className="capitalize underline text-slate-500 hover:text-blue-500" onClick={handleViewGuide}>guided search</NavLink>
                    </Paragraph>
                </HelpContainer>
            }
            
            body={   
                viewGuide &&
                <SteppedForm divStyles="border border-2 border-cyan-700 rounded-lg p-5 m-3" handleStepsCollect={handleStepsCollect}/>       
            }
            bottom={
                results && results.length > 0 &&
                <div className=" my-10 lg:mt-10 bg-emerald-500 border border-1-solid blur:border-slated-600 rounded-t-lg hover:border-blue-300">
                
                    <ListHeader>
                        {`Results (${results.length})`}
                    </ListHeader>
                    <ResourcesList
                        urls={results} 
                        matchInput={'' } 
                        matchOption={'free'} 
                        searchBy={'title'} 
                        viewText={false}
                        error={'Error loading'}
                    />
                </div>

            }
             
        />
    )

    /*
    return (
        <>
            {
                results && results.length > 0 &&
                <>
                    <h3>{`Results (${results.length})`}</h3>
                    <ResourcesList
                        urls={results} 
                        matchInput={'' } 
                        matchOption={'free'} 
                        searchBy={'title'} 
                        viewText={false}
                        error={'Error loading'}
                    />
                </>

            }
            {

                <>
                    <p>
                        {'If results were not as expected, you could try our'}
                        <NavLink onClick={()=>setViewGuide(!viewGuide)}>guided search</NavLink>
                    </p>
                </>
            }
            {   
                viewGuide &&
                <>
                    <SteppedForm handleStepsCollect={handleStepsCollect}/>       
                </>
            }
            
        </>
    )*/
}