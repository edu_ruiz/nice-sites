import {useContext, useEffect, useState, useRef} from "react";
import axios from 'axios';
import 'react-folder-tree/dist/style.css';
import FolderTree from 'react-folder-tree';

import ResourcesList from "../lists/ResourcesList";
import { SiteDataContext } from "../context/SiteDataContext";
import Button from "../buttons/Button";
import PageContainer from "../containers/PageContainer";
import PageHeader from "../elements/PageHeader";
import ListHeader from "../elements/ListHeader";
import HelpBanner from "../containers/HelpBanner";
import ResourcesTree from "../lists/ResourcesTree";


export default function Directory (props) {

    const siteData = useContext(SiteDataContext);
    const [tree, setTree] = useState();
    const pathRef = useRef();
    const [collectCount, setCollectCount] = useState();
    const [queryObj, setQueryObj] = useState();
    const [results, setResults] = useState();

    const onTreeStateChange = (state, event) => {

        if( Object.keys(siteData).length > 0 ) {
            let obj = {};

            if( event.type === 'toggleOpen' ) {
                let path = '';
                
                // FALTA: recalcular totales ante cada cambio,
                // obtener nombres seleccionados, a partir de event.path array y de siteData
                // cambiar el switch por un método dinámico para obtener el query y el path.
                let keys = [];


                if ( event.path[0] === 2 ) {

                    keys = ['section', 'education', 'source', 'updated', 'parental'];
                }
                else {

                    keys = ['section', 'source', 'updated', 'parental'];
                }
                
                event.path.map( (index, i) => {
                    obj[keys[i]] = siteData[keys[i]][index]
                })

                Object.keys(obj).map( (key) => {
                    path += `/${key}`
                })

                path += '...';

                setQueryObj(obj);                

                console.log("QUERY: " + JSON.stringify(obj))
                //console.log("open: " + path)
                pathRef.current.innerHTML = path;
            }
            //console.log("state,event :" + JSON.stringify(state), event);
            
            axios
                .get("/search/count", {params: obj})
                .then( (res) => {
                    //console.log("count: " + res.data.data)
                    let count = res.data.data;
                    if( !res.data.data ) {
                        count = 0;
                    }
                    setCollectCount(count);
                })
                .catch( (e) => {
                    console.log(e);
                })
        } 
        
    }

    const handleCollect = (e) => {
        axios
        .get("/search/count/collect", {params: queryObj })
        .then( (res) => {
            setResults(res.data.data);
        })
        .catch( (e) => {
            console.log(e);
        })
    }

    // Init tree
    useEffect( () => {

        // Pasar a un context que se inicie nada más cargar la página o descargar el árbol ya calculado
        /*
        if( siteData && siteData.section ) {

            const rootNode = {
                name : 'section',
                isOpen : true,
                children : []
            }

            let {children} = rootNode;
            
            siteData.section.map( (cat, i) => {
                //console.log(cat)
                let parentalChild = [];

                siteData.parental.map( (par, i) => {
                    parentalChild.push({name: par, isOpen: false, children: []})
                })

                let updatedChild = [];

                siteData.updated.map( (upd, i) => {
                    updatedChild.push({name: upd, isOpen: false, children: parentalChild})
                })

                let sourceChild = [];

                siteData.source.map( (src, i) => {
                    sourceChild.push({name: src, isOpen: false, children: updatedChild})
                }) // 

                if( cat === 'education' ) {
                    let educationChild = [];

                    siteData.education.map( (edu, i) => {
                        educationChild.push({name: edu, isOpen: false, children: sourceChild})
                    })
                    children.push({name: cat, isOpen: false, children: educationChild})
                }
                else {
                    children.push({name: cat, isOpen: false, children: sourceChild})
                }

                
            })

            setTree(rootNode); 
        } // */

        

    }, []);

    const handleNodeChange = (e) => {

    }

    // Crear por mi cuenta un Tree que use el mismo objeto y ofrezca facilidad de estilo y animación

    return (
            <PageContainer
                header= {
                    <PageHeader>
                        {'Directory Search'}
                        
                    </PageHeader>
                }
                top= {
                    tree && !results && false &&
                    <>

                        <ResourcesTree
                            treeData={tree}
                            handleClick={handleNodeChange}
                        />

                        <div className="sticky top-20">
                            <Button 
                                label={`Collect(${collectCount})`} 
                                type="button" handleClick={handleCollect} 
                            />
                        </div>
                        <div className="m-8 font-bold align-items-center">
                            <strong className=""><span ref={pathRef}></span></strong>
                            <FolderTree
                                data={ tree }
                                onChange={ onTreeStateChange }
                                indentPixels={ 40 }
                                showCheckbox={ false }
                                readOnly={true}
                                initOpenStatus='custom'
                                className="m-0 p-0"
                            />
                        </div>
                    </>  
                }
                body={
                    results &&
                    <div>
                        <ListHeader>
                            {`Results (${results.length})`}
                        </ListHeader>
                        <ResourcesList
                            urls={results}
                            scrollable={true}
                            viewType={'tiny'}
                            error={'No links'}
                        />
                    </div>
                }

                footer={ false &&
                    
                    <HelpBanner timeOut={3000} />
   
                }
            />
    )
}