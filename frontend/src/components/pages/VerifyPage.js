import { useEffect, useState } from "react";
import axios from "axios";
import ResourcesList from "../lists/ResourcesList";
import PageContainer from "../containers/PageContainer";
import HelpBanner from "../containers/HelpBanner";
import PageHeader from "../elements/PageHeader";
import Paragraph from "../elements/Paragraph";
import ListHeader from "../elements/ListHeader";
import HelpContainer from "../containers/HelpContainer";

export default function VerifyPage (props) {
    const [contents, setContents] = useState();
    const [list, setList] = useState();
    const [resource, setReource] = useState();

    useEffect( () => {

        axios.get("/contents/verify")
            .then( (res) => {
                //console.log(res.data.data)
                setContents(res.data.data);
            })
            .catch( (e) => {
                console.log(e); 
            }) // */

        axios.get("/resources/untrusted")
            .then( (res) => {
                //console.log("UNTRUSTED :" + res.data.data.length + JSON.stringify(res.data.data) )
                setList(res.data.data);
            })
            .catch( (e) => {
                console.log(e)
            }) // */
    }, [])


    return (
    
        <>
        { contents ?
            <PageContainer
            header={<PageHeader>{contents.mainHeader}</PageHeader>}
            top={
                <HelpContainer styles="p-2 m-2">
                    <Paragraph styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">
                        {contents.welcomeText}
                    </Paragraph>
                </HelpContainer>
            }
            body={
                <div>
                    <ListHeader styles="bg-orange-500">
                        {contents.listHeader}
                    </ListHeader>
                    <ResourcesList
                        urls={list}
                        scrollable={true}
                        viewType={'tiny'}
                        error={'No links'}
                    />
                </div>
            }
            
            footer={ false ?
                <div>
                {
                    ! resource &&
                    <HelpBanner>
                    {`Please, click over 'Verify' section to select and verify an item`}
                    </HelpBanner>
                }
                </div>
                : ''
            }

            navAux={"search"}
            
        />
        : ''
        }  
        </>  
    )
}