
import { NavLink } from "react-router-dom";
import { LuArrowBigLeft } from "react-icons/lu";
import { MdHome } from "react-icons/md";
import { BiSolidFileExport } from "react-icons/bi";
import { MdOutlineSearch } from "react-icons/md";
import history from 'history/browser';

export default function NavBottom (props) {
    // top-0 left-0 w-16 h-full
//icon-list: grid h-full max-w-lg grid-cols-4 mx-auto 
    return (

        <nav>
        {
            <div className="fixed z-50 bottom-0 left-0 w-screen h-16 bg-white border-t border-gray-200 dark:bg-gray-700 dark:border-gray-600">
                <div className="grid h-full max-w-lg grid-cols-3 justify-center font-medium">
                    
                    <button onClick={history.back} className="inline-flex flex-col items-center justify-center px-5 hover:bg-gray-50 dark:hover:bg-gray-800 group">
                        <LuArrowBigLeft className="w-5 h-5 mb-2 text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" />
                        <span className="text-sm text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500">Back</span>
                    </button>
                    <NavLink to="/" className="inline-flex flex-col items-center justify-center px-5 hover:bg-gray-50 dark:hover:bg-gray-800 group">
                        <MdHome className="w-5 h-5 mb-2 text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" />
                        <span className="text-sm text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500">Home</span>
                    </NavLink>
                    <NavLink to={`/${props.navAux ? props.navAux : "export"}` } className="inline-flex flex-col items-center justify-center px-5 hover:bg-gray-50 dark:hover:bg-gray-800 group">
                        {
                            props.navAux && props.navAux === 'search' &&
                            <MdOutlineSearch className="w-5 h-5 mb-2 text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" />   
                        }
                        { ! props.navAux &&
                            <BiSolidFileExport className="w-5 h-5 mb-2 text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 20" />
                        
                        }
                        <span className="text-sm text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500">{props.navAux ? props.navAux : "export"}</span>
                    </NavLink>
                </div>
            </div>
        }
        </nav>
    )
}

