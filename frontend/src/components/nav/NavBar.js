import { useState } from "react";
import { NavLink  } from "react-router-dom";

export default function NavBar (props) {
    //const location = useLocation();
    const [styles, setStyles] = useState("");

    const handleActive = (isActive) => {

        if( isActive ) {
            setStyles("")
            return "underline underline-offset-4 text-slate-500/40 ";
        }
        else {
            setStyles("")
        }
    }

    const handleClick = (name) => {
        console.log(name)
        props.handleActive(name)
    }
    
    return (       
        props.navLinks.map( ( nav, i) => {
            //console.log(nav.path)
            return (
                <li key={i}
                    className={`${props.styles} ${styles} peer w-full pt-1 px-4 pb-2 xl:text-xl text-gray-700 border-gray-200 hover:bg-gray-100 dark:border-gray-600`}
                    
                > 
                    <NavLink
                        className={ ({isActive}) => handleActive(isActive)} 
                        to={nav.path}
                    >
                        <div className={`w-full`}>{nav.name}</div> 
                    </NavLink>
                </li>
            )
        })        
    )
    /**
     * 
     * nav.name === 'resources' ?
                        <span>
                            <button 
                                id="dropdown-resources-button" 
                                data-dropdown-toggle="dropdown-resources" 
                                className="flex items-center justify-between w-full text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 md:w-auto dark:text-white md:dark:hover:text-blue-500 dark:focus:text-white dark:border-gray-700 dark:hover:bg-gray-700 md:dark:hover:bg-transparent"
                                
                            >
                                {nav.name}
                                <svg className="w-2.5 h-2.5 ms-2.5" aria-hidden="false" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                                    <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 4 4 4-4"/>
                                </svg>
                            </button>
                            <div id="dropdown-resources" className="z-50 flex-col md:hidden font-normal bg-white divide-y divide-gray-100 rounded-lg shadow  dark:bg-gray-700 dark:divide-gray-600">
                                <ul className="py-2 text-sm text-gray-700 dark:text-gray-400" aria-labelledby="dropdown-resources-button">
                                    <li className="w-full">
                                        <a href="#" class="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Dashboard</a>
                                    </li>
                                </ul>
                                <div class="py-1">
                                    <a href="#" class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white">Sign out</a>
                                </div>
                            </div>
                        </span>
                        :
     */
}