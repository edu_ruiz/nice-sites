
export default function NavTabs (props) {

    // Props: Label, handler y estilo final (color, size)

    return (
        <div className="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
            <ul className="flex flex-wrap -mb-px">
                {props.children}
            </ul>
        </div>
    )
}



