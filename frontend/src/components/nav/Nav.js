
import { NavLink } from "react-router-dom";

export default function Nav (props) {
    

    return (
        <>
            <nav>
            {
                props.navLinks.map( ( nav, i) => {
                    //console.log(nav.path)
                    return (
                    <NavLink key={i}
                    className={ ({isActive}) => isActive ? "bg-yellow-500" : ''} 
                    to={nav.path}
                    >
                        {nav.name}
                    </NavLink>
                    )
                })
            }
            </nav>
        </>
    )
}