import React, { useState, useEffect, useContext, useRef } from "react";
import Button from "./Button";
//import "../../App.scss";

export default function ToggleButton (props) {

    // Intentar envolver el botón base para uso toggle (cambian los estilos en función de los switch)
    // Modifica estados focus y tiempo animación,...
    //

    return (
        <div className="inline-flex rounded-md shadow-sm">
            {props.children}
        </div>
    )
}