import React, { useState, useEffect, useContext, useRef } from "react";
import Button from "./Button";
//import "../../App.scss";

export default function ButtonGroup (props) {

    // ¿Agregar botones como children? 
    // Evita tantas props, solo usa el group como wrapper

    return (
        <div className="inline-flex rounded-md shadow-sm" role="group">
            {props.children}
        </div>
    )
}