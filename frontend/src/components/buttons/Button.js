import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function Button (props) {

    // Props: Label, handler y estilo final (color, size)

    return (
        <button 
            type={props.type}
            onClick={(e) => props.handleClick()}
            className={ `${props.styles} relative inline-flex items-center justify-center p-0.5 mb-1 me-2 overflow-hidden text-lg font-medium text-gray-900 rounded-lg group bg-gradient-to-br from-green-400  group-hover:from-green-400 group-hover:to-blue-600 hover:text-white dark:text-white focus:ring-4 focus:outline-none focus:ring-green-200 dark:focus:ring-green-800`}>
            <span className="relative px-3 py-1 transition-all ease-in duration-75 bg-white dark:bg-gray-900 rounded-md group-hover:bg-opacity-0">
                {props.label}
            </span>
        </button>
    )
}



