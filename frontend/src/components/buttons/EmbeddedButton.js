import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function EmbeddedButton (props) {

    // Props: Label, handler y estilo final (color, size)

    return (
        <button 
            type={props.type}
            onClick={(e) => props.handleClick()}
            className={ `${props.styles} flex-shrink-0 z-10 inline-flex items-center py-2.5 px-4 text-sm font-medium text-center text-gray-900 bg-gray-100 border border-gray-300 rounded-s-lg hover:bg-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 dark:bg-gray-700 dark:hover:bg-gray-600 dark:focus:ring-gray-700 dark:text-white dark:border-gray-600`}
        >
            <span className="hidden md:block md:ml-2 mr-2">{props.label}</span>
            <span className="block md:hidden">{props.children}</span>

        </button>
        
    
    )
}



