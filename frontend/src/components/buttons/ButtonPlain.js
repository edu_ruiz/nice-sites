
//import "../../App.scss";

export default function ButtonPlain (props) {

    // Props: Label, handler y estilo final (color, size)

    return (
        <button 
            type={props.type}
            onClick={(e) => props.handleClick}
            className={`${props.styles} py-2 md:px-3 mb-2 text-sm font-medium text-gray-900 focus:outline-none bg-white rounded-lg border border-cyan-600 hover:bg-gray-100 hover:text-blue-700 focus:z-10 focus:ring-4 focus:ring-gray-100 dark:focus:ring-gray-700 dark:bg-gray-800 dark:text-gray-400 dark:border-gray-600 dark:hover:text-white dark:hover:bg-gray-700`}
        >
            {props.label}
        </button>
    )
}


