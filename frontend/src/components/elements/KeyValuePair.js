
import IconOrLabel from "./IconOrLabel";
//import "../../App.scss";

export default function KeyValuePair (props) {

    // Props: Label, handler y estilo final (color, size)

    // pl-2 pr-1 py-0.5 md:pb-1
    // <span className={`${props.keyStyles} rounded-s-xl`}>{props.children}</span>
       // <span className={ `${props.valueStyles} bg-white  italic text-gray-400 rounded-e-xl`}
       //>
       //{props.value}</span>

    return (
        <div      
            onClick={(e) => props.handleClick()}
            className={ `inline-flex justify-center items-center my-1 `}
        >
            <IconOrLabel label={`${props.label}:`} styles={`${props.keyColors ? props.keyColors : "font-semibold text-gray-600"} ${props.keyStyles ? props.keyStyles : "text-base"} `}>
                {props.children}
            </IconOrLabel>
            
            <span 
                className={`${props.valueStyles} ${props.valueColors ? props.valueColors : 'bg-yellow-100 text-yellow-800 border-yellow-300' } ml-1 text-lg  font-medium px-2.5 border rounded dark:bg-gray-700 dark:text-yellow-300`}
            >
                {props.value}
            </span>

        </div>
        
    
    )
}



