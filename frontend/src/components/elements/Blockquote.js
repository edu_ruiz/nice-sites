
export default function Blockquote (props) {

    return (

        <blockquote className={`${props.styles} italic font-serif  dark:text-white`}>
            {props.children}   
        </blockquote>
        
    )
}