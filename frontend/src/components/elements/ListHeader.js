
export default function ListHeader (props) {

    return (

        <h3 className={`${props.styles} my-2 text-2xl font-bold text-center text-white text-shadow-2`}>
            {props.children}
        </h3>
                    
        
    )
}