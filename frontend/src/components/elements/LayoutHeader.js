
export default function LayoutHeader (props) {

    // text-center text-2xl md:text-3xl lg:text-5xl xl:text-5xl font-bold font-display
    return (

        <h2 className={`${props.styles} text-center text-2xl font-semibold`}>
            {props.children}
        </h2>
        
    )
}