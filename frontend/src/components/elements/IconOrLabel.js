
export default function IconOrLabel (props) {

    // Props: Label, handler y estilo final (color, size)

    return (
        <div 
            
            onClick={(e) => props.handleClick()}
            className={ ''}
        >
            <span className="inline-block md:hidden align-middle ">{props.children}</span>
            <span className={`${props.styles} hidden md:inline-block align-middle`}>{props.label}</span>
            

        </div>
        
    
    )
}



