
export default function PageHeader (props) {

    return (

        <h3 className={`${props.styles} text-center text-3xl lg:text-4xl font-semibold`}>
            {props.children}
        </h3>
        
    )
}