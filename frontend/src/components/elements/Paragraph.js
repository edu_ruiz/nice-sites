
export default function Paragraph (props) {

    return (
        <p className={`${props.styles} leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl`}>
            {props.children}
        </p>
        
    )
}