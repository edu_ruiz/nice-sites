
export default function HrefLink (props) {

    const handleClick = (e) => {
        e.preventDefault();
        props.handleClick(e);
    }

    return (
        
        <div className="overflow-scroll m-1 py-5">
            <a 
                onClick={handleClick} 
                href={props.url} rel="nofollow"
                className={`truncate underline underline-offset-3 text-indigo-700`}
            >
                {props.url}
            </a>
        </div>
        
    )
}