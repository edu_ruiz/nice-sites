
export default function DataPill (props) {

    // Props: Label, handler y estilo final (color, size)

    return (
        <span
            
            
            className={ `${props.styles} bg-white pb-1 px-3 py-0 border border-1 border-gray-200 text-gray-400 rounded-e-xl`}
        >
            {props.children}

        </span>
        
    
    )
}



