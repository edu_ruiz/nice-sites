import React, {useEffect, useState, useRef, useContext} from "react";
import "../../App.scss";
import {SearchContext} from "../context/SearchContext";
import Blockquote from "../elements/Blockquote";


export default function HighlightMatches(props) {
    const searchData = useContext(SearchContext);

    // NECESITA Context(), guardar objeto matchOption, input y text desde el padre para alimentarlo
    // Obtener de un Context el item selected + parámetros search ??!!

    // CONVERTIR A FUNCIÓN REACT, ahora es un bloque de función js
    // Aunque el return funciona... comprobar estilos in situ

    // matchOption === 'exact'

    if ( searchData.matchOption === 'exact' ) {
        // Split on highlight term and include term into parts, ignore case
        const parts = props.text.split(new RegExp(`(${searchData.textInput})`, 'gi'));
        return (<span> { parts.map((part, i) => 

            part.toLowerCase() === searchData.textInput.toLowerCase() ? <mark className="highlight" key={i}>{part}</mark> : <span key={i}>{part}</span> )

        } </span>);
    }
    else {
        // input troceado por palabras
        // Este mismo método en el servidor, para obtener la búsqueda seleccionada
        const words = 
            searchData.textInput
            .toLowerCase()
            .replaceAll(/[^a-z0-9\s]/g, ' ')
            .replaceAll('  ', ' ')
            .trim()
            .split(' ')

        const str = `(${words.toString().replaceAll(',', '|')})`;

        const parts = props.text.split(new RegExp(str, 'gi'));
        
        return (
            <span> 
                { parts.map((part, i) => 

                    words.includes(part.toLowerCase()) 
                    ? 
                        <mark className="highlight" key={i}>{part}</mark>
                    : 
                        <span className={props.styles} key={i}>{part}</span> )

                }
            </span>);

    }  
}

