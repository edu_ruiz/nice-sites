
export default function getFilterCategories () {
    return (

        {
            sections: [
                'global',
                'news',
                'science',
                'history',
                'home',
                'arts',
                'music',
                'sports',
                'gaming',
                'coding'
            ],
            sources: [
                'news',
                'blog',
                'community',
                'hosting',
                'bussiness',
                'personal',
                'wiki'
            ],
            quality: [
                'contents',
                'ux',
                'parental'
            ],
            levels: [
                'aceptable',
                'buena',
                'muy buena'
            ],
            search: [
                'title',
                'text',
                'url',
                'keywords',
                'subject'
            ]
        }
    )
}