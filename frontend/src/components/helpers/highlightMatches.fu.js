import React, {useEffect, useState, useRef, useContext} from "react";
import "../../App.scss";
//import {HighlightContext} from "../context/HighlightContext";


export default function highlightMatches(text, input, matchOption) {
   // const context = useContext(HighlighContext);

    // NECESITA Context(), guardar objeto matchOption, input y text desde el padre para alimentarlo
    // Obtener de un Context el item selected + parámetros search ??!!


    // matchOption === 'exact'

    if ( matchOption === 'exact' ) {
        // Split on highlight term and include term into parts, ignore case
        const parts = text.split(new RegExp(`(${input})`, 'gi'));
        return (<span> { parts.map((part, i) => 

            part.toLowerCase() === input.toLowerCase() ? <mark className="bg-warning" key={i}>{part}</mark> : <span key={i}>{part}</span> )

        } </span>);
    }
    else if ( matchOption === 'free' ) {
        // input troceado por palabras
        // Este mismo método en el servidor, para obtener la búsqueda seleccionada
        const words = 
            input
            .toLowerCase()
            .replaceAll(/[^a-z0-9\s]/g, ' ')
            .replaceAll('  ', ' ')
            .trim()
            .split(' ')

        const str = `(${words.toString().replaceAll(',', '|')})`;

        const parts = text.split(new RegExp(str, 'gi'));
        
        return (<span> { parts.map((part, i) => 

            words.includes(part.toLowerCase()) ? <mark className="highlight" key={i}>{part}</mark> : <span key={i}>{part}</span> )

        } </span>);

    }  
}

