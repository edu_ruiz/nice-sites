// From www.geeksforgeeks.org/how-to-share-state-across-react-components-with-context/?ref=ml_lbp

import React, { useState } from "react";
 
export const Context = React.createContext();
export const ContextProvider = ({ children }) => {
    const [suggest, setSuggest] = useState(0);

    return (
        <Context.Provider value={{ suggest, setSuggest }}>
            {children}
        </Context.Provider>
    );
};