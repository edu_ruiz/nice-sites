import React, {useContext, useState, useEffect} from "react";
import { Outlet } from "react-router-dom";
import axios from "axios";
import "../../App.scss";
import { SiteDataContext } from "../context/SiteDataContext";
import Nav from "../nav/Nav";


export default function FormsLayout (props) {

    const [navLinks, setNavLinks] = useState([])

    const getLinks = () => {
        axios.get("/index/nav/forms")
        .then((res) => {
            const {data} = res.data;
            setNavLinks(data);
            
    
        })
        .catch( (e) => {
            console.log(e)
        })
      }
    
      useEffect(() => {
        getLinks();
        
        return () => '';//console.log('unmounting...');
        
      }, []); // <-- add this empty array here 

    useEffect(() => {

        //console.log('mounted');
        return () => '';//console.log('unmounting...');
        
    }, []); // <-- add this empty array here

    return (
        <>
            <Nav navLinks={navLinks} />

            <Outlet/>
        </>
    )
}