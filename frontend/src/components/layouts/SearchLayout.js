import { useState, useEffect, useRef, useContext } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import axios from "axios";
import "../../App.scss";

import FilterCategories from "../forms/FilterCategories";
import {SiteDataContext} from "../context/SiteDataContext";

import {SearchContext} from "../context/SearchContext";
import UserContext from "../context/UserContext";
//import Button from "../buttons/Button";
//import ButtonGroup from "../buttons/ButtonGroup";
import EmbeddedButton from "../buttons/EmbeddedButton";
import Banner from "../containers/Banner";
import LayoutHeader from "../elements/LayoutHeader";


export default function SearchLayout (props) {
    //const [handleLayoutChange] = useOutletContext()
    //const [getLayoutPrev] = useOutletContext();
    const searchData = useContext(SearchContext);
    const siteData = useContext(SiteDataContext);
    const user = useContext(UserContext);
    const navigate = useNavigate();
    const inputRef = useRef();
//console.log(siteData)
    //const [siteData, setSiteData] = useState();
    //const [wishesList, setWishesList] = useState([]);
    const [found, setFound] = useState({});
    const [formValues, setFormValues] = useState();
    const [formStyles, setFormStyles] = useState();
    const [input, setInput] = useState("");

    const [formTitle, setFormTitle] = useState('Looking for some different?');
    
    const [formSubTitle, setFormSubTitle] = useState('Keep and share your best resources');

    const [isExtended, setIsExtended] = useState(false);
    const [isFiltOpen, setIsFiltOpen] = useState(false);
    const [titleStyle, setTitleStyle] = useState("py-9");
    // "pt-7 pb-4  md:pt-8 md:pb-9 lg:pt-20 xl:pb-30"

    let handleBlur = (e) => {
       //setFocus(false)
    }

    let handleOpenFilter = (e) => {
//console.log(formValues)
        setIsFiltOpen(!isFiltOpen);
        if(!isFiltOpen) {
            window.scroll(0,0)
        }
    }

    let handleOpenExtendedFilter = (e) => {

        // MODIFICAR formValues 
        // COPIAR y filtrar nuevo objeto query
        // CUANDO isExtended es false,
        // ELIMINAR o INVISIBILIZAR CAMPOS no visibles del query,
        // NO incluir para no endurecer los matches en el modo simple
        // Al extender, volver a añadir con los default (pensar solución con el tema profile default)
        if ( !isExtended && user.profile ) {
            //console.log(user.profile)
            user.filters.map ( (filter, i) => {
                // if !isExtended, just update single filter
                if( filter !== 'matchOption' && filter !== 'searchBy' && filter !== 'section') {
                    setFormValues((prevForm) => ({ ...prevForm, [filter]: user.profile[filter] }));
                }
                
            } )
        }
        else if (isExtended) {
            let obj = {};
            obj['searchBy'] = formValues['searchBy'];
            obj['textInput'] = input.toLowerCase();
            obj['section'] = formValues['section'];
            obj['matchOption'] = formValues['matchOption'];
            /*Object.keys(formValues).map ( (filter, i) => {
                // if !isExtended, just update single filter
                

                if( filter !== 'matchOption' && filter !== 'searchBy' && filter !== 'section') {
                    setFormValues((prevForm) => ({ ...prevForm, [filter]: [] }));
                }
                
            } ) // */
            setFormValues(obj);
        } 

        setIsExtended(!isExtended);
    }
    
    let handleFormChange = (e) => {
        //setFormValues( (prevForm) => ({ ...prevForm, ['results']: [] }) );
        //navigate("/"); 
    }

    let handleActiveIndexChange = (index) => {
        //setFormValues( (prevForm) => ({ ...prevForm, ["activeIndex"]: index }) ); 
    }

    let handleInputChange = (e) => {
        // Hide Input Label on First change value

        

        if (formTitle.length > 0) {
          //setTitleStyle("md:invisible text-lg pt-2 pb-4 md:pt-2 lg:pb-5");
          //setFormSubTitle('')
        }
        //convert input text to lower case
        var lowerCase = e.target.value.toLowerCase();
        setInput(e.target.value)
        setFormValues( (prevForm) => ({ ...prevForm, ["textInput"]: lowerCase }) );
        //setFormValues( (prevForm) => ({ ...prevForm, ['results']: [] }) );      
    };


    let handleSubmit = (e) => {  
        e.preventDefault();
//console.log("submit:" + formValues.section)
        // VOLVER A PROBAR EL SUBMIT Y LA RECEPCION DE PARAMS y TRATAMIENTO DE DATOS
        
        //setFormValues( (prev) => ({ ...prev, ['textInput']: input.toLowerCase() }) ); 
        //setFormValues( (prev) => ({ ...prev, ['results']: [] }) ); 
        /*const queryObj = {};

        Object.keys(formValues).map( (key, i) => {
            if( key !== 'results' ) {
                queryObj[key] = formValues[key]
            }
        })// */
           
        //setFormValues( (prev) => ({ ...prev, ['textInput']: input }) ); 
        searchData['matchOption'] = formValues['matchOption'];
        searchData['searchBy'] = formValues['searchBy'];
        searchData['textInput'] = input;

        console.log("layout: " + JSON.stringify(searchData))
        axios.get("/search", { params: formValues } )
        .then( (res) => {
                searchData['results'] = res.data.data;
                setFound((prev) => ({ ...prev, ["found"]: res.data.data  }));
            //searchData = formValues;
            //navigate("/search");
            navigate("/search",{'state': {
                'results' : res.data.data,
                'formValues': formValues
            }})  // */
        })
        .catch( (e) => {
            console.log(e)
        }) // */
        /*navigate("search",{'state': { 
            'resources' : response.data.data,
            'matchInput': '',
            'matchOption': '',
            'searchBy': ''
        }}) // */
 /*       const { value } = e.target.elements.search;
//console.log(value)
        if (value.length > 0) {
            //setFocus(true);
            //setIsFiltOpen(false);
            //setFocus(true);
            // FALTA!: Llamada a api, Navegar solo si hay resultados, y pasarlos al searchPage para mostrarlos
            e.target.reset();
            setInput('');
            navigate("search")
          //props.handleSearchResource(value, catChecked, srcChecked, quality, srchByChecked, matchOption); 
          

          /// probar a obtener objeto con useSearchParams
        }*/
        //setFocus(true);
        //e.target.reset();
        //setInput('');
    };


    let handleFilterChange = (checked,name) => {
        //console.log("filter change:" + checked)
        //console.log(name)
        setFormValues((prevForm) => ({ ...prevForm, [name]: checked }));
        
//console.log("filter:" + JSON.stringify(name + ' ' + checked))
console.log("filter:" + JSON.stringify(formValues))
    }


    useEffect(() => {

        // Update filter with profile
        if ( user.filters ) {
            //console.log(user.profile)
            user.filters.map ( (filter, i) => {
                // if !isExtended, just update single filter
                if( filter === 'matchOption' || filter === 'searchBy' || filter === 'section') {
                    setFormValues((prevForm) => ({ ...prevForm, [filter]: user.profile[filter] }));
                }
                
            } )
        } 
        
    }, [user]); // */

    const handleScroll = () => {
    /*
       setTimeout( () => setFormStyles("lg:bg-white/70 bg-white/40"), 2000);
       //setTimeout( () => setFormSubTitle(''), 1000); */
       setTitleStyle("invisible mb-5");  //"text-2xl pt-0 pb-4 md:pt-2 lg:pb-5"); // */
       
    }

    // focus input text
    useEffect(() => {
        //inputRef.current.focus();
        window.addEventListener("scroll", handleScroll);
        /*setTimeout( () => {
            setFormSubTitle(''); 
            //setFormStyles('invisible text-3xl')
        },12000) // */
        //

        return () => window.removeEventListener("scroll", handleScroll);
    }, []); // */

/**
 * 
 * 
                <button className="toggle toggle-off" type="submit">
                Get Links
                </button>
                <input type="button" className={isFiltOpen? " toggle toggle-on": "toggle toggle-off"} value="Filters" onClick={handleOpenFilter}/>
                <input type="button" className={isExtended? " toggle toggle-on": "toggle toggle-off"} value="Advanced" onClick={handleOpenExtendedFilter}/>
            
 */

    return (
    <div className="">
        <div className={`${formStyles} bg-white/60 border border-slated-600 border-t-0 border-x-0 xl:grid xl:grid-cols-1 justify-items-center xl:top-5 ${isFiltOpen ? '': 'sticky top-5'}`}>
        <form
            noValidate
            onSubmit={handleSubmit}
            onChange={handleFormChange}
            className={``}
        >
            
        {
            <div className="">
                {
                    isFiltOpen
                    ?
                        <FilterCategories label="section" title="filters" typeOption="checkbutton" name="section" categories={siteData['section']} checked={formValues['section']} handleChecked={handleFilterChange} >

                        </FilterCategories>          
                    :
                        <LayoutHeader styles={`${titleStyle}`}>{formTitle}</LayoutHeader>
                }
            </div>
        }
                      
            <div className={`flex mx-4 `}>
                {
                    <div className={``}>
                <label htmlFor="search-dropdown" className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Filters</label>
                <EmbeddedButton styles="" label={'Filters'} type={'button'} handleClick={handleOpenFilter} >
                    <svg className="w-5 h-5 text-gray-500 dark:text-gray-400 group-hover:text-blue-600 dark:group-hover:text-blue-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                        <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M4 12.25V1m0 11.25a2.25 2.25 0 0 0 0 4.5m0-4.5a2.25 2.25 0 0 1 0 4.5M4 19v-2.25m6-13.5V1m0 2.25a2.25 2.25 0 0 0 0 4.5m0-4.5a2.25 2.25 0 0 1 0 4.5M10 19V7.75m6 4.5V1m0 11.25a2.25 2.25 0 1 0 0 4.5 2.25 2.25 0 0 0 0-4.5ZM16 19v-2"/>
                    </svg>
                    <span className="sr-only">Settings</span>
                </EmbeddedButton>
                </div>
                }
                
                <div className={`relative w-full`}>
                    <input
                        name="search"
                        type="search" 
                        id="search" 
                        className={`block w-full p-2.5 z-20 text-sm text-gray-900 rounded-e-lg border-s-gray-50 border-s-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-s-gray-700  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:border-blue-500`}
                        placeholder="Search Resources"
                        ref={inputRef}
                        minLength={1}
                        autoComplete="off"
                        value={input}
                        required 
                        onChange={handleInputChange}
                    />
                    <button type="submit" className="absolute top-0 end-0 p-2.5 text-sm font-medium h-full text-white bg-blue-700 rounded-e-lg border border-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        <svg className="w-4 h-4" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                        </svg>
                        <span className="sr-only">Search</span>
                    </button>
                </div>
            </div>
            
            <div className="text-center mt-5">           
                {
                
                    isFiltOpen && 
                    <span className="flex items-center">
                        <label 
                            htmlFor="extend" 
                            className="inline-flex items-center cursor-pointer mt-3 lg:text-xl"

                        >
                            {`_All`}
                        </label>
                        <input 
                            type="checkbox"
                            name="extend"
                            value="" 
                            onChange={(e) => setIsExtended(!isExtended)} 
                            checked={isExtended}
                            className="sr-only peer" 
                            onClick={handleOpenExtendedFilter} />
                        <div className="relative w-9 h-5 lg:ms-8  bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600"></div>
                    </span>   
                }
            </div>
        { 
            <div className="search-bottom">
                {
                    isFiltOpen && isExtended 
                    ?
                    
                        siteData.filters.map( (filter, i) => {
                            if ( filter !== 'searchBy' && filter !== 'section' ) {
                                return (
                                <FilterCategories key={i} label={filter}
                                            typeOption={
                                            siteData.radio && siteData.radio.includes(filter)
                                            ? "radiobutton"
                                            : "checkbutton"
                                        } 
                                            name={filter} categories={siteData[filter]} checked={formValues[filter]} handleChecked={handleFilterChange} title={""} />
                            )}
                        })
                    
                        :  ''  
                }
                {
                    isFiltOpen 
                    ?
                    <FilterCategories label="searchBy" name="searchBy" typeOption="radiobutton" categories={siteData.searchBy} checked={formValues.searchBy} handleChecked={handleFilterChange} title={""} />
                        : ''   
                }    
            </div>
        }

        { false &&
            <h3
                className={`${''} text-center mx-2 mt-6 font-extrabold text-xl lg:text-3xl text-slate-500`}
            >
                {formSubTitle}
            </h3>
        }
        {false &&
            <div className="flex inline-flex justify-items-center">
                <Banner timeOut="15500" />
            </div>
        }        
        </form>
        </div>
        <Outlet context={[found]}/>
    </div>
    )
}