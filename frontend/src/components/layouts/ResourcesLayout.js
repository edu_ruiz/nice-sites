import { React, useEffect, useState } from 'react';
import { Outlet } from 'react-router-dom';
import axios from "axios";
//import history from 'history/browser'; 
import NavBar from '../nav/NavBar';
import InlineList from '../lists/InlineList';
import LayoutHeader from '../elements/LayoutHeader';
import NavDrawer from '../nav/NavDrawer';

export default function ResourcesLayout (props) {

    const [navLinks, setNavLinks] = useState([])
    //const [active, setActive] = useState();

    const getLinks = () => {
        axios.get("/index/nav/resources")
        .then((res) => {
            const {data} = res.data;
            setNavLinks(data);
            
    
        })
        .catch( (e) => {
            console.log(e)
        })
      }

      const handleActive = (name) => {

      }
    
      useEffect(() => {
        getLinks();
        //setActive('Resources')
        return () => '';//console.log('unmounting...');
        
      }, []); // <-- add this empty array here 

    useEffect(() => {

        //console.log('mounted');
        return () => '';//console.log('unmounting...');
        
    }, []); // <-- add this empty array here

    /**
     * <InlineList styles="pt-6 md:my-3 lg:my-5 xl:my-6 border-b">
                <NavBar styles="border-t border-e rounded-t-lg rounded-b-none" navLinks={navLinks} handleActive={()=>{}} />
            </InlineList>
     */

    return (

        <div className="mt-7">
            <LayoutHeader styles="text-gray-400 pt-5 mb-5">Share your favourite resources</LayoutHeader>
            

            <NavDrawer/>
            
            <Outlet/>
        </div>
               
    )
}