import { React, useEffect } from 'react';
import {Outlet, NavLink } from 'react-router-dom';

//import history from 'history/browser'; 

export default function FilterLayout ({children}) {

    useEffect(() => {

        //console.log('mounted');
        return () => '';//console.log('unmounting...');
        
    }, []); // <-- add this empty array here

    return (
        <>
            <h4>Top Filter</h4>

            {children}
            <h4>Bottom filter</h4>
        </>
    )
}