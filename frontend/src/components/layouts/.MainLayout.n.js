
import { useState, useEffect, useContext } from "react";
import { NavLink, Outlet } from "react-router-dom";

import history from 'history/browser';
import axios from "axios";

import UserContext from "../context/UserContext";
import NavBar from "../nav/NavBar";
import InlineList from "../lists/InlineList";
import Dropdown from "../containers/Dropdown";

export default function MainLayout (props) {
  // CREAR animación en Nice! cada varios segundos, un estremecimiento (rotate alterno poco recorrido y rápido)
  const [navLinks, setNavLinks] = useState([]);
  const [userLinks, setUserLinks] = useState([]);
  const [pageLinks, setPageLinks] = useState([])
  const user = useContext(UserContext);


  const handleNavBar = (page) => {
    /*let navbar = 'main';
    
    switch(page) {
      case 'resources': navbar = 'resources'; break;
      default: navbar = 'main';
    } // */
    //getPageLinks(navbar);
  }

  const handleStyles = (isActive) => {
    if(isActive) {
      return "border border-e-0 rounded-lg"
    }
  }

  const getLinks = () => {
    let path = '';
    user ? path = 'user' : path = 'login';

    axios.get(`/index/nav/main`)
    .then((res) => { 
        const {data} = res.data;
        setNavLinks(data);
    })
    .catch( (e) => {
        console.log(e)
    })

    axios.get(`/index/nav/${path}`)
    .then((res) => { 
        const {data} = res.data;
        setUserLinks(data);
        
    })
    .catch( (e) => {
        console.log(e)
    }) // 
  }

  const getPageLinks = (page) => {

    axios.get(`/index/nav/${page}`)
    .then((res) => { 
        const {data} = res.data;
        setPageLinks(data);
    })
    .catch( (e) => {
        console.log(e)
    })
  }

  useEffect(() => {

    getLinks();
    getPageLinks('main');
  }, []);

// Menu dropdown user: Si no hay user logged, ofrece links a forms, si hay logged, ofrece navlinks user
// grid grid-cols-12
  return (

  <div id="app" className="font-sans">

    <header className="top-0 z-40 w-screen fixed  flex flex-row justify-between bg-indigo-500 py-3">

      <button
        id="buttonMainNav" 
        data-dropdown-toggle="dropdownMainNav" 
        className="ml-4 text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" 
        type="button"
      >
        <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 1h15M1 7h15M1 13h15"/>
        </svg>
      </button>

      <div className="">
        <h1 className="-skew-y-2 col-span-9 lg:col-span-10 text-center text-3xl md:text-xl lg:text-2xl xl:text-4xl text-white font-bold font-display">
          { "Nice!" }
        </h1>
      </div>
      
      <div className="mr-6 md:mr-20">
        <button 
          id="buttonUserNav" 
          data-dropdown-toggle="dropdownUserNav" 
          className="relative inline-flex items-center justify-center w-10 h-10 overflow-hidden bg-gray-100 rounded-full dark:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-blue-300" 
          type="button"
        >
          <span className="font-medium text-gray-600 dark:text-gray-300">{user.nick ? user.nick.slice(0,1) : '' }</span>
        </button>
      </div>
      <Dropdown 
        ariaLabel="buttonUserNav" 
        dropdownId="dropdownUserNav"
        styles="hidden w-50"
        listStyles="flex-col"
      >
        <NavBar navLinks={userLinks}/>    
      </Dropdown>
    {
      false &&
      <span className={"bg-blue-100 text-blue-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300"}>
        {`Hi, ${user.nick}, ${user._id}`}
      </span>
    }

    </header>

    <Dropdown 
      ariaLabel="buttonMainNav" 
      dropdownId="dropdownMainNav"
      styles="hidden w-50 md:flex md:w-full justify-center"
      listStyles="flex-col md:flex-row md:space-x-8"
    >
      <NavBar styles={"border-y-0 border-e-1 shadow-2"} navLinks={navLinks} handleActive={handleNavBar} />
    </Dropdown>

    <main className="bg-white p-0">  
      <Outlet/>
    </main>

    <footer>
      <NavLink><span onClick={history.back}>Back</span></NavLink>
      <h5>{'Reached bottom'}</h5>
    </footer>
  </div>
  );
}
