
import { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";

import axios from "axios";

import UserContext from "../context/UserContext";
import NavBar from "../nav/NavBar";
import InlineList from "../lists/InlineList";
import NavBottom from "../nav/NavBottom";
import NavDrawer from "../nav/NavDrawer";
import UserRegister from "../forms/UserRegister";
import UserLogin from "../forms/UserLogin";
import Dropdown from "../containers/Dropdown";

export default function MainLayout (props) {
  // CREAR animación en Nice! cada varios segundos, un estremecimiento (rotate alterno poco recorrido y rápido)
  const [navLinks, setNavLinks] = useState([]);
  const [pageLinks, setPageLinks] = useState([]);
  const [userLinks, setUserLinks] = useState([]);

  const [viewRegister, setViewRegister] = useState(false);
  const [viewLogin, setViewLogin] = useState(true);
  const [viewRemember, setViewRemember] = useState(false);
  const [user, setUser] = useState({});

  const handleLogin = (data) => {

    setUser(data);
  }

  const handleLogout = () => {

    axios
      .get("/auth/logout")
      .then( (res) => {
        console.log(res.data.data);
        setUser({});
      })
  }

  const handleUserCreated = (user) => {

    setUser(user);
    handleClose();
  }

  const handleClose = () => {
    setViewRegister(false);
    setViewLogin(false);
    setViewRemember(false);
  }

  const handleUserForm = (view) => {
    handleClose();

    switch (view) {
      case 'login': setViewLogin(true);break;
      case 'remember': setViewRemember(true);break;
      case 'register': setViewRegister(true);break;
      default: ;
    }
  }

  const handleNavBar = (page) => {
    let navbar = 'main';
    
    switch(page) {
      case 'resources': navbar = 'resources'; break;
      default: navbar = 'main';
    }
    //getPageLinks(navbar);
  }

  const handleStyles = (isActive) => {
    if(isActive) {
      return "border border-e-0 rounded-lg"
    }
  }

  const getLinks = () => {

    axios.get(`/index/nav/main`)
    .then((res) => { 
        const {data} = res.data;
        setNavLinks(data);
    })
    .catch( (e) => {
        console.log(e)
    })

    axios.get(`/index/nav/user`)
    .then((res) => { 
        const {data} = res.data;
        setUserLinks(data);
        
    })
    .catch( (e) => {
        console.log(e)
    }) // 

    /*axios.get(`/index/nav/resources`)
    .then((res) => { 
        const {data} = res.data;
        data.map( (item) => {
          setNavLinks( (prev) => ([ ...prev, item]))
        })
    })
    .catch( (e) => {
        console.log(e)
    }) // */
  }

  const getPageLinks = (page) => {

    axios.get(`/index/nav/${page}`)
    .then((res) => { 
        const {data} = res.data;
        setPageLinks(data);
    })
    .catch( (e) => {
        console.log(e)
    })
  }

  useEffect(() => {

    getLinks();
    getPageLinks('main');
  }, []);

// Menu dropdown user: Si no hay user logged, ofrece links a forms, si hay logged, ofrece navlinks user
// grid grid-cols-12
  return (
  <UserContext.Provider value={user}>
  <div id="app" className="font-sans">

    <header className="top-0 z-20 w-screen fixed  flex flex-row justify-between bg-indigo-500 py-3">

      <button
        id="dropdown-main-nav-button" 
        data-dropdown-toggle="dropdown-main-nav" 
        className="ml-4 md:invisible text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800" 
        type="button"
      >
        <svg className="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 17 14">
            <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M1 1h15M1 7h15M1 13h15"/>
        </svg>
      </button>

      <div id="dropdown-main-nav" className="z-10 hidden bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700">
        <ul className="text-sm text-gray-700 dark:text-gray-200" aria-labelledby="dropdown-main-nav-button">
          <NavBar navLinks={navLinks}/>
        </ul>
      </div>

      <div className="">
        <h1 className="-skew-y-2 col-span-9 lg:col-span-10 text-center text-3xl md:text-xl lg:text-2xl xl:text-4xl text-white font-bold font-display">
          { "Nice!" }
        </h1>
      </div>
      <div className="mr-6 md:mr-20">
        <button 
          id="buttonUserNav" 
          data-dropdown-toggle="dropdownUserNav" 
          className="relative inline-flex items-center justify-center w-10 h-10 overflow-hidden bg-gray-100 rounded-full dark:bg-gray-600 focus:ring-4 focus:outline-none focus:ring-blue-300" 
          type="button"
        >
          <span className="font-medium text-gray-600 dark:text-gray-300">{user && user.nick ? user.nick.slice(0,1) : '' }</span>
        </button>
      </div>
      {
        !user || !user._id ?
      <Dropdown 
        ariaLabel="buttonUserNav" 
        dropdownId="dropdownUserNav"
        styles="hidden w-50"
        listStyles="flex-col"
      >
        <li
          className={`peer w-full pt-1 px-4 pb-2 xl:text-xl text-gray-700 border-gray-200 hover:bg-gray-100 dark:border-gray-600`}    
        > 
          <button type="button" onClick={() => setViewLogin(true)}>login</button>
        </li>
        <li
          className={`peer w-full pt-1 px-4 pb-2 xl:text-xl text-gray-700 border-gray-200 hover:bg-gray-100 dark:border-gray-600`}       
        >
          <button type="button" onClick={() => setViewRegister(true)}>register</button>
        </li>
        <li
          className={`peer w-full pt-1 px-4 pb-2 xl:text-xl text-gray-700 border-gray-200 hover:bg-gray-100 dark:border-gray-600`}
        >
          <button type="button" onClick={() => setViewRemember(true)}>remember</button>
        </li>  
      </Dropdown>
      :
      <Dropdown 
        ariaLabel="buttonUserNav" 
        dropdownId="dropdownUserNav"
        styles="hidden w-50"
        listStyles="flex-col"
      >
        <NavBar navLinks={userLinks}/>
        <li
          className={`peer w-full pt-1 px-4 pb-2 xl:text-xl text-gray-700 border-gray-200 hover:bg-gray-100 dark:border-gray-600`}
        >
          <button type="button" onClick={handleLogout}>logout</button>
        </li> 
      </Dropdown>
    }     

    {
      false &&
      <span className={"bg-blue-100 text-blue-800 text-xs font-medium me-2 px-2.5 py-0.5 rounded dark:bg-blue-900 dark:text-blue-300"}>
        {`Hi, ${user.nick}`}
      </span>
    }

    </header>

    {
      viewRegister &&

      <UserRegister handleUserCreated={(usr) => handleUserCreated(usr)} handleClose={handleClose}/>
    }
    {
      viewLogin &&

      <UserLogin handleClose={handleClose} setUser={(data) => handleLogin(data)} />
    }

    <div className="invisible my-0 md:visible md:mt-8">
      <InlineList styles="">
        <NavBar styles={"border-y-0 border-e-1 shadow-2"} navLinks={navLinks} handleActive={handleNavBar} />
      </InlineList>
    </div>    

    <main className="bg-white p-0">  
      <Outlet/>
    </main>

    <footer>
      <h5>{'Reached bottom'}</h5>
      
    </footer>
  </div>
  </UserContext.Provider>
  );
}
