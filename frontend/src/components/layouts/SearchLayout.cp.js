import React, { useState } from "react";
import { Outlet, useNavigate, NavLink } from "react-router-dom";
import axios from "axios";
import "../../App.scss";


import getFilterCategories from "../helpers/categories";
import FilterLayout from "./FilterLayout";
import FilterCategories from "../forms/FilterCategories";


export default function SearchLayout (props) {
    const title = 'Looking for some different?';
    const navigate = useNavigate();
    const [context] = useState("...Context Data...Result List, Status Message");

    const [input, setInput] = useState("");
    const [focus, setFocus] = useState(false);
    const [formTitle, setFormTitle] = useState(title);
    const [isFiltOpen, setIsFiltOpen] = useState(false);

    const [categories, setCategories] = useState(getFilterCategories());

    const [catChecked, setCatChecked] = useState(categories.sections);
    const [srcChecked, setSrcChecked] = useState(categories.sources);
    const [srchByChecked, setSrchByChecked] = useState(['title']);
    const [quality, setQuality] = useState([5.0, 10.0]);
    const [matchOption, setMatchOption] = useState('free');


    // ADAPTAR PARA USE EFFECt, probar efectos en cada cambio

    // CREO que hay Ops pendientes al hacer Unmount, Mount, probar antes de insertar todo

    // CORREGIR!!! SearchBy no guarda Estado anterior


    let handleBlur = (e) => {
        //setFormTitle(title);
        setFocus(false)
        //setIsFiltOpen(false);
    }

    let handleOpenFilter = (e) => {
        setIsFiltOpen(!isFiltOpen); 

    }

    let handleFormChange = (e) => {
        //setFocus(false);
    }

    let handleChange = (e) => {
        // Hide Input Label on change value
        //if (formTitle.length > 0) {
          //setFormTitle('')
        //}
        //convert input text to lower case
        var lowerCase = e.target.value.toLowerCase();
        setInput(lowerCase);
        // Ante un cambio, aparece otra vez la lista, si estaba oculta
        setFocus(true);
        setIsFiltOpen(false)
      };

          // onSubmit -> si hay resultados, navegar a SearchPage, si no, mostrar mensaje error búsqueda
    
      let handleSubmit = (e) => {
        
        e.preventDefault();
        const { value } = e.target.elements.search;
//console.log(value)
        if (value.length > 0) {
            //setFocus(true);
            //setIsFiltOpen(false);
            //setFocus(true);
            // FALTA!: Llamada a api, Navegar solo si hay resultados, y pasarlos al searchPage para mostrarlos
            e.target.reset();
            setInput('');
            navigate("search")
          //props.handleSearchResource(value, catChecked, srcChecked, quality, srchByChecked, matchOption); 
          

          /// probar a obtener objeto con useSearchParams
        }
      };

    let handleCategChecked = (chk) => {
    setCatChecked(chk); 
    
    }
    let handleSearchChecked = (chk) => {
        setSrchByChecked(chk);
      }
// Intentar reutilizar Input component, pasándole la lógica por props

    return (
    <>
        <form
            noValidate
            onSubmit={handleSubmit}
            className={ "search" }
        >
            {
                !focus && 
                <div className={"search-top" + (isFiltOpen? "top-mod": '')}>
                    {
                        isFiltOpen
                        ?
                            <FilterCategories label="" title={"Sections"} categories={categories.sections} checked={catChecked} handleChecked={handleCategChecked}/>          
                        :
                            <h2>{formTitle}</h2>
                    }
                </div>
            }
            <div className="search-bar">
                <input
                    className="search-input"
                    type="text"
                    name="search"
                    required={true}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    minLength={1}
                    autoComplete="off"
                    value={input}
                />
                <button className="toggle toggle-off" type="submit">
                Get Links
                </button>
                <input type="button" className={isFiltOpen? " toggle toggle-on": "toggle toggle-off"} value="Filters" onClick={handleOpenFilter}/>
            </div>
            {
                !focus && 
                <div className="search-bottom">
                    {
                        isFiltOpen
                        ?
                        <FilterCategories label="search by" categories={categories.search} checked={srchByChecked} title={""} />
                         : ''   
                    }
                </div>
            }   
            </form>

        <Outlet context={[context]} />
    </>
    )
}