import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function ComponentLabel (props) {

    return (
        <label 
            htmlFor={props.label} 
            className={`${props.styles} capitalize mb-2 font-semibold  dark:text-white`}
        >
            {props.label}
        </label>
    )
}



