import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function FormLabel (props) {

    return (
        <label 
            htmlFor={props.label} 
            className={`${props.styles} capitalize block ml-1 mb-2 text-sm font-bold  dark:text-white`}
        >
            {props.label}
        </label>
    )
}



