import { useState, useEffect } from "react";
import FormLabel from "../labels/FormLabel";

export default function SelectOption(props) {
    const [selected, setSelected] = useState();
    const [options, setOptions] = useState();

    const handleChange = (e) => {

        props.handleSelected(e);
        setSelected(e.target.value); 
    }

    useEffect( () => {
        setSelected(props.selected);
        //setOptions(props.options);
    },[])

    // props.size
    // block inline-block w-1/2 items-center
    return (
        props ?
        <div className={`${props.divStyles}`}>
            <FormLabel styles={ props.disabled ? "text-gray-300" : "text-gray-600"} label={props.label} />
            <select
                name={props.label}
                size={props.size} 
                className={`${props.selectStyles} w-full p-2.5 bg-gray-100 border border-gray-300 ${ props.disabled ? "text-gray-500" : "text-gray-900"} text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500`}
                value={selected}
                onChange={handleChange} 
                disabled={props.disabled} 
            >
                { props.options &&
                props.options.map( (option, i )  => {
                    return (
                        <option 
                            key={i} 
                            value={option}
                            className={""}
                        >
                            {option}
                        </option>
                    )
                }) }
            </select>
        </div>
        : ''
    )
}
