import { React, useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom"; 
import axios from 'axios';
import getFilterCategories from "../helpers/categories";
import PageContainer from "../containers/PageContainer";
import PageHeader from "../elements/PageHeader";
import Paragraph from "../elements/Paragraph";


//INTENTAR extraer la lógica de validaciones y comportamiento campos (blur, focus, change)
// A hooks externos para reutilizarlos y dar claridad

// INTENTAR Reducir form, usar SelectOption y Input components, pasarles el [input]
// el componente asigna las props en su interior, ocultar aquí la asignación
// CAda input onChange onBlur llamará a validity por su cuenta
// y delegará al padre solamente el manejo del dato Value con funciones

export default function AddResource(props) {
    let navigate = useNavigate();
    let location = useLocation();
    const [sections] = useState(getFilterCategories().sections);
    const [sources] = useState(getFilterCategories().sources);
    const [response, setResponse] = useState('');

    const [disabled, setDisabled] = useState('disabled');
// Es necesario declarar cada campo vacío al inicio ????
    const [formData, setFormData] = useState({
        url: '',
        section: 'global',
        source: 'hosting',
        title: '',
        subject: '',
        keywords: '',
        contents: '',
        ux: '',
        contents_score: '5',
        ux_score: '5',
        email: '',
        userId: ''
    });
    const [errors, setErrors] = useState({
        url: [],
        title: [],
        subject: [],
        keywords: [],
        contents: [],
        ux: [],
        contents_score: [],
        ux_score: [],
        email: [],
        userId: []
    }); 

    const [inputs, setInputs] = useState([]);

    useEffect(() => {       
        axios
            .get("/forms/resource")
            .then( (res) => {
                setInputs(res.data.data)
            })
            .catch((e) => {
                console.log(e)
            }) 
        return () => '';//console.log('unmounting...');
    }, []); // <-- add this empty array here
      
     
// INTENTAR extraer lógica a hook useValidity ??? revisar artículo simplify forms
    let checkValidity = (event) => {

        let { validity } = event.target;
        let { name } = event.target;

        if (!validity.valid) {
            let er = []
            for (let key in validity) {
                //console.log(key)
                if (validity[key] === true) {

                    let message = '';

                    switch (key) {
                        case 'valueMissing': message = "Can't be empty"; break;
                        case 'typeMismatch': message = "Wrong characters"; break;
                        case 'patternMismatch': message = "Not well formed"; break;
                        case 'tooLong': message = "Max size exceeded"; break;
                        case 'tooShort': message = "Minimum size required"; break;
                        case 'rangeUnderflow': message = "Minimum in range required"; break;
                        default: message = "other error";
                    }
                    er.push(message);
                    setErrors((prevErrors) => ({ ...prevErrors, [name]: er }));
                }
            }
        }
        else {
            setErrors((prevErrors) => ({ ...prevErrors, [name]: [] }))
        }
    }

    let handleSubmit = (e) => {
        // cambiar por useSearchParams() o algo así
        e.preventDefault();
        let resource = {
            'url': formData.url,
            'section': formData.section,
            'source': formData.source,
            'title': formData.title,
            'subject': formData.subject,
            'keywords': formData.keywords,
            'contentsScore': formData.contents_score,
            'uxScore': formData.ux_score,
            'contentsTag': formData.contents,
            'uxTag': formData.ux,
            'email': formData.email,
            'userId': formData.userId
        }
//console.log(resource)
// FALTA Arreglar routes en backend, cambiar resource/add por resource POST
        axios
            .post("/resources", resource)
            .then((response) => {
                setResponse(response.data.data)

                return (
                    navigate(`/resources/${response.data.data._id}`, 
                                    {'state': { 
                                        'resource' : response.data.data,
                                        'path': location.pathname
                                    }}
                            )
                )
                
            })
            .catch((e) => {
                console.log(e)
            });

    }

    const handleUrlChange = (event) => {
        setDisabled('disabled');
        const { name, value } = event.target;
        setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));
    };

    const handleBlur = (event) => {
        const { name, value, type } = event.target;
        setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));

        if (type !== 'url') {
            checkValidity(event);
        }
    };

    const handleUrlBlur = (event) => {
        let msg = '';

        // from validity get html validation and errors
        checkValidity(event);

        const { value } = event.target;

        // REVISAR COMPORTAMIENTO, unas ops desbloquean el campo,
        // revisar orden de llamadas validity y llamadas a Errors y Disabled
        if (value.length > 0) {

            // AÑADIR:  revisar si el url es el home page, y restringir formulario a categoría 'sites'
            axios
                .get("/search/urls", { params: { 'url': value } })
                .then((res) => {

                    if (res.data.data) {
                        msg = 'This resource already exists';
                        // FALTA ofrecer un enlace para visitar la coincidencia
                        let er = [];
                        er.push(msg)
                        setDisabled('disabled');
                        setErrors((prevErrors) => ({ ...prevErrors, ['url']: er, ['title']: [] }));
                    }
                    else {
                        // if valid unlock whole form
                        setDisabled();
                        // if isHomePage lock section in 'sites' option value
                    }
                })
                .catch((e) => {
                    console.log(e.errors)
                });
        }
        handleBlur(event);
    };

    const handleSelect = (event) => {

        let { name, value } = event.target;

        setFormData((prevFormData) => ({ ...prevFormData, [name]: value }))
    }

    // [a-z0-9\\-]{1,63}\.(onion|[a-z]{2,3})\/?
    return (
        <PageContainer
            header={<PageHeader>{'Add new resource'}</PageHeader>}
            top={
                <Paragraph>
                    {`We're glad to announce your next resource. Fill and submit this form, please...`}
                </Paragraph>
            }
            body={
                <form onSubmit={handleSubmit}>
                <label htmlFor="url" >
                    {'URL'}
                    <input
                        type="url"
                        label="url"
                        name="url"
                        value={formData.url.value}
                        required={true}
                        title="First must check if your url already exists"
                        className={errors.url.length > 0 ? "border-danger" : "border-muted"}
                        size="40"
                        maxLength="60"
                        pattern="https://[a-z0-9\-]{1,63}\.(onion|[a-z]{2,3})\/?.*"
                        placeholder="https://example.com/path-to-resource"
                        onBlur={handleUrlBlur}
                        onChange={handleUrlChange}
                    />
                    {errors.url && <span className="text-danger">{errors.url.toString()}</span>}
                </label>
                <label htmlFor="section" className={disabled ? "text-muted" : ''}>
                    {'section'}
                    <select name="section" value={formData.section} disabled={disabled} onChange={handleSelect}>
                        {
                            sections.map((section, i) => {
                                return (
                                    <option key={i} value={section}>
                                        {section}
                                    </option>
                                )
                            })
                        }
                    </select>
                </label>
                <label htmlFor="source" className={disabled ? "text-muted" : ''}>
                    {'source'}
                    <select name="source" value={formData.source} disabled={disabled} onChange={handleSelect}>
                        {
                            sources.map((source, i) => {
                                return (
                                    <option key={i} value={source}>
                                        {source}
                                    </option>
                                )
                            })
                        }
                    </select>
                </label>
                {
                    inputs.map((input, i) => {
                        return (
                            <label key={i} htmlFor={input.label} className={disabled ? "text-muted" : ''}>
                                {input.label}
                                <input
                                    type={input.type}
                                    label={input.label}
                                    name={input.label}
                                    value={formData[input.label].value}
                                    required={true}
                                    title={input.tooltipRequired}
                                    className={errors[input.label].length ? "border-danger" : "border-muted"}
                                    size={input.size}
                                    maxLength={input.maxLength}
                                    minLength={input.minLength}
                                    min={input.min}
                                    max={input.max}
                                    step={input.step}
                                    pattern={input.pattern}
                                    disabled={disabled}
                                    placeholder={input.placeholder}
                                    onBlur={handleBlur}
                                />
                                {errors[input.label] && <span className="text-danger">{errors[input.label].toString()}</span>}
                            </label>
                        )
                    })
                }
                <button className="btn btn-primary" type="submit">
                    Send
                </button>
            </form>}
        />
    );
}
