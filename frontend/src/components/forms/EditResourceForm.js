import { useContext, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import ResourceUpdate from './ResourceUpdate';
import UserContext from '../context/UserContext';
import NavTabs from '../nav/NavTabs';


export default function EditResourceForm (props) {
    const user = useContext(UserContext);
    const [resource, setResource] = useState();
    const [message, setMessage] = useState();
    const location = useLocation();
    const [viewContents, setViewContents] = useState();
    const [viewProperties, setViewProperties] = useState();
    const [styles, setStyles] = useState();
    

    const handleActive = (active) => {
        //setViewContents(!viewContents)
        //setViewProperties(!viewProperties)
        if( active === 'contents' ) {
            //setStyles("")
            setViewContents(true);

            setStyles({
                contents: "border-t-2 border-e-2 text-gray-500",
                properties: "border-0 text-slate-500/50  hover:text-gray-600 "
            })
            
        }   
        else {
            //setStyles("")
            setViewContents(false);
            setStyles({
                contents: "border-0 text-slate-500/50  hover:text-gray-600 ",
                properties: "border-t-2 border-x-2 text-gray-500",
            })
            //setViewProperties(!viewProperties);
        } // */
    }

    const handleContentsUpdate = (e) => {
        setViewContents(false);
        setMessage('Was Updated!');
        let timer = setTimeout(() => {
            setMessage(null);
        },1500)
    }

    const handlePropertiesUpdate = (e) => {
        setViewProperties(false);
        setMessage('Was Updated!');
        let timer = setTimeout(() => {
            setMessage(null);
        },1500)
    }

    useEffect(() => {
        setMessage(null);
        setViewContents(false);
        setViewProperties(true);
        setStyles({
            contents: "border-0 text-slate-500/50  hover:text-gray-600 ",
            properties: "border-t-2 border-x-2 text-gray-500",
        })

        if(location.state) {
            setResource(location.state.resource);
        }
        return () =>''  // console.log('unmounting...');
      }, [])  // <-- add this empty array here

      // 
     // onClick={()=>setViewContents(!viewContents)}
    return (
        <div className="mt-4 flex flex-col items-center">
            <h3 className="font-bold text-2xl text-center">Edit Resource</h3>
                <NavTabs>
                    <li
                        onClick={() => handleActive('contents')}
                        className="me-2"
                        key={0}
                    >
                        <div
                            
                            className={styles && `${styles.contents} inline-block p-4 rounded-t-lg`}
                            
                        >
                            <h5>Contents Update</h5>
                        </div>
                    </li>
                    <li
                        onClick={() => handleActive('properties')}
                        className="me-2"
                        key={1}
                    >
                        <div
                            
                            className={styles && `${styles.properties} inline-block p-4 rounded-t-lg`}
                            
                        >
                            <h5>Properties Update</h5>
                        </div>
                    </li>
                </NavTabs>
            { viewContents &&
                <ResourceUpdate
                    user={user} 
                    field="contents" 
                    resource={resource} 
                    handleUpdate={handleContentsUpdate}
                />
            }
            { !viewContents &&
                <ResourceUpdate
                    user={user} 
                    field="properties" 
                    resource={resource} 
                    handleUpdate={handlePropertiesUpdate}
                />
            }
        </div>
    )
}