
import { useState } from "react";
import MatchSuggest from "../lists/MatchSuggest";
import FilterCategories from "./FilterCategories.o";
import getFilterCategories from "../helpers/categories";
import FilterQuality from "./FilterQuality";
import SelectOption from "./SelectOption";

// añadir un <FilterSelect/> para escoger entre 'Busqueda exacta/Orden de palabras libre'
//  Orden libre ---> trocear string, y plantear al regexp como opciones|de|coincidencia|con|el|patrón

function SearchResource(props) {
    const [input, setInput] = useState("");
    const [focus, setFocus] = useState(true);
    const [openFilter, setOpenFilter] = useState(false); 
    const [formTitle, setFormTitle] = useState('Looking for some different?');
    // PENDIENTE: intentar que las sugerencias se carguen desde MatchSuggest, un evento comunica al padre la selección
    const [categories, setCategories] = useState(getFilterCategories());

    const [catChecked, setCatChecked] = useState(['news', 'gaming', 'home', 'arts', 'science', 'music', 'sports', 'history']);
    const [srcChecked, setSrcChecked] = useState(['news','blog', 'community', 'hosting', 'personal', 'bussiness']);
    const [srchByChecked, setSrchByChecked] = useState(['title']);
    const [quality, setQuality] = useState([5.0, 10.0]);
    const [matchOption, setMatchOption] = useState('free');

        // DEBERÍA AUTO-CARGAR Los títulos ?? 
    // Para reducir acoplamiento 

    let handleSubmit = (e) => {
      e.preventDefault();
      const { value } = e.target.elements.value;

      if (value.length > 0) {

        props.handleSearchResource(value, catChecked, srcChecked, quality, srchByChecked, matchOption); 
        e.target.reset();
        setInput('');
      }
    };


    let handleChange = (e) => {
      // Hide <h> label when focus input
      if (formTitle.length > 0) {
        setFormTitle('')
      }
      //convert input text to lower case
      var lowerCase = e.target.value.toLowerCase();
      setInput(lowerCase);
      // Ante un cambio, aparece otra vez la lista, si estaba oculta
      setFocus(true);
    };

    let handleSelected = (value) => {
      // Al seleccionar sugerencia, asignar al campo de texto
      setInput(value);
      // y ocultar la lista
      setFocus(false);
      // Apaño, revisar
      document.getElementById("search-input").focus()
    };

    let handleCategChecked = (chk) => {
      setCatChecked(chk); 
      
    }

    let handleSourceChecked = (chk) => {
      setSrcChecked(chk); 
    }

    let handleSearchChecked = (chk) => {
      setSrchByChecked(chk);
    }

    let handleMatchOption = (opt) => {
      setMatchOption(opt);
    }

    let handleQuality = (value) => {
      let min = 1.0;
      let max = 10;

      switch (value) {
        case 'aceptable': 
          min = 5.0;
          max = 10.0;
          break;
        case 'buena': 
        min = 6.6;
        max = 10.0;
        break;
        case 'muy buena': 
          min = 8.01;
          max = 10.0;
          break;
        default:
          break;
      }
      const rng = [ min, max];
      setQuality(rng);
    }

    function toggle() {
      setOpenFilter((openFilter) => !openFilter);

    }
    
  
    return (
      <div>
        <h2>{formTitle}</h2>
        
        <form
          noValidate
          onSubmit={handleSubmit}
          onChange={handleChange}
          className="new-todo form-group"
        >
        <input
          type="text"
          name="value"
          id="search-input"
          required
          onChange={handleChange}
          minLength={1}
          className="form-control"
          autoComplete="off"
          value={input}
        />
        <button className="btn btn-primary" type="submit">
          Search Links
        </button>
        {
          openFilter &&
          <div>
            <FilterCategories label="sections" categories={categories.sections} checked={catChecked} handleChecked={handleCategChecked}/>
            <FilterCategories label="sources" categories={categories.sources} checked={srcChecked}  handleChecked={handleSourceChecked} />
            <FilterCategories label="search by" categories={categories.search} checked={srchByChecked} handleChecked={handleSearchChecked}/>
            <FilterQuality label="quality" selected={'buena'} handleQuality={handleQuality} /> 
            <SelectOption label="match" options={[ 'exact', 'free' ]} selected={'free'} handleSelected={handleMatchOption} /> 
            <button className="btn btn-primary" type="submit">
              Apply Filters
            </button>
          </div>
        }
        <button className="btn btn-primary" onClick={toggle}>
          Filters
        </button>
      </form>
      
      {input !== '' && focus ? <MatchSuggest handleSelected={handleSelected} titles={props.titles} input={input} /> : '' }
      </div>
    );
  }
  
  export default SearchResource;