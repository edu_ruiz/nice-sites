import { React, useState, useEffect } from "react";

// Campo de Texto
// Props: label, type, validator, lastInput, handlerOnBlur

// events: handlerOnChange, handlerOnBlur, handleOnFocus

// state: text, isMuted, error

function Input(props) {
    const [ value, setValue ] = useState( () => {
        return props.lastInput ? props.lastInput : '';
    });
    const [ disabled, setDisabled] = useState(props.disabled);
    const [ error, setError ] = useState( () => {
        return props.validator.error ? props.validator.error : null;
    });
    

    let handlerOnChange = (e) => {
        // reset error message, ready to rewrite and revalidate
        setError(null);
        setValue(e.target.value);
        props.onChange(e.target.name);
    }

    let handlerOnBlur = (e) => {
        //console.log(e.target.validity)

        // Llamada a un hook o función externo useValidity

        // Si el name es === "search" desactivar la muestra de errores? o modificar el comportamiento
        // probar con Popups para muestra de errores ?? 


            if ( e.target.value.match( props.validator.pattern )  ) {
                

                props.onBlur( e.target.name, e.target.value );
            }
            else {
                // Show error message
            }

    }

    return (
        <>
            <label htmlFor={ props.label } >
                { props.label }
            
            <input 
                type={ props.type } 
                value={ value }
                name={ props.label }
                placeholder={ props.placeholder }
                pattern={props.validator.pattern}
                size={props.validator.size}
                maxLength={props.validator.length}
                required={ props.validator.required }
                onChange={ handlerOnChange }
                onBlur={ handlerOnBlur }
                title={`Field ${ props.label } can't be empty`}
                disabled={props.disabled}
                className="m-2"
            /> 
            </label>   
        </>
        
    )
}

export default Input;