import React from "react";

export default class ServerMessage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newMessage : false
    };
  }

  componentDidMount() {
    
  }

  render() {
    //console.log(this.props.msg)
    //return <h1>server</h1>
    let { msg } = this.props;
    return msg.length > 0 ? (
      <div className="mt-3 alert alert-success" role="alert">
        {msg}
      </div>
    ) : (
      <div className="mt-3 alert alert-primary" role="alert">
        { this.props.hello } No Messages to display
      </div>
    );
  }
}
