import { useRef, useEffect, useState, useContext } from 'react';
import { useLocation, NavLink } from 'react-router-dom';
import axios from 'axios';

import { ImNewspaper } from "react-icons/im";

import UserContext from "../context/UserContext";
import FormLabel from '../labels/FormLabel';
import DescriptionList from '../lists/DescriptionList';
import DescriptionItem from '../lists/DescriptionItem';
import Paragraph from '../elements/Paragraph';
import Button from '../buttons/Button';
import KeyValuePair from '../elements/KeyValuePair';
import HelpContainer from '../containers/HelpContainer';

export default function RateResource (props) {
    //console.log(props.resourceId)
    const [contents, setContents] = useState(2.5);
    const [ux, setUx] = useState(2.5);
    const [message, setMessage] = useState();
    const userRef = useRef();
    const location = useLocation();
    const { url, title } = location.state;
    const { resourceId } = location.state;
    const user = useContext(UserContext);
    const [rating, setRating] = useState();

    function getSum(a, b) {
        return parseFloat(a) + parseFloat(b) ; 
    }

    const handleChange = (id) => {
        
    }

    const fetchRating = (id) => {
        axios
        .get(`/ratings/for/${resourceId}/current/by/${id}`)
        .then( (res) => {
console.log('already exists: ' + JSON.stringify(res.data.data))
            setRating(res.data.data);

            console.log(res.data.data)
            // Temporizar el mensaje, o ligarlo a cambios en ux,contents ??
  
        })
        .catch( (e) => {
            console.log(e);
        })
    }

    function handleSubmit(e) {

        // ALTERNATIVA similar a form add,
        // onLoad, comprobar si existe un voto previo, mostrar y comunicar que se realizará un update sobre el antiguo
        e.preventDefault();
//console.log(e.target.elements)

        const rating = {
            'userId': e.target.elements.user.value,
            'resourceId': resourceId,
            'contents': e.target.elements.contents.value, 
            'ux': e.target.elements.ux.value,
            'contentsTag': 'nice',
            'uxTag': 'site'
        }
        // faltaría recibir en props: userId, resourceId, al navegar hasta esta página
        axios
            .post(`/ratings`, 
                rating)
            .then((res) => {
                console.log(res.data.data)
                const {data} = res.data
                if(data.acknowledged) {
                    setMessage('You updated it');
                    fetchRating(rating.userId);
                    //setRating(rating)
                    return;
                }
                else {
                    setRating(data);
                    setMessage(`You rated it!`);

                    return;
                
                }
            })
            .catch((e) => {
                console.log(e);
            });

        e.target.reset();

    }

    useEffect(() => {
       
        // al cargar preguntar si votó anteriormente
        const id = userRef.current.value;
        console.log(id);
        fetchRating(id);
        if(rating) {
            setMessage('You rated this resource recently! Really want to update it?')
        }

    }, [contents,ux]); // [ message ]

    return (
        
        <div className="border border-2 border-cyan-700 rounded-lg p-5 m-5 space-y-4">

            <h4 className="flex-row font-bold text-xl text-center">
                {`Your rating for this resource: ` }
            </h4>
            <DescriptionList>
                <DescriptionItem label="URL">
                    {url}   
                </DescriptionItem>
                <DescriptionItem label="Title">
                    {title}   
                </DescriptionItem>
          </DescriptionList>
            <Paragraph><i>
                {`Rating = ${getSum(contents, ux)} => ${contents} for contents and ${ux} for UX` }
            </i></Paragraph>
            <div className="grid ">
            <form onSubmit={handleSubmit} className="space-y-3">
                <input 
                    name="user" 
                    type="hidden" 
                    size="24" 
                    readOnly={true}
                    ref={userRef}
                    value={user._id}
                />
                <FormLabel label="contents" />
                    <input
                        className={`${props.inputStyles} bg-gray-100/80 border text-gray-700 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500` }
                        value={contents}
                        type="number"
                        name="contents"
                        min="1"
                        max="5"
                        step="0.5"
                        size="3rem"
                        onChange={(e) => setContents(e.target.value)}
                    />
                <FormLabel label="ux" />
                    <input
                        className={`${props.inputStyles} bg-gray-100/80 border text-gray-700 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500` }
                        value={ux}
                        type="number"
                        name="ux"
                        min="1"
                        max="5"
                        step="0.5"
                        size="3rem"
                        onChange={(e) => setUx(e.target.value)}
                    />

                <Button label={rating ? "update" : "submit" } type="submit" handleClick={handleSubmit} />

                {
                    message &&
                    <div>
                        <HelpContainer styles="p-2 m-2">
                            <Paragraph styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">
                                {message} <NavLink className="text-slate-500 hover:text-blue-500 underline" to="/" state={{'resourceId': resourceId }}>Rate more</NavLink>
                            </Paragraph>
                        </HelpContainer>
                    </div>
                }
                {
                    rating &&
                    <span className="space-x-5 m-5">
                        <KeyValuePair
                        keyStyles=""
                        valueStyles="italic"
                        label={"contents"}
                        value={rating.contents}
                        >
                        <ImNewspaper className="text-lg" />

                        </KeyValuePair>
                        <KeyValuePair
                        keyStyles=""
                        valueStyles="italic"
                        label={"ux"}
                        value={rating.ux}
                        >
                        <ImNewspaper className="text-lg" />
                        </KeyValuePair>

                        <KeyValuePair
                        keyStyles=""
                        valueStyles="italic"
                        label={"timestamp"}
                        value={rating.updatedAt.slice(0,10)}
                        >
                        <ImNewspaper className="text-lg" />
                        </KeyValuePair>     
                    </span>
                }
            </form>
            </div>
        </div>
        
    )
}