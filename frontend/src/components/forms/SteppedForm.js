import {  useState, useEffect, useContext } from "react";
import { useNavigate, NavLink, useOutletContext } from "react-router-dom"; 
import axios from 'axios';
import StepSelect from "./StepSelect";
import {StepSearchContext} from "../context/StepSearchContext";

import { SiteDataContext } from "../context/SiteDataContext";
import ComponentLabel from "../labels/ComponentLabel";

/**
 * REMODELAR !!! steppedSearch solo se modifica al cambiar de steps, como referencia guardada
 * // usar stepped como auxiliar al navegar de nuevo (steppedSearch?...:..)
 * 
 * // pero los componentes deben trabajar con states propios, no depender del context fuera de estado
 *  // se guardan al contexto por si debiera volver a cargarse
 * // collect debería llamar a un form submit()
 * // El objeto stepped puede construirse a partir de steppedSearch.fields en el propio SteppedForm
 * // Las rutas de submit y count y los nameSpaces de fields y values deben ser configurables en Stepped form
 * // para ser reutilizable, Stepped form lo recibe como props todo, para ser agnóstico al scope
 * // 
 */


export default function SteppedForm (props) {
    //const [resources, setResources] = useState([]);
    // es mejor si recibe por props.siteData la config y solo guarda en contexto stepped y current
    const navigate = useNavigate();
    const [formValues] = useOutletContext();
    // Debería ser reutilizable con el arreglo de nombres que se le pase desde siteData
    const steppedSearch = useContext(StepSearchContext);
    const siteData  = useContext(SiteDataContext);
    //const [index,setIndex] = useState();
    const [remaining, setRemaining] = useState([]);
    const [collectCount, setCollectCount] = useState(0);
    const [valuesCount, setValuesCount] = useState([]);

    // Terminar objeto categories
    // Pruebas con resources, calcular filtrado en cada step y actualizar totales por categoria

    let handleShowList = (e) => {

        // pasa a formValues.results, y navega a SearchPage
    }

    let handleFieldChange = (field) => {
        console.log("event" + field)

        const query = queryObj(steppedSearch.stepped);

        query['fieldSelected']  = field;

        axios
            .get("/search/count/values", {params: query })
            .then( (res) => {
                console.log("count: " + res.data.data)
                let countObj = {};
                let {data} = res.data;

                data.map( (row, i) => {
                    countObj[row._id] = row.count;
                })

                setValuesCount(countObj);
            })
            .catch( (e) => {
                console.log(e);
            })

        //console.log(value)
    }

    let handleValueChange = (value) => {
        //console.log(value)
    }

    let handleClear = (value) => {
        //console.log(value)
        steppedSearch.stepped.fields = [];
        steppedSearch.stepped.values = [];
        steppedSearch.current = 0;
    }

    let handleCollect = () => {

        const query = queryObj(steppedSearch.stepped);

        axios
        .get("/search/count/collect", {params: query })
        .then( (res) => {
            steppedSearch.results = res.data.data;
            props.handleStepsCollect(res.data.data);
        })
        .catch( (e) => {
            console.log(e);
        })
        
    }


    let handleStepChange = (step, field, value) => {
//console.log(value)
        steppedSearch.stepped.fields[steppedSearch.current] = field;
        steppedSearch.stepped.values[steppedSearch.current] = value;

        const length = steppedSearch.fields.length;
        if ( step  <  length && step >= 0 ) {
            steppedSearch['current'] = step;
        }

        console.log('Steps:' + steppedSearch.stepped.values)
    }

    const queryObj = (steps) => {
        const {fields, values} = steps;
        let obj = {};

        fields.map( (field, i) => {
            obj[field] = values[i];
        })

        return obj;

    }
    ///
    /// REVISAR!!!! estados, abuso effects, pasar a eventos funciones
    // función count total, y count cats separadas para poder llamar, transportar y experimentar
    useEffect(() => {

        const remain = [];

        const calcSteps = steppedSearch.stepped.fields.slice(0, steppedSearch.current);
        //console.log("fields stepped: " + calcSteps)
        if(steppedSearch.fields) {
            steppedSearch.fields.map( (field, i) => {
                if( ! calcSteps.includes(field) ) {
                    remain.push(field);
                }
            })
        }

        steppedSearch.stepped.fields = calcSteps;
        steppedSearch.stepped.values = steppedSearch.stepped.values.slice(0, steppedSearch.current);

        setRemaining(remain);
        
        return () => ''; //console.log('Stepper unmounting...');
    }, [steppedSearch.current]); // <-- add this empty array here

    useEffect(() => {
        // Reducir númmero renders y useEffect, pasar la llamada al evento StepChange
        // useReducer para simplificar ?
        
        // Evitar tantas referencias de dependencias, centralizar todo al evento

        const query = queryObj(steppedSearch.stepped);

        // HAY QUe modificar modelo Resource, nombres de campos, y tipos de dato, para poder
        // aprovechar los labels de categories

        axios
            .get("/search/count", {params: query})
            .then( (res) => {
                //console.log("count: " + res.data.data)
                setCollectCount(res.data.data);
            })
            .catch( (e) => {
                console.log(e);
            })

        


        return () =>'';// console.log('Step unmounting...');

    }, [steppedSearch.current]); // <-- add this empty array here


// handleCollect -> restringir en el step#1, mensaje 'no hay nada que filtrar, ¿realmente deseas buscar en el <link to="/tree">listadocompleto ?'
    return (
        <div className={`${props.divStyles} grid justify-center space-y-3`}>
            <ComponentLabel label="guided search" styles="text-center text-xl" />
            <nav className="flex flex-col text-center underline text-slate-500 space-y-2">
                <NavLink className="hover:text-blue-500" onClick={handleClear}>Clear</NavLink> 
                <NavLink className="hover:text-blue-500" onClick={handleCollect}>Collect<i>{`(${collectCount})`}</i></NavLink> 
            </nav>
            <StepSelect
                current ={steppedSearch.current}    
                fields={remaining}
                valuesCount={valuesCount}
                fieldSelected={steppedSearch.stepped.fields[steppedSearch.current]}
                valueSelected={steppedSearch.stepped.values[steppedSearch.current]}
                siteData={siteData}
                handleStepChange={handleStepChange}
                handleFieldChange={handleFieldChange}
                handleValueChange={handleValueChange}
            />
            
        </div>
    )
}