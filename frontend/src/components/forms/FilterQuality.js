import { useState } from "react";

function FilterQuality(props) {
    const [selected, setSelected] = useState(props.selected);
    //console.log(selected);

    const [isOpen, setIsOpen] = useState(false);

    function toggle() {
        setIsOpen((isOpen) => !isOpen);
  
    }

    function handleChange (e) {

        props.handleQuality(e.target.value);
        setSelected(e.target.value);
        
    
    }

    // PENDIENTE: Convertir a componente auto-escalable, iterador con props.labels/values y state.Selected

    // Migrar a genérico FilterSelect

    return (
        <div>
            
            {   isOpen &&
            <select onChange={handleChange} name="select" value={selected}>
                <option value="aceptable">{'aceptable'}</option>
                <option value="buena" >{'buena'}</option>
                <option value="muy buena" >{'muy buena'}</option>
                </select>
            }      
            
            <button onClick={toggle}>{props.label}</button>
        </div>
        
    )
}

export default FilterQuality;