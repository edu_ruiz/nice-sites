import { useState, useEffect } from "react";
import axios from "axios";

import FormLabel from "../labels/FormLabel";
import FormContainer from "../containers/FormContainer";


export default function UserLogin(props) {
    
    /*const [nick, setNick] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();*/
    const [formData, setFormData] = useState({
        nick: '',
        email: '',
        password: '',
        confirm: ''
    });

    const handleSubmit = (e) => {
        e.preventDefault();

        //console.log(formData);

        axios
          .post("/auth/login", formData )
          .then( (res) => {
            //console.log(res.session)
            props.setUser(res.data.data);
            props.handleClose();
          })
          .catch( (e) => {
            console.log(e)
          })
    }
    

    const handleEmail = (e) => {
        setFormData((prev) => ({ ...prev, ['email'] : e.target.value}));
    }

    const handlePassword = (e) => {
        setFormData((prev) => ({ ...prev, ['password'] : e.target.value}));
    }


    useEffect( () => {
        
    },[])


    return (
        <FormContainer 
            label="User login" 
            handleSubmit={(e) => handleSubmit(e)} 
            handleClose={props.handleClose}
        >
                        
                        <li className="flex flex-col">
                        <FormLabel styles={ "text-gray-600"} label={"email"} />
                            <input
                                type="email"
                                name="email"
                                required={true}
                                placeholder="your@mail.here"
                                value={formData.email}
                                onChange={handleEmail}
                            />
                        </li>
                        <li className="flex flex-col">
                        <FormLabel styles={ "text-gray-600"} label={"password"} />
            
                            <input
                                type="password"
                                name="password"
                                required={true}
                                placeholder="***"
                                value={formData.password}
                                onChange={handlePassword}
                            />
                        </li>
                        
                    </FormContainer>

    )
}
