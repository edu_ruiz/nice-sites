import { useState } from "react";
// {isOpen && <div>OPEN</div>}

function SearchFilter(props) {
    const [isOpen, setIsOpen] = useState(true);
    const [categories, setCategories] = useState(props.categories );
    const [namesChecked, setNamesChecked]= useState(props.checked);
    const [checked, setChecked] = useState(getArrayChecked(namesChecked));
    const [checkedNamed, setCheckedNamed] = useState(getCheckedNamed);

    function getCheckedNamed () {

        const chk = [];
        categories.map( (category, i) => {

            const obj = {};
            obj.name = category;
            obj.checked = checked[i];

            chk.push(obj);
        });

        return chk;
    }

    function getArrayChecked (nms) {
        const chk = [];
        categories.map( (category) => {
            if (nms.includes(category)) {
                chk.push(true)
            }
            else chk.push(false)
        })
        
        return chk;

    }

    function toggle() {
      setIsOpen((isOpen) => !isOpen);

    }

    function changeHandler (e) {
        const chks = Array.from(checkedNamed)

        checkedNamed.map( (chk) => {
            if ( chk.name === e.target.name ) {
                chk.checked = e.target.checked;
            }
        })

        setChecked(chks);

        // comunicar al padre los cambios
    }
  
    return (
        <ul>
        {   isOpen &&
            checkedNamed.map((chknm, i) => (
                <li key={i}>
                    <label htmlFor={chknm.name}>
                        {chknm.name}
                    </label>
                    <input 
                        type="checkbox"
                        name={chknm.name}
                        key={i}
                        checked={chknm.checked}
                        onChange={changeHandler}/>
                </li>
            ))
        }

        <button onClick={toggle}>Categories</button>
        
      </ul>
    );
  
} 
export default SearchFilter;