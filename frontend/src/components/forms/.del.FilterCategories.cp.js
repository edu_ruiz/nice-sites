import { React, useState, useEffect } from "react";


// Se podría añadir una función para comportamiento tipo Radio Button, una sola selección
function FilterCategories (props) {
    const [categories,setCategories] = useState([]);
    const [namesChecked]= useState(props.checked);
    const [checked, setChecked] = useState();
    const [checkedNamed, setCheckedNamed] = useState();
console.log(props)
    function getCheckedNamed (chks) {

        const chk = [];
        categories.map( (category, i) => {

            const obj = {};
            obj.name = category;
            //if( props.typeOption === "checkbutton")
            obj.checked = chks[i];

            chk.push(obj);
        });

        return chk;
    }

    function getArrayChecked (nms) {
        const chk = [];
        categories.map( (category) => {
            if (nms.includes(category)) {
                chk.push(true)
            }
            else chk.push(false)
        })
        
        return chk;

    }

    /// Método para obtener de vuelta array de nombres checked
    function getNamesFromChecked (named) {
        const nms = [];

        named.map( (chk, i) => {
            if( chk.checked ) {
                nms.push(chk.name);
            }
        })
        return nms;
    }

    function handleChange (e) {

        const  chks = Array.from(checkedNamed)
        // if typeOption === "checkbutton"
        chks.map( (chk) => {
            if ( chk.name === e.target.name ) {
                chk.checked = e.target.checked;
            }
            else if ( props.typeOption === "radiobutton" ) {
                chk.checked = false;
            }
        })
        
        setChecked(chks);

        const nms = getNamesFromChecked(chks)

        props.handleChecked(nms, props.name);
/// 

    }

    useEffect(() => {
        setCategories(props.categories);
        setChecked(getArrayChecked(namesChecked));
        setCheckedNamed(getCheckedNamed(checked));
        return () => '';
    }, []); // <-- add this empty array here

    const render = () => {
        console.log(checkedNamed)
        return (
            <div className="form-border">
                <h4 className="filter-title">{props.title}</h4>
                <div className="filter-group">
                    <strong>{`${props.label} `}</strong>
                    {  
                    checkedNamed.map((chknm, i) => (
                        <span key={i}>
                            <label htmlFor={chknm.name}>
                                {chknm.name}
                            </label>
                            <input 
                                type="checkbox"
                                name={chknm.name}
                                key={i}
                                checked={chknm.checked}
                                onChange={handleChange} 
                            />
                        </span>
                    ))
                }
                </div>         
            </div>
        );
    }

    return (
        
        categories ? render() : ''  
    );
  
} 
export default FilterCategories;