import { React, useState, useEffect } from "react";

export default function SelectOption(props) {
    const [selected, setSelected] = useState();
    const [options, setOptions] = useState();

    const handleChange = (e) => {

        props.handleSelected(e);
        setSelected(e.target.value); 
    }

    useEffect( () => {
        setSelected(props.selected);
        //setOptions(props.options);
    },[])

    return (
        <label htmlFor={props.label}>
            {props.label}
            {   props.options &&
            <select onChange={handleChange} disabled={props.disabled} name={props.label} value={selected}>
                { props.options.map( (option, i )  => {
                    return <option key={i} value={option}>{option}</option>
                }) }
            </select>
            }      
        </label>
        
    )
}
