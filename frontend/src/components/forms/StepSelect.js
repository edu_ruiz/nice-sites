import {useState, useEffect } from "react";
import { NavLink } from "react-router-dom"; 

import { TiArrowLeftOutline, TiArrowRightOutline } from "react-icons/ti";

import SelectOption from "./SelectOption";

export default function StepSelect (props) {

    //const siteData  = useContext(SiteDataContext);

    // Backup y ajustar, renderizan demasiadas veces
    // 
    // Hay incoherencias:
    // - no hay default select, no se propagan los valores si no hay onChange
    // - no se visualizan los conteos desde el inicio, no hay llamada a server
    // - on back-forward no hay valores por defecto, no hay llamada
    // - el estado del form parece que sí se mantiene

    // probar a resolver todo en los eventos, sin useEffect, 
    // o probar a manejar los eventos dentro del propio useEffect, suscribir y cancelar on unmount


    const [field, setField] = useState('');  
    const [value, setValue] = useState('');

    //const [remaining, setRemaining] = useState([]);
    //const [fieldsCount, setFieldsCount] = useState([]);
    //const [valuesCount, setValuesCount] = useState([]);

    const handleFieldSelected = (e) => {
        setField(e.target.value);
        props.handleFieldChange(e.target.value)
    }
    const handleValueSelected = (e) => {
        setValue(e.target.value);
        props.handleValueChange(e.target.value);
    }
    useEffect(() => {
        
        //console.log('Step mounted');
        if(props.fieldSelected && props.valueSelected) {
            setField(props.fieldSelected);
            setValue(props.valueSelected);
        }
        else if(props.siteData && props.fields) {
            setField(props.fields[0]);
            let {fields} = props;
            if(props.siteData[field]) {
            setValue(props.siteData[field][1]);  
            }    
        }

        //console.log(props.siteData)
            return () => ''; //console.log('Step unmounting...');
        }, [props.fields, props.siteData]); // <-- add this empty array here

    useEffect(() => {

        setField(props.fieldSelected);
        setValue(props.valueSelected);

        // llamar a /search/count

        return () =>'';// console.log('Step unmounting...');

    }, [props.current]); // <-- add this empty array here

    /**
     * 
     * <select name={`step-${props.current}`} onChange={handleFieldSelected} value={field}>
                {
                    props.fields.map((label, i ) => {
                        return (
                            <option key={i} value={label}>{label}</option>
                        )
                    })
                }
                </select>

     */

    // Para poder incluir la cuenta en el SelectOption hay que pasarle el
    // objeto de labels ya preparado, no el siteData, 
    // o incluir una prop.optionsData en el SelectOption

    let render = () => {
        //let {formData} = props;
        return (
            <div className="space-y-3">
                <nav className="flex justify-between underline text-slate-500 space-x-3">
                    <NavLink 
                        className="hover:text-blue-500 inline-flex items-center space-x-1" 
                        onClick={(e) =>props.handleStepChange(props.current - 1, field, value)}
                    >
                        <TiArrowLeftOutline/>
                        <span>Back</span>
                    </NavLink>
                    <NavLink 
                        className="hover:text-blue-500 inline-flex items-center space-x-1" 
                        onClick={(e) => props.handleStepChange(props.current + 1, field, value)}
                    >
                        <span>Next</span>
                        <TiArrowRightOutline className="" />
                    </NavLink>
                </nav>
                <h5 className="text-lg text-slate-600 italic font-semibold text-center">{`Step#${props.current + 1}`}</h5>
                <div className="md:grid md:grid-cols-2 space-x-3">
                <SelectOption
                    label={`select category`}    
                    options={props.fields}
                    selected={field} 
                    handleSelected={handleFieldSelected}
                />
                <SelectOption
                    label={`select value`}    
                    options={props.siteData[field]}
                    selected={value} 
                    handleSelected={handleValueSelected}
                />
                {
                    false && props.siteData &&
                    <select name={field} onChange={handleValueSelected} value={value}>
                    {  props.siteData[field] &&
                        props.siteData[field].map((cat, j ) => {
                            return (
                                <option key={j} value={cat}>{cat}{` (${props.valuesCount[cat]})`}</option>
                            )
                        })
                    }
                    </select>
                }
                </div>
                
            </div>

        )
        
    }

    return (
        <>
          { props ? render(): 'Error Step'}  
        </>
       
    )
}