import { useState, useEffect } from "react";
import axios from "axios";

import InputText from "./InputText";
import FormContainer from "../containers/FormContainer";

export default function UserRegister(props) {
    /*const [nick, setNick] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();*/
    const [formData, setFormData] = useState({
        nick: '',
        email: '',
        password: '',
        confirm: ''
    });

    const [message, setMessage] = useState('');

    const handleSubmit = (e) => {

        console.log(formData);

        if(formData.password === formData.confirm) {

            axios
                .post("/users", formData)
                .then( (res) => {
                    const {data} = res.data;
                    props.handleUserCreated(data);
                })
                .catch( (e) => {
                    setMessage('Got error');
                })
        }
        else {
            setMessage('password missmatch');
        }


    }
    
    const handleNickChange = (e) => {
        
        setFormData( (prev) => ({ ...prev, ['nick'] : e.target.value }));
    }

    const handleEmailChange = (e) => {
        setFormData( (prev) => ({ ...prev, ['email'] : e.target.value }));
    }

    const handlePasswordChange = (e) => {
        setFormData( (prev) => ({ ...prev, ['password'] : e.target.value }));
    }

    const handleConfirmChange = (e) => {
        setFormData( (prev) => ({ ...prev, ['confirm'] : e.target.value }));
    }



    useEffect( () => {
        
    },[])

// handleView={(view) => props.handleView(view)}
    return (
        <FormContainer 
            label="New User form register" 
            handleSubmit={handleSubmit} 
            handleClose={props.handleClose}
            
        >
                        <li className="flex flex-col">
                            <InputText
                                input={{
                                    label:"nickname",
                                    type: "text",
                                    name: "nick",
                                    required: true,
                                    placeholder: "nick-short_@name",
                                    titleTooltipRequired: "Choose an unique nickname"                         
                                }}
                                focus={true}
                                value={formData.nick.value}
                                handleInputValue={handleNickChange}
                                disabled={null}
                            />
                        </li>
                        <li className="flex flex-col">
                            <InputText
                                input={{
                                    label:"email",
                                    type: "email",
                                    name: "email",
                                    required: true,
                                    placeholder: "your@mail.com",
                                    titleTooltipRequired: "Write a valid email"                         
                                }}
                                focus={true}
                                value={formData.email.value}
                                handleInputValue={handleEmailChange}
                                disabled={null}
                            />
                        </li>
                        <li className="flex flex-col">
                            <InputText
                                input={{
                                    label:"password",
                                    type: "password",
                                    name: "password",
                                    required: true,
                                    placeholder: "******",
                                    titleTooltipRequired: "Write a secure password"                         
                                }}
                                focus={true}
                                value={formData.password.value}
                                handleInputValue={handlePasswordChange}
                                disabled={null}
                            />
                        </li>
                        <li className="flex flex-col">
                            <InputText
                                input={{
                                    label:"confirm password",
                                    type: "password",
                                    name: "confirm",
                                    required: true,
                                    placeholder: "******",
                                    titleTooltipRequired: "Confirm password"                         
                                }}
                                focus={true}
                                value={formData.confirm.value}
                                handleInputValue={handleConfirmChange}
                                disabled={null}
                            />
                        </li>

                        <span className="text-red-500 text-sm">{message}</span>
                    </FormContainer>

    )
}
