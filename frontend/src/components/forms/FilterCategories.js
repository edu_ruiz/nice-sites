import { useState, useEffect } from "react";


// Se podría añadir una función para comportamiento tipo Radio Button, una sola selección
function FilterCategories (props) {
    const [categories,setCategories] = useState([]);
    const [namesChecked, setNamesChecked]= useState([]);

    const isChecked = (category) => {

        if ( namesChecked.includes(category)) {
            return category;
        }
        else return '';     
    }

    const handleChange = (e) => {

        let checks = [];

        switch (props.typeOption) {
            case 'radiobutton': checks = [e.target.name] ; break;
            case 'checkbutton': {
                if( e.target.checked ) {
                    if( ! namesChecked.includes(e.target.name)) {

                        checks = Array.from(namesChecked);
                        checks.push(e.target.name)
                    }
                }
                else {
                    namesChecked.map( (category, i) => {
                        if ( category !== e.target.name ) {

                            checks.push(category);
                        }
                    });

                }
            }; break;
            default: setNamesChecked([]);
        }
        setNamesChecked(checks)

        props.handleChecked(checks, props.name)
    }

    useEffect(() => {

        setCategories(props.categories);
        setNamesChecked(props.checked);

        return () => '';
    }, []); // */

    // ampliar zona click para los checkbox !! con otro handle en el <li>
    return (
        <div className="z-20 rounded-lg border-2 m-2 text-gray-500 ">
            {props.children}
                <h5 className="font-bold pt-5 text-lg text-center mr-4">{`_${props.label} `}</h5>
                <ul className="p-4 pl-10 grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 text-lg justify-items-start">
                {         
                    categories.map((category, i) => {
                        return (
                            <li key={i}>
                                <div className="cursor-pointer flex items-center p-2 rounded hover:bg-gray-100 dark:hover:bg-gray-600">
                                    <input 
                                        type="checkbox"
                                        name={category}   
                                        value=""
                                        checked={isChecked(category)}
                                        onChange={handleChange}
                                        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-700 dark:focus:ring-offset-gray-700 focus:ring-2 dark:bg-gray-600 dark:border-gray-500"
                                    />
                                    <label htmlFor={category} className="w-full ms-2 text-md md:text-lg xl:text-lg font-medium text-gray-900 rounded dark:text-gray-300">
                                        {category}
                                    </label>
                                
                                </div>
                            </li>
                        );        
                    })
                }
                </ul>
        </div>  
    );
} 
export default FilterCategories;