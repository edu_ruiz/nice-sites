import React, { useState, useEffect,useRef } from "react";
import FormLabel from "../labels/FormLabel";
import axios from "axios";
//import "../../App.scss";

export default function InputText (props) {
    const [value, setValue] = useState();
    const [errors, setErrors] = useState();
    const inputRef = useRef();

    let checkValidity = (event) => {

        let { validity } = event.target;
        let { name } = event.target;
        let er = [];
        let message = null;


        if (!validity.valid) {
            
            for (let key in validity) {
                //console.log(key)
                if (validity[key] === true) {
                    
                    

                    switch (key) {
                        case 'valueMissing': message = "Can't be empty"; break;
                        case 'typeMismatch': message = "Wrong characters"; break;
                        case 'patternMismatch': message = "Not well formed"; break;
                        case 'tooLong': message = "Max size exceeded"; break;
                        case 'tooShort': message = "Minimum size required"; break;
                        case 'rangeUnderflow': message = "Minimum in range required"; break;
                        default: message = "other error";
                    }
                    er.push(message);
                    
                }
            }
            setErrors(er);
        }
        else {
            setErrors([]);
        }
    }

    const handleBlur = (e) => {
        checkValidity(e);
        //console.log(errors.toString())
        props.handleInputValue(e);
    }

    const handleClick = (e) => {
        //console.log("CLICK")
        if ( props.disabled ) {
            setErrors( (prev) => ([`You should give a valid 'URL'` ]))
        }
    }

    useEffect( () => {
        setValue(props.value);
        setErrors([]);
        
        if( props.focus ) {
            inputRef.current.focus();
        }
    }, [])

    // Insertar validación interna en handleChange()

    // className={ `block w-full`+ (errors[input.label].length ? "border-2 border-red-500" : "border-0")}
    const handleUrlBlur = (event) => {

        if( props.input.type === 'url' ) {
            let msg = '';

        // from validity get html validation and errors
        //checkValidity(event);

            const { value } = event.target;

            // REVISAR COMPORTAMIENTO, unas ops desbloquean el campo,
            // revisar orden de llamadas validity y llamadas a Errors y Disabled
            if (value.length > 0) {

                // AÑADIR:  revisar si el url es el home page, y restringir formulario a categoría 'sites'
                axios
                    .get(`/search/url`, { params: { 'url': value } })
                    .then((res) => {

                        if (res.data.data) {
                            msg = 'This resource already exists';
                            // FALTA ofrecer un enlace para visitar la coincidencia
                            let er = [];
                            er.push(msg)
                            props.handleDisabled('disabled');
                            //setErrors((prevErrors) => ({ ...prevErrors, ['url']: er, ['title']: [] }));
                            setErrors((prevErrors) => ([ ...prevErrors, msg ]));
                        }
                        else {
                            // check if it's valid domain
                            axios
                                .get(`/blacklists/check-domain`, { params: {url: value}})
                                .then( (res) => {
                                    if(res.data.data) {
                                        msg = `Can't insert anything from this domain`
                                        props.handleDisabled('disabled');
                                        //setErrors((prevErrors) => ({ ...prevErrors, ['url']: [msg], ['title']: [] }));
                                        setErrors((prevErrors) => ([ ...prevErrors, msg ]));
                                    }
                                    else {
                                        // if valid unlock whole form
                                        props.handleDisabled();
                                    }
                                })
                        }
                    })
                    .catch((e) => {
                        console.log(e.errors)
                    });
            }
        }
        checkValidity(event);
        props.handleInputValue(event);
    } 
    
    const handleChange = (e) => {
        setValue(e.target.value)
        props.handleInputValue(e);
    }
    
    return (
        <div className={`relative ${props.divStyles}`} onClick={handleClick}  >
            <FormLabel styles={ props.disabled ? "text-gray-300" : "text-gray-600"} label={props.input.label} />
            <input 
                type={props.input.type ? props.input.type : "text"} 
                name={props.input.name ? props.input.name : props.input.label} 
                className={`${props.inputStyles} bg-gray-100/80 border text-gray-700 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 ${props.disabled ? "border-1" : "border-2"} ${errors && errors.length > 0 ? "border-red-500" : "border-gray-400"}` }
                placeholder={props.input.placeholder} 
                value={value}
                size={props.input.size}
                maxLength={props.input.maxLength}
                minLength={props.input.minLength}
                min={props.input.min}
                max={props.input.max}
                step={props.input.step}
                pattern={props.input.pattern}
                disabled={props.disabled}
                ref={inputRef}
                title={props.input.tooltipRequired}
                required={props.input.required}
                onChange={handleChange}
                onBlur={props.input.type === 'url' ? handleUrlBlur : handleBlur}
                                     
            />

            {errors && errors.length > 0 && <span className="ml-3 text-sm text-red-400">{errors.toString()}</span>}
                            
        </div>
    )
}



