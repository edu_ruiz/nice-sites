import React, { useState, useEffect, useContext } from "react";
import axios from "axios";


//import {SiteDataContext} from "../context/SiteDataContext";
import history from 'history/browser'; 
import { SiteDataContext } from "../context/SiteDataContext";
import SelectOption from "./SelectOption";
import UserContext from "../context/UserContext";
import InputText from "./InputText";

export default function ResourceUpdate (props) {
    const [formInputs, setFormInputs] = useState();
    const [formSelects, setFormSelects] = useState();
    const [fields, setFields] = useState();
    const [queryObj, setQueryObj] = useState();
    const [errors, setErrors] = useState();
    const siteData = useContext(SiteDataContext);
    const [message, setMessage] = useState();

    // Feed Popular Resources List and other sections while SearchBar value === ''
    useEffect(() => {
        if( props.field && props.resource ) {
                
                
            let keys = Object.keys(props.resource[props.field]);
            let fields = props.resource[props.field];
            //let er = {};
            keys.map( (field, i) => {
                setQueryObj( (prev) => ({ ...prev, [field] : fields[field]}))
                setErrors( (prev) => ({ ...prev, [field] : [] }))
            }); // */
            
            setQueryObj( (prev) => ({ ...prev, ['id'] : props.resource._id }))
            setQueryObj( (prev) => ({ ...prev, ['field'] : props.field }))

            //console.log(Object.keys(props.resource[props.field])); // */
        
        }
        

        axios.get("/forms/resource") // /verified")
        .then( (res) => {
            
            //setForm(res.data.data);
            const {data} = res.data;
            
            setFormInputs(data.inputs);
            setFormSelects(data.selects);
            
            //setErrors(er);
            //setQueryObj(obj);
        //setField(props.field);
        })
        .catch( (e) => {
            console.log(e);
        })

        return () => '';
        
    }, []); // */

    /* useEffect( () => {
        props.handleUpdate();

    }, [message]) */

    const handleSelect = (e) => {

        setQueryObj( (prev) => ({ ...prev, [e.target.name] : e.target.value }))
    }

    // FALTARía muestra de errores
    const handleBlur = (e) => {
        // incorporar validación validity
        console.log(queryObj)
        setQueryObj( (prev) => ({ ...prev, [e.target.name] : e.target.value }))
    }

    const handleChange = (e) => {
        setQueryObj( (prev) => ({ ...prev, [e.target.name] : e.target.value }))
    }

    const handleInputValue = (name, value) => {
        setQueryObj( (prev) => ({ ...prev, [name] : value }))
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        //console.log("UPDATE: " + JSON.stringify(queryObj))

        // axios put , escribir ruta
        axios
            .put("/resources", queryObj)
            .then( (res) => {
                if (res) {
                    
                    props.handleUpdate();

                }
            })
            .catch( (e) => {
                console.log(e)
            }) // */

        // Informar del resultado
    }
    // onSubmit, si tiene éxito, propagar al padre para ocultar form

    // Mostrar form según nombre de campo
    // Incluir field 'updated' en form contents
    // Incluir tags en form properties
        return (
        <>
            <form onSubmit={handleSubmit}>
            {
                props.field && queryObj && props.user &&
                <>
                    <h4>{`Update ${props.field}`}</h4>
                    <input type="hidden" value={props.user._id} />
                    <input type="hidden" value={'verify'} />
                    {
                        formInputs && queryObj &&
                        <div class="grid gap-6 mb-6 md:grid-cols-2">

                        {formInputs.map( (input, i) => {
                            if(Object.keys(queryObj).includes(input.label)) {
                                return (
                                    <>
                                    <InputText 
                                        input={input}
                                        value={queryObj[input.label]}
                                        handleInputValue={handleInputValue}
                                        labelStyles=""
                                        inputStyles=""
                                    /> 
                                    <label htmlFor={input.label} key={i}>
                                        {input.label}
                                    <input
                                        type={input.type}
                                        label={input.label}
                                        name={input.label}
                                        value={queryObj[input.label]}
                                        required={false}
                                        title={input.tooltipRequired}
                                        className={errors[input.label].length ? "border-danger" : "border-muted"}
                                        size={"20"}
                                        maxLength={input.maxLength}
                                        minLength={input.minLength}
                                        min={input.min}
                                        max={input.max}
                                        step={input.step}
                                        pattern={input.pattern}
                                        disabled={false}
                                        placeholder={input.placeholder}
                                        onChange={handleChange}
                                    />
                                    {errors[input.label] && <span className="text-danger">{errors[input.label].toString()}</span>}
                            
                                    </label>
                                    </>
                                    
                                    
                                )
                            }
                            
                        })}
                        </div>
                    }
                    {
                        formSelects && siteData &&
                        formSelects.map( (field, i) => {
                            if(Object.keys(queryObj).includes(field)) {
                                return (
                                    <SelectOption 
                                    key={i}
                                    label={field}    
                                    options={siteData[field]}
                                    disabled={false}
                                    selected={queryObj[field]} 
                                    handleSelected={handleSelect} />
                                )
                            }
                            
                        })
                    }
                </>
                
            }
                <input type="submit" value="Update"/>
            </form>
            
        </>
    )
}