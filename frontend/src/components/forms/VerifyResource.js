import {  useState, useEffect, useContext } from "react";
import { useLocation, NavLink, useNavigate } from "react-router-dom"; 
import axios from 'axios';
import { MdOutlineChildCare, MdOutlineEdit } from "react-icons/md";
import UrlBlock from "../lists/UrlBlock";
import ResourceUpdate from "./ResourceUpdate";
import UserContext from "../context/UserContext";

import ComponentLabel from "../labels/ComponentLabel";
import Paragraph from "../elements/Paragraph";
import HelpContainer from "../containers/HelpContainer";
import IconOrLabel from "../elements/IconOrLabel";


export default function VerifyResource (props) {
    const location = useLocation();
    const navigate = useNavigate();
    const user = useContext(UserContext);
    const [resource, setResource] = useState();
    const [formValues, setFormValues] = useState();
    const [message, setMessage] = useState();
    const [viewForm, setViewForm] = useState();
    const [viewWorking, setViewWorking] = useState();
    const [viewContents, setViewContents] = useState();
    const [viewProperties, setViewProperties] = useState();
    const [contentsUpdateForm, setContentsUpdateForm] = useState();
    const [propertiesUpdateForm, setPropertiesUpdateForm] = useState(); 
    const [verifySteps, setVerifySteps] = useState();
    const [blacklistForm, setBlacklistForm] = useState();
    const [rejectedMsg, setRejectedMsg] = useState('');

    const handleUrlClick = (url) => {
        setViewForm(true);
    }

    const handleWorking = (e) => {
        const {value} = e.target;
        if( value === 'no') {
            // terminar form y update stage='disabled'
        }
        else {
            setViewWorking(false);
            setVerifySteps( (prev) => ({ ...prev, ['working'] : true }));
        }
    }

    const handleContents = (e) => {
        const {value} = e.target;
        if( value === 'no') {
            // mostrar form update contents
            setContentsUpdateForm(true);
        }
        else {
            setViewContents(false);
            setVerifySteps( (prev) => ({ ...prev, ['contents'] : true }));
        }
    }

    const handleProperties = (e) => {
        const {value} = e.target;
        if( value === 'no') {
            // mostrar form update properties
            setPropertiesUpdateForm(true);
        }
        else {
            setViewProperties(false);
            setVerifySteps( (prev) => ({ ...prev, ['properties'] : true }));
        }
    }

    const handleContentsUpdate = () => {
        setContentsUpdateForm(false);
        setViewContents(false);
        setVerifySteps( (prev) => ({ ...prev, ['contents'] : true }))
        setMessage('Was Updated!');
        let timer = setTimeout(() => {
            setMessage(null);
        },1500)
    }

    const handlePropertiesUpdate = () => {
        setPropertiesUpdateForm(false);
        setViewProperties(false);
        setMessage('Was Updated!');
        setVerifySteps( (prev) => ({ ...prev, ['properties'] : true }))
        let timer = setTimeout(() => {
            setMessage(null);
        },1500)
    }

    const handleReject = () => {

        axios.put("/resources", {'id': resource._id, 'userId': user._id, 'reject': true})
            .then( (res) => {
                setMessage('Rejected! Thanks!')
                setBlacklistForm(false);
            })
            .catch( (e) => console.log(e))

    }

    const handleNavigate = () => {

        axios
            .get(`/resources/${resource._id}`)
            .then( (result) => {
                const {data} = result.data;
                navigate(`/resources/${resource._id}`, {state:{resource: data}} )
            })
            .catch( (e) => {
                setMessage("Something gone wrong");
            })
    }

    useEffect(() => {
        setMessage(null);
        setViewForm(false);
        setContentsUpdateForm(false);
        setPropertiesUpdateForm(false);
        setViewWorking(true);
        setViewContents(true);
        setViewProperties(true);
        setVerifySteps({working: false, contents: false, properties: false});
        setBlacklistForm(false);

        if(location.state) {
            setResource(location.state.resource);
        }
        return () =>''  // console.log('unmounting...');
    }, [])  // <-- add this empty array here


    useEffect( () => {
        if ( user && verifySteps && verifySteps.working && verifySteps.contents && verifySteps.properties ) {
            console.log("send verify")
            axios.put("/resources", {'id': resource._id, 'userId': user._id, 'verify': true})
            .then( (res) => {
                setMessage('Link verified! Thanks!')
                setBlacklistForm(false);
            })
            .catch( (e) => console.log(e))
        }
        else {
            // mostrar botón 'marcar para borrado y añadir a blacklist'
            setBlacklistForm(true);
        }

    }, [viewContents, viewWorking, viewProperties])
    
    return (
        <div className="grid justify-center">
            <ComponentLabel label={'verify form'} styles="text-center text-xl" />
            <HelpContainer styles="p-2 m-2">
                <Paragraph styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">
                    {'Please, you should click once over this link before proceed. Feel free to use virus scan before'}
                </Paragraph>
            </HelpContainer>
            <div className="flex justify-center">
            {
                resource &&
                <UrlBlock url={resource.url} id={resource._id} handleUrlClick={handleUrlClick} />
            }
            </div>
            {
                viewForm &&
                <div className="flex flex-col justify-center items-center">
                    {
                        blacklistForm && resource &&
                        <button 
                            type="button" 
                            className="text-slate-500"
                        >
                            <IconOrLabel handleClick={() => handleReject(resource._id)} styles="underline hover:text-red-500" label="Reject">
                            <MdOutlineEdit className="text-xl" />
                            </IconOrLabel>
                        </button>                        
                    }
                    {
                        message &&
                        <span className="">
                            {message}
                            <button
                                type="button"
                                className="mx-2 text-slate-600 underline hover:text-blue-500"
                                onClick={handleNavigate}
                            >
                                {`Go to resource`}
                            </button>
                        </span>
                        
                    }
                    {   viewWorking &&
                        <label htmlFor="working">
                        {`1. It worked as expected?`}
                        <input
                            className="mx-2 text-indigo-700 underline hover:text-blue-500"
                            name="working" 
                            type="button" 
                            value="no" 
                            onClick={handleWorking} 
                        />
                        <input
                            className="text-indigo-700 underline hover:text-blue-500"
                            name="working" 
                            type="button" 
                            value="yes" 
                            onClick={handleWorking} 
                        />
                        </label>
                    }
                    {
                        viewContents &&
                        <label htmlFor="contents">
                            {`2. Contents are as described?`}
                            <input
                                className="mx-2 text-indigo-700 underline hover:text-blue-500"
                                name="contents" 
                                type="button" 
                                value="no" 
                                onClick={handleContents} 
                            />
                            <input
                                className="text-indigo-700 underline hover:text-blue-500"
                                name="contents" 
                                type="button" 
                                value="yes" 
                                onClick={handleContents} />
                        </label>
                    }
                    {
                       contentsUpdateForm &&
                        <ResourceUpdate 
                            user={user} 
                            field="contents" 
                            resource={resource} 
                            handleUpdate={handleContentsUpdate} 
                        />
                    }
                    {
                        viewProperties &&
                        <label htmlFor="properties">
                            {`3. Properties are as described?`}
                            <input
                                className="mx-2 text-indigo-700 underline hover:text-blue-500"
                                name="properties" 
                                type="button" 
                                value="no" 
                                onClick={handleProperties} 
                            />
                            <input
                                className="text-indigo-700 underline hover:text-blue-500"
                                name="properties" 
                                type="button" 
                                value="yes" 
                                onClick={handleProperties} 
                            />
                        </label>
                    }
                    {
                        propertiesUpdateForm && resource &&
                        <ResourceUpdate 
                            user={user} 
                            field="properties" 
                            resource={resource} 
                            handleUpdate={handlePropertiesUpdate} 
                        />
                    }
                    
                </div>
            }
        </div>
    )
}
