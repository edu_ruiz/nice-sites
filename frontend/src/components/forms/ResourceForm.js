import { React, useState, useEffect, useContext, useRef } from "react";
import { useNavigate, useLocation } from "react-router-dom"; 
import axios from 'axios';
import getFilterCategories from "../helpers/categories";
import SelectOption from "./SelectOption";
import UserContext from "../context/UserContext";
import { SiteDataContext } from "../context/SiteDataContext";
import PageContainer from "../containers/PageContainer";
import PageHeader from "../elements/PageHeader";
import Paragraph from "../elements/Paragraph";
import InputText from "./InputText";
import Button from "../buttons/Button";
import FormGroup from "../containers/FormGroup";
import HelpContainer from "../containers/HelpContainer";


//INTENTAR extraer la lógica de validaciones y comportamiento campos (blur, focus, change)
// A hooks externos para reutilizarlos y dar claridad

// INTENTAR Reducir form, usar SelectOption y Input components, pasarles el [input]
// el componente asigna las props en su interior, ocultar aquí la asignación
// CAda input onChange onBlur llamará a validity por su cuenta
// y delegará al padre solamente el manejo del dato Value con funciones

export default function ResourceForm(props) {
    let urlRef = useRef();
    let navigate = useNavigate();
    let location = useLocation();
    const user = useContext(UserContext);
    const [sections] = useState(getFilterCategories().sections);
    const [sources] = useState(getFilterCategories().sources);
    const [response, setResponse] = useState('');
    const [inputs, setInputs] = useState();
    const [selects, setSelects] = useState();
    const [education, setEducation] = useState(false);
    const siteData = useContext(SiteDataContext);//useOutletContext();
    const [disabled, setDisabled] = useState('disabled');
// Es necesario declarar cada campo vacío al inicio ????
    const [formData, setFormData] = useState();
    const [errors, setErrors] = useState(); 


    // FALTA Autocargar datos previos en el form,
    // y conservar estado después del submit
    // usar Contexto ? Debe funcionar en el update y el verify, tb

    useEffect(() => {   
        
        //setOptions(props.siteData);    
        axios
            .get("/forms/resource")
            .then( (res) => {
                const {data} = res.data;
                setInputs(data.inputs);
                setSelects(data.selects);
                setFormData(data.queryObject);
                if(user) {
                    //console.log(user)
                    setFormData( (prev) => ({ ...prev, ['userId'] : '65c151d6d19fbab1411b6b87' }))
                }
                
                setErrors(data.errorsObject);
                
            })
            .catch((e) => {
                console.log(e)
            })
        
        
        

    }, []); // <-- add this empty array here
      
     
// INTENTAR extraer lógica a hook useValidity ??? revisar artículo simplify forms
    let checkValidity = (event) => {

        let { validity } = event.target;
        let { name } = event.target;

        if (!validity.valid) {
            let er = []
            for (let key in validity) {
                //console.log(key)
                if (validity[key] === true) {

                    let message = '';

                    switch (key) {
                        case 'valueMissing': message = "Can't be empty"; break;
                        case 'typeMismatch': message = "Wrong characters"; break;
                        case 'patternMismatch': message = "Not well formed"; break;
                        case 'tooLong': message = "Max size exceeded"; break;
                        case 'tooShort': message = "Minimum size required"; break;
                        case 'rangeUnderflow': message = "Minimum in range required"; break;
                        default: message = "other error";
                    }
                    er.push(message);
                    setErrors((prevErrors) => ({ ...prevErrors, [name]: er }));
                }
            }
        }
        else {
            setErrors((prevErrors) => ({ ...prevErrors, [name]: [] }))
        }
    }

    let handleSubmit = (e) => {

        e.preventDefault();
        //console.log(formData)
        axios
            .post("/resources", formData)
            .then((response) => {
                
                setResponse(response.data.data)

                return (
                    navigate(`/resources/${response.data.data._id}`, 
                                    {'state': { 
                                        'resource' : response.data.data,
                                        'path': location.pathname
                                    }}
                            )
                )
                
            })
            .catch((e) => {
                console.log("ERROR: " + e)
            });

    }

    const handleBlur = (event) => {
        console.log(event.target)
        const { name, value, type } = event.target;
        setFormData((prevFormData) => ({ ...prevFormData, [name]: value }));

        if (type !== 'url') {
            checkValidity(event);
        }
    };

    const handleDisabled = (disabled) => {
        setDisabled(disabled);
    };

    const handleSelect = (event) => {

        let { name, value } = event.target;

        setFormData((prevFormData) => ({ ...prevFormData, [name]: value }))
    }

    // [a-z0-9\\-]{1,63}\.(onion|[a-z]{2,3})\/?
    // <ul className="grid mx-4 w-full gap-x-6 gap-y-4 grid-cols-2 justify-center">
    return (
        
            siteData && formData ?
            <PageContainer
            header={<PageHeader>{'Add new resource'}</PageHeader>}
            top={
                <HelpContainer styles="p-2 m-2">
                    <Paragraph styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">
                        {`We're glad to announce your next resource. Fill and submit this form, please...`}    
                    </Paragraph>
                </HelpContainer>
            }
            body={
            <form 
                onSubmit={(e) => handleSubmit(e)}
                className=""
            >
                
                {
                inputs && formData && errors &&
                <FormGroup label="about resource contents">
                    <ul className=" md:grid md:mx-4 md:gap-x-6 md:gap-y-4 md:grid-cols-2 md:justify-center">
                        <li className="flex flex-col" key={0}>
                            <InputText
                                input={{
                                    label:"URL",
                                    type: "url",
                                    name: "url",
                                    required: true,
                                    pattern: "https://[a-z0-9\-]{1,63}\.(onion|[a-z]{2,3})\/?.*",
                                    placeholder: "https://example.com/path-to-resource",
                                    titleTooltipRequired: "First must check if your URL already exists"                         
                                }}
                                focus={true}
                                value={formData.url.value}
                                handleInputValue={(e) => handleBlur(e)}
                                handleDisabled={handleDisabled}
                                disabled={null}
                            />
                        </li>
                
                        {inputs.map((input, i) => {
                            if(input.label !== 'email') {
                            return (
                                <li className="flex flex-col" key={i+1}>
                                    <InputText
                                        input={input} 
                                        label={input.label}
                                        name={input.label}
                                        value={formData[input.label].value}
                                        handleInputValue={(e) => handleBlur(e)}
                                        disabled={disabled}
                                        
                                    />
                                </li>
                            )
                            }
                        })}

                    </ul>
                </FormGroup>
                }
                {
                selects && formData && siteData &&
                <FormGroup label="about resource properties">
                <ul className="grid w-full gap-x-6 gap-y-4 grid-cols-2 lg:grid-cols-3 justify-center">
                {selects.map( (select, i) => {
                    return (
                        <li className="">
                        <SelectOption
                            key={i}
                            label={select}    
                            options={siteData[select]}
                            disabled={disabled}
                            selected={formData[select].value} 
                            handleSelected={handleSelect}
                        /> 
                        </li>
                            
                    )
                })}
                    <li className="flex justify-center " >
                        <div className="flex flex-row justify-center mt-6">
                            <Button disabled={disabled} handleClick={handleSubmit} type="submit" label="submit" styles=""/>
                        </div>
                    </li>
                </ul>
                </FormGroup>  
                }
                { inputs && formData && false &&
                    <FormGroup label="about you">
                        <ul className=" md:grid md:mx-4 md:gap-x-6 md:gap-y-4 md:grid-cols-2 md:justify-center">
                            <li className="flex flex-col" key={0}>
                                <InputText 
                                    input={inputs[inputs.length -1 ]} 
                                    label={inputs[inputs.length -1].label}
                                    value={formData['email'].value}
                                    handleInputValue={handleBlur}
                                    disabled={disabled}
                                    
                                />
                            </li>
                            <li className="flex flex-col " key={1}>
                                <div className="flex flex-row justify-center mt-6">
                                    <Button disabled={disabled} handleClick={handleSubmit} type="submit" label="submit" styles=""/>
                                </div>
                            </li>
                        </ul>
                    </FormGroup>
                }
                
                
            </form>
            }
        />
        :
        ''    
    );
}
