import React, { useState, useEffect, useContext, useRef } from "react";
import axios from "axios";
import FormGroup from "./FormGroup";
import ButtonClose from "../buttons/ButtonClose";
import ButtonPlain from "../buttons/ButtonPlain";
//import "../../App.scss";

export default function FormContainer (props) {
// sm:w-100 md:w-3/4 lg:w-1/2

    const handleSubmit = (e) => {
        e.preventDefault();
        props.handleSubmit(e);
        // enviar data body al padre 
    }

    /**
     * <div>
                <button
                    type="button"
                    onClick={props.handleView('remember')}
                    className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4"
                >
                    {`Remember password`}
                </button>

                <button
                    type="button"
                    onClick={() => props.handleView('register')}
                    className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4"
                >
                    {`Register form`}
                </button>
            </div>
     */
    return (
        <div className="fixed z-50 top-1/6 right-0 mx-2 rounded w-full md:w-1/2">
            <form onSubmit={handleSubmit}>
                <FormGroup label={props.label} styles="bg-indigo-300 rounded">
                    <ButtonClose handleClick={props.handleClose} />
                    <ul className="md:grid md:mx-4 md:gap-x-6 md:gap-y-4 md:grid-cols-2 md:justify-center">      
                        {props.children}
                    </ul>
                    <div className="flex justify-center mt-4">
                        <ButtonPlain styles="px-3 py-2 text-lg" label="submit" type="submit" handleClick={''}/>
                    </div>
                </FormGroup>
            </form>
            
        </div>
    )
}



