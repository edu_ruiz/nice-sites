import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function ItemContainer (props) {
// sm:w-100 md:w-3/4 lg:w-1/2
    return (
        <div className={`${props.styles}  pt-4 pb-2 rounded-lg bg-indigo-100`}>
            {props.children}
        </div>
    )
}



