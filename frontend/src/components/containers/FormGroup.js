import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function FormGroup (props) {
// sm:w-100 md:w-3/4 lg:w-1/2
    return (
        <div className={`${props.styles}  px-4 pt-6 py-6 mt-10 border-2 rounded-md `}>
            <h5 className="font-semibold text-gray-400 mb-4 uppercase text-center">{props.label}</h5>
            {props.children}
        </div>
    )
}



