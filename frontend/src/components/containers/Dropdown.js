import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function Dropdown (props) {
// sm:w-100 md:w-3/4 lg:w-1/2
    return (
        <div className={`${props.styles} z-30`} id={props.dropdownId}>
            <ul className={`${props.listStyles} flex font-medium p-4 md:p-6 md:mt-8 md:mb-5 border border-gray-100 rounded-lg bg-gray-50 justify-center md:mt-4 md:border-0 md:bg-transparent dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700`}
                aria-labelledby={props.ariaLabel}
            >
                {props.children}
            </ul>
        </div>
    )
}