import React, { useState, useEffect, useContext, useRef } from "react";
//import "../../App.scss";

export default function TextContainer (props) {
// sm:w-4/5 md:w-3/4 lg:w-1/2 rounded-lg
    return (
        <div className={`${props.styles}  bg-indigo-200/20`}>
            {props.children}
        </div>
    )
}



