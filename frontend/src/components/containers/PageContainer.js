import React, { useState, useEffect, useContext, useRef } from "react";

import NavBottom from "../nav/NavBottom";
//import "../../App.scss";

export default function PageContainer (props) {
// sm:w-100 md:w-3/4 lg:w-1/2

// !! PROBAR si es posible adaptar el menú Nav por props


    return (
        <div className={`${props.styles} mx-0 md:mx-4 lg:mx-10 2xl:mx-20 space-y-10 bg-white`}>
            {
                props.header &&
                <div>
                    {props.header}
                </div>
            }
            {
                props.top &&
                <div>
                    {props.top}
                </div>
            }
            {
                props.body &&
                <div>
                    {props.body}
                </div>
            }
            {
                props.bottom &&
                <div>
                    {props.bottom}
                </div>
            }
            {   true && 
                <NavBottom navAux={props.navAux} />
            }
            
        </div>
    )
}



