import { useEffect } from 'react';
import { TbFolder, TbFolderOpen } from "react-icons/tb";

export default function ResourcesTree (props) {


    useEffect(() => {
        //console.log('mounted');
        //history.push("/resources/tree"); // + objeto de estado
        //console.log(history.location);
        //history.push("/resources/tree");
        return () =>''  // console.log('unmounting...');
      }, [])  // <-- add this empty array here
     
    // usar react-tree,
    // generar los nodos dinámicamente, a partir de props:
    // parámetros del filtro y listado de resultados
    // Al hacer Search, podríamos navegar desde Home hasta Resources con los resultados,
    // y desde aquí elegir una vista u otra, sin hacer otra llamada al servidor

    return(

        <ul className="">
            NODE DATA
        {
            props.treeData &&

            props.treeData.map( (node, i) => {
                return (
                    <li key={i} className="flex items-center" onClick={props.handleClick}>
                        {
                            node.isOpen ?
                            <TbFolderOpen />
                            :
                            <TbFolder />
                        }
                        <h6>{node.name}</h6>
                    </li>
                )
            })
        }
        </ul>
        
        
    )
}