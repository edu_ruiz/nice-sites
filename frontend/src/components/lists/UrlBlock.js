import { useState, useEffect } from "react";
import axios from "axios";
import HrefLink from "../elements/HrefLink";
import ButtonPlain from "../buttons/ButtonPlain";

export default function UrlBlock (props) {
    const [viewEmbedded, setViewEmbedded] = useState();
    const [result, setResult] = useState();

    
    // props.url y props.textInput
    // el componente accederá a virusTotal y mostrará confirmación de enlace seguro
    // si no es seguro, el href se autobloquea

    // Usar NavLink ? 
    // onClick, no navega desde navegador, sino desde scraper o frame ?
    // A cont., mostrar form: 
    // 1.It worked as expected?
    // 2. Contents are as described?
    // 3. Properties are as described?

    // 1. 'no': disable resource
    // 2. 'no': muestra form update contents (intentar scraper?)
    // 3. 'no': form update properties

    // acknownledge === true, en cada bloque, cierra form y muestra label 'updated'
    // crear component FormContents y formProperties

    const handleUrlScan = (e) => {
        //console.log(props.url);
        e.preventDefault();
        
        axios
            .get("/resources/scan", {params: { url: props.url }})
            .then( (res) => {
                console.log(res);
                const {data} = res.data;
                setResult(data)
            })
            .catch( (e) => {
                console.log(e)
            })
    }
    // Usar el propio resource para obtener los keys por campo (contents/properties)
    // y cargar desde inputs y selects /forms/resource un formulario

    const handleUrlClick = (e) => {
        e.preventDefault();
        // acciones para mostrar frame embedded
        props.handleUrlClick(props.url);
        setViewEmbedded(true);
    }

// virustotal 

    useEffect(() => {
        setViewEmbedded(false);
        return () =>''  // console.log('unmounting...');
      }, [])  // <-- add this empty array here

    
     

    return(
        <div className="flex">

            { props && 
                <div className="grid md:flex justify-center items-center space-y-2 md:space-x-3">
                    
                    <HrefLink url={props.url} className="" handleClick={handleUrlClick}>
                        {props.children}
                    </HrefLink>
                    <ButtonPlain styles="me-4" label="URL scan" handleClick={handleUrlScan} />
                </div>
            }
            {
                result && result.length > 0 &&
                <div className="text-red-500 p-5 m-5">
                    Warning, this could be an unsafe link!
                </div>
            }
            { result && result.length === 0 &&
                <div className="text-green-700 p-5 m-5">
                    It seems safe! No bad reports yet
                </div>
            }
            {
                viewEmbedded &&
                <div className="fixed top-2/5 bg-white border border-2 border-cyan-700 rounded-lg z-30">
                    <button onClick={() => setViewEmbedded(false)}>Close</button>
                    <iframe className="" src={props.url}></iframe>
                    
                </div>
            }
        </div>
        
    )
}