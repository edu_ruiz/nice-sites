
function MatchSuggests(props) {

    let handleSelected = (e) => {
        // Hide <h> label when focus input
        props.handleSelected(e.title)
        
    };

    const filtered = props.titles.filter((el) => {
        
        if (props.input === '') {
            return '';
        }
        else {
            
           return el.title.toLowerCase().match(RegExp('.*' + props.input.toLowerCase() + '.*$', 'i') )          
        }
    })
    
    return (      
        <ul className={"bg-light"}>
            {filtered.map((item, i) => (
                <li
                onClick={(e) => handleSelected(item)}
                className="list-group-item cursor-pointer" 
                key={i}>{item.title}</li>
            ))}
        </ul>
    )
}

export default MatchSuggests;