
export default function DescriptionItem (props) {

    return (
        <div className="flex flex-col py-1">
            <dt className="mb-1 text-gray-500 md:text-base dark:text-gray-400">{props.label}</dt>
            <dd className="">
                {props.children}
            </dd>
        </div>
    )
}



