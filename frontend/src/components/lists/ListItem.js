import { useState, useEffect, useContext, useRef } from "react";
import { NavLink, useNavigate } from "react-router-dom";

import { ImNewspaper } from "react-icons/im";
import { PiPlanet } from "react-icons/pi";
import { MdOutlineChildCare, MdOutlineEdit } from "react-icons/md";
import { IoIosLink, IoMdHeart, IoMdHeartEmpty } from "react-icons/io";
import { TbAlertTriangle } from "react-icons/tb";

import axios from "axios";
import {WishListContext} from "../context/WishListContext";
import {HighlightContext} from "../context/HighlightContext";

import {SearchContext} from "../context/SearchContext";
import UserContext from "../context/UserContext";
import { floor } from "mathjs";
import HighlightMatches from "../helpers/HighlightMatches";
import UrlBlock from "./UrlBlock";
import Blockquote from "../elements/Blockquote";
import DescriptionList from "./DescriptionList";
import DescriptionItem from "./DescriptionItem";
import Tooltip from "../elements/Tooltip";
import ItemContainer from "../containers/ItemContainer";
import TextContainer from "../containers/TextContainer";
import IconOrLabel from "../elements/IconOrLabel";
import KeyValuePair from "../elements/KeyValuePair";
import InlineList from "./InlineList";
import HelpContainer from "../containers/HelpContainer";


export default function ListItem (props) {
  const highlightContext = useContext(HighlightContext);
  const searchData = useContext(SearchContext);
  const user = useContext(UserContext);
  const [isWish, setIsWish] = useState();
  const wishListRef = useRef();
  const [addedMsg, setAddedMsg] = useState('');
  const [deletedMsg, setDeletedMsg] = useState('');
  const [blockedMsg, setBlockedMsg] = useState('');
  const wishList = useContext(WishListContext);
  const [checked, setChecked] = useState(false);
  const [search, setSearch] = useState(searchData);
    let navigate = useNavigate();
    const [activeIndex, setActiveIndex] = useState(0);
    const [isOver, setIsOver] = useState(false) 
    //const [item, setItem] = useState(props.item);
  const [i,setI] = useState(props.index);
  const [views, setViews] = useState({
    detail: false,
    properties: true,
    contents: true,
    feedback: true,
    timestamps: true,
    signature: false,
    stage: false,
    text: true,
    wishButton: true
  });
  //setViews();



const handleDelete = (resourceId) => {

  const queryObj = {};

  queryObj['userId'] = user._id;
  queryObj['resourceId'] = resourceId;

  axios
    .delete(`/resources`, queryObj )
    .then( (res) => {
      setDeletedMsg("deleted");
      
    })
    .catch( (e) => {
      setDeletedMsg("got error");
    })
}

const handleBlockUser = (blockUserId) => {

  const queryObj = {};

  queryObj['userId'] = user._id;
  queryObj['blockUserId'] = blockUserId;

  axios
    .post(`/blacklists/user`, queryObj )
    .then( (res) => {
      setBlockedMsg("user blocked");
    })
    .catch( (e) => {
      setBlockedMsg("got error");
    })
}

const handleAddWish = (id) => {

  if( ! wishList.includes(id) ) {
   
    wishList.push(id);
    //console.log(wishList)
  }  // */
}

const handleDeleteWish = (id) => {

  if( wishList.includes(id) ) {
/**
 * setArtists(
  artists.filter(a => a.id !== artist.id)
);
 */
    let index = wishList.indexOf(id)
    wishList.splice(index, 1);
    //console.log(wishList);
  }  // */
} // */

  const handleActive = (index) => {
    //  props.handleActive(index)
  }

  const handleDoubleClick = (index, item, e) => {
    
    props.handleDoubleClick(index,item,e)
  }

  const handleMouseOver = (e) => {
    setIsOver(!isOver);
  }

  // pasar a useEffect Once
  const reduceAmount = (amount) => {
    if( amount / 1000000 >= 1) {
      return ''+ floor(amount/1000000) + '.' + floor(amount%1000000/100000) + 'M'
    }
    else if (amount / 1000 >= 1 ) {
      return ''+ floor(amount/1000) + 'K'
    }
    else return amount;
  }
// a useEffect once
  const sliceDateTime = (dateStr) => {
    const date =  dateStr.slice(0,-14)

    return date;
  }

  // activa botón oculto
  const verifClass = (str, index) => {
    if ( str ) {
      return ""
    }
    else {
      if ( isOver ) {
        return "btn-hidden"
      }
      else if ( activeIndex !== index) {
        return "btn-hide"
      }
    }

  }
  // paired, control hide btn styles
  const verifLabel = (str, index) => {
    if ( str ) {
      return <span><svg class="flex-shrink-0 w-3.5 h-3.5 text-green-500 dark:text-green-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 16 12">
      <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M1 5.917 5.724 10.5 15 1.5"/>
   </svg>"Verified</span>
    }
    else {
      if ( isOver || activeIndex === index) {
        return "Could verify?"
      }
      else return "Untrusted"
    }
  }


  const handleWishListChange = (id) => {
//console.log(id)
    if (! checked ) {
      handleAddWish(id);
    }
    else {
      handleDeleteWish(id);
    }
    
    //setIsWish(!isWish)
    setChecked(!checked)
    console.log(wishList)
  }

  const wasChecked = (id) => {
    if ( wishList.includes(id)) {
      //console.log(id)
      return true;
    }
    else {
      return false;
    }
  }

  const handleUrlClick = () => {

  }

  useEffect(() => {
    //console.log("List Item mounted");
    //console.log("props Item: " + JSON.stringify(search))
    //console.log(user);
    if (wasChecked(props.item._id)) {
      setChecked(true);
    }

    return () => '';//console.log("List Item unmounting");
}, []);

useEffect( () => {

  switch( props.viewType ) {
      
    case 'tiny': setViews({
      detail: false,
      properties: true,
      contents: false,
      feedback: false,
      timestamps: false,
      signature: false,
      stage: false,
      text: false,
      wishButton: true,
      scrollable: true
    }) ; break;
    case 'medium': setViews({
      detail: false,
      properties: true,
      contents: true,
      feedback: true,
      timestamps: true,
      signature: false,
      stage: false,
      text: false,
      wishButton: true,
      scrollable: false
    }) ; break;
    case 'full': setViews({
      detail: false,
      properties: true,
      contents: true,
      feedback: true,
      timestamps: true,
      signature: true,
      stage: true,
      text: true,
      wishButton: true,
      scrollable: false
    }) ; break;
    default: setViews({
      detail: false,
      properties: true,
      contents: true,
      feedback: true,
      timestamps: true,
      signature: false,
      stage: false,
      text: true,
      wishButton: true,
      scrollable: true
    }) ;
  }
}, [props.viewType])

useEffect(() => {
  //console.log("List Item mounted");
  if (checked) {
    wishListRef.current.innerHTML = "Added";
  }
  else {
    wishListRef.current.innerHTML = "Deleted";
  }
  
  let timer = setTimeout(()=>{
    wishListRef.current.innerHTML = '' ;
  }, 1500);
  return () => {clearTimeout(timer) ; props.handleWishChange(); } //console.log("List Item unmounting");}
}, [checked]);//handleWishListChange]);
    
// <i className={ verifClass(item.timestamps.verifiedAt, i) } onMouseOverCapture={handleMouseOver} onMouseOutCapture={handleMouseOver}>
//{ verifLabel(item.timestamps.verifiedAt, i) }</i>
// IDEA: Solo mostrar el texto cuando se haya seleccionado la búsqueda por texto, o la vista se active desde menú o profile
  const renderList = (item) => {
    
    // grid grid-cols-1 md:gap-6 md:grid-cols-2
    return (
    
      <li
        className="w-full mb-4 border border-cyan-700/40 rounded-lg border-2"
        key={i}
        onClick={() => {
            handleActive(i);
        }}
        onDoubleClick={ (e) => handleDoubleClick(i, item, e) }
      >
        <ItemContainer styles="">
          <label htmlFor="title" className="mx-5 mb-1 text-gray-500 md:text-base dark:text-gray-400">
            {`Title`}
          </label>
          <h4 name="title" className="mx-5 mb-2 flex-row font-bold text-lg md:text-2xl lg:text-4xl">
          {search.textInput ? <HighlightMatches text={item.contents.title} /> : item.contents.title }
          </h4> 
 
          <div className="flex items-center justify-around mx-5 mb-5 mt-5">
            <div className="flex-row items-center justify-center">
              
            { views.properties &&  
              <span className="space-x-5 m-5">
                <KeyValuePair
                  keyStyles=""
                  valueStyles="italic"
                  label={"section"}
                  value={item.properties.section}
                >
                  <ImNewspaper className="text-lg" />
                </KeyValuePair>

                <KeyValuePair
                  keyStyles=""
                  valueStyles="italic"
                  label={"source"}
                  value={item.properties.source}
                >
                  <PiPlanet className="text-lg" />
                </KeyValuePair>
              </span>
            }
            </div>
            <div className="flex-row items-center space-x-5 justify-end">
              <span className="space-x-5 m-5">
              <KeyValuePair
              keyStyles=""
              valueStyles="italic"
              label={"parental"}
              value={item.properties.parental}
            >
              <MdOutlineChildCare className="text-lg" />
            </KeyValuePair>
            { ! item.timestamps.verifiedAt ?
              <NavLink to={`/verify/${item._id}`} state={{resource: item}} className="">
                <span className="inline-flex items-center space-x-1 p-3">
                  <TbAlertTriangle />
                  <i className="underline text-slate-500 italic">Could Verify?</i>
                </span>   
              </NavLink>
              : 
              <KeyValuePair
                keyStyles=""
                valueStyles="italic"
                label={"trust"}
                value={"Verified"}
              >
                <IoIosLink className="text-lg" />
              </KeyValuePair>
            }
            </span>
            </div>
          </div>
        
        <div className="bg-white/40 py-3 pl-5 border-2">
        <DescriptionList>
          <DescriptionItem label="subject">
            {search.textInput ? <HighlightMatches text={item.contents.subject} /> : item.contents.subject }   
          </DescriptionItem>
          <DescriptionItem label="keywords">
            {item.contents.keywords.toString()}
          </DescriptionItem>
          <DescriptionItem label="URL">
            <UrlBlock
              dataTooltipTrigger="hover"
              dataTooltipTarget="tooltip-full-url" 
              styles="" 
              url={item.url} 
              id={item._id} 
              handleUrlClick={handleUrlClick}
            >
              {search.textInput ? <HighlightMatches styles={""} text={item.url} /> : item.url }
            </UrlBlock>
            <Tooltip id="tooltip-full-url">
              <span className="underline">{props.url}</span>
            </Tooltip>
          </DescriptionItem>
          <DescriptionItem label="updated:">
            {sliceDateTime(item.timestamps.updatedAt)}
          </DescriptionItem>
        </DescriptionList>
        </div>
        { true &&
          <div>
            <dt className="pl-5 mt-2 text-gray-500 md:text-base dark:text-gray-400">Text sample</dt>
            <TextContainer styles="flex flex-row mt-2 pt-3 pb-1 border-t border-b border-2">
                {search.textInput ? 
                  <Blockquote><HighlightMatches text={`"${item.contents.text}"`} /></Blockquote>
                : 
                
                  <Blockquote styles="leading-relaxed mt-0 my-2 pl-5 mr-2 lg:text-xl text-blue-900 sm:line-clamp-3 first-line: first-line:capitalize first-line:tracking-wider first-letter:text-2xl first-letter:font-semibold first-letter:text-blue-700/90 ">{`"${item.contents.text}"`}</Blockquote> 
                }
             
            </TextContainer>
          </div>  
        }
        <div className="flex items-center justify-around mx-5 mb-5 mt-5">
            <div className="flex-row items-center justify-center">  
              
              { views.contents &&
              <span className="space-x-5 m-5">
                <KeyValuePair
                  keyStyles=""
                  valueStyles="italic"
                  label={"contents"}
                  value={item.feedback.contentsTag}
                >
                  <ImNewspaper className="text-lg" />
                </KeyValuePair>

                <KeyValuePair
                keyStyles=""
                valueStyles="italic"
                label={"UX"}
                value={item.feedback.uxTag}
                >
                <ImNewspaper className="text-lg" />
                </KeyValuePair>
              </span>
              }
              {
                views.feedback &&
                <span className="space-x-5 m-5">
                <KeyValuePair
                  keyStyles=""
                  valueStyles="italic"
                  label={"quality"}
                  value={item.feedback.quality}
                >
                  <ImNewspaper className="text-lg" />
                </KeyValuePair>

                <KeyValuePair
                  keyStyles=""
                  valueStyles="italic"
                  label={"score"}
                  value={item.feedback.score}
                >
                  <ImNewspaper className="text-lg" />
                </KeyValuePair>

                <KeyValuePair
                  keyStyles=""
                  valueStyles="italic"
                  label={"ratings"}
                  value={reduceAmount(item.feedback.ratings)}
                >
                  <ImNewspaper className="text-lg" />
                </KeyValuePair>
                </span>
              }
              
            </div>
        </div>
          <div className="flex items-center justify-around mt-4 pb-1 px-5">
            {
              user && user._id &&
              <InlineList styles="space-x-3">
                <NavLink 
                  to={`/edit/${item._id}`}
                  state={{ resource : item }}
                  className="text-indigo-700"
                >
                  <IconOrLabel styles="underline hover:text-blue-500" label="edit">
                    <MdOutlineEdit className="text-xl" />
                  </IconOrLabel>
                </NavLink>
                <NavLink to={`/rate/${item._id}`} className="text-indigo-700 underline hover:text-blue-500"
                  state={{ 'url': item.url, 'title': item.contents.title, 'resourceId': item._id }}
                >
                  <IconOrLabel styles="underline hover:text-blue-500" label="rate">
                    <MdOutlineEdit className="text-xl" />
                  </IconOrLabel>
                </NavLink>
                {
                  user && user.admin &&
                  <button 
                    type="button" 
                    className="text-indigo-700 underline hover:text-blue-500"
                  >
                    <IconOrLabel handleClick={() => handleDelete(item._id)} styles="underline hover:text-blue-500" label="delete">
                      <MdOutlineEdit className="text-xl" />
                    </IconOrLabel>
                  </button>
                }
                <span className="text-red-500 text-sm italic">{deletedMsg}</span> 
                { user  && user.admin &&
                  <button 
                    type="button"
                    className="text-indigo-700 underline hover:text-blue-500"
                  >
                    <IconOrLabel handleClick={() => handleBlockUser(item.signature.userId)} styles="underline hover:text-blue-500" label="block user">
                      <MdOutlineEdit className="text-xl" />
                    </IconOrLabel>
                  </button>    
                }
                <span className="text-red-500 text-sm italic">{blockedMsg}</span>
              </InlineList>
            }
          </div>
          <div className="flex items-center justify-around mt-4 pb-1 px-5">
               
              <span 
                className="inline-flex items-center space-x-1"
                name="wish"
                onClick={() => handleWishListChange(item._id)}
              >
                <NavLink to="/export">
                  <label 
                    className="text-slate-400 text-lg italic pb-1 underline hover:text-blue-500 underline-offset-4" 
                    htmlFor="wish"
                  >
                    toWishlist
                  </label>
                </NavLink>
                {
                  checked ?
                  <IoMdHeart className="text-red-500 text-3xl" />
                  :
                  <IoMdHeartEmpty className="text-3xl" />
                }
                <span className="relative italic w-10" ref={wishListRef}>{addedMsg}</span> 
              </span>
              <KeyValuePair
                keyStyles="text-lg"
                keyColors="font-light text-slate-500 italic"
                valueStyles="italic"
                valueColors="bg-transparent text-slate-500 border-slate-300"
                label={"last update"}
                value={sliceDateTime(item.timestamps.updatedAt)}
              >
                <ImNewspaper className="text-lg" />
              </KeyValuePair>  
            <span 
              className=" bg-white px-3 text-slate-600 border border-indigo-400 rounded-xl italic"
            >
              {`#${i + 1}`}
            </span>
          </div>    
        </ItemContainer>
      </li>    
    );
  }

  return (
    <>
    { 
        props.item
    ?
        renderList(props.item)
    :
        <div className={ "alert" + (props.error ?  "alert-danger": "alert-primary")} role="alert">
        { props.error ? props.error : 'No link matches' }
      </div>
    }
    </>
  )
}