import { useState, useEffect, useContext, useRef } from "react";
import { NavLink, useNavigate, useLocation } from "react-router-dom";
import ListItem from "./ListItem";

import { WishListContext } from "../context/WishListContext";

export default function ResourcesList (props) {

  const wishList = useContext(WishListContext);
    let location = useLocation();
    let navigate = useNavigate();
    const [activeIndex, setActiveIndex] = useState(0);
    const [isOver, setIsOver] = useState(false);
    //const [isViewText, setIsViewText] = useState(props.viewText);
    const resourcesRef = useRef();
    const countRef = useRef();
    

  const handleActive = (index) => {
    setActiveIndex(index);
    // ADD Float SideBar showing Actions for the item
    //alert( `Item ${index}!`);
  }
  
  

  const handleDoubleClick = (index, item, e) => {
    setActiveIndex(index);
    //history.push(`/resources/${item._id}`)
    navigate(`/resources/${item._id}`, 
        {
            'state': { 
                'index': index,
                'resource' : item,
                'path': location.pathname,
                'viewText': true
            }
        }
    )
  }

  const handleWishChange = () => {
    /*if(wishList){
      if(countRef)
      countRef.current.innerHTML = wishList.length;
    }*/

    // aplicar efecto: si la lista es type 'editable', 
    // cuando el evento tiene valor unchecked, 
    // eliminar el elemento de la lista

  }

  useEffect(() => {
    // Obtener ID por contexto, será el de la lista pre-cargada, desde Main o Search layout
    
    return () => '';
}, []);

  
/**
 * 
 * Rutas a wishes populadas
 * Navega to="/wishes/:id", :id -> _id de 'wishes',
 * Backend entrega objeto populated en esa ruta
 * se ofrece la actual lista cargada en el contexto, sus items están completos
 * CartPage ofrece opción de saveChanges() y nav-link Export, si hay error mantiene el contexto
 * En /user/:id/wishes se obtiene listado de las propias
 * En /wishes/open se obtiene listado de 'open', ordenar las que tengan participación de User primero
 * En /wishes se obtiene 'public'|'open',
 * En /wishes/public se obtiene 'public'
 * En /wishes/:id/export se navega a ExportPage (petición de parsed file a server: json, csv, txt)
 * 
 * 
 * Al guardar lista, 
 * /wishes/:id PUT,  manejar respuesta
 * 
 */

  const renderList = (urls) => {
    return (
      <div className="py-5 bg-gray-100">
        <NavLink to="/" className="flex justify-center text-lg lg:text-2xl text-indigo-700 underline">{`Cart(`}<span ref={countRef}></span>{`)`}</NavLink>
        <ul ref={resourcesRef} className={`mt-3 px-0 space-y-10 md:mt-5 md:px-5 lg:mt-7` + ( props.scrollable ? '' : '' )}>
          { 
          urls.map((item, i) => (
            <ListItem 
              key={i} 
              index={i} 
              item={item} 
              handleClick={handleActive} 
              handleDoubleClick={handleDoubleClick}
              handleActive={handleActive}
              viewType={props.viewType}
              handleWishChange={handleWishChange}
            />
          ))
          }
        </ul>
      </div>   
    );
  }

  return (
    <>
    { 
        props.urls && props.urls.length > 0 
    ?
        renderList(props.urls)
    :
    ''
    }
    </>
  )
}
