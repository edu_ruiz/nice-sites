

export default function InlineList (props) {

    return (       
        <div className="text-center">
            <ul className={`${props.styles} flex inline-flex items-center text-gray-900 dark:text-white`}>
                {props.children}
            </ul> 
        </div>
              
    )
}