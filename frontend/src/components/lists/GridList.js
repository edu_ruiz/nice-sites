import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import SelectOption from "../forms/SelectOption";
import { SiteDataContext } from "../context/SiteDataContext";


//import "../../App.scss";

export default function GridList (props) {
    const navigate = useNavigate();
    const siteData = useContext(SiteDataContext);
    const [parental, setParental] = useState();
    const [subject, setSubject] = useState();
    const [level, setLevel] = useState();

    const handleParental = (e) => {
        setParental(e.target.value);
    }

    const handleSubject = (e) => {
        setSubject(e.target.value);
    }

    const handleLevel = (e) => {
        setLevel(e.target.value);
    }

    const handleClick = (e, name) => {
        e.preventDefault();
        //console.log(name)
        //let {name} = e.target;

        //name = 'none';

        //console.log(e.target.name)

        if( name === "parental" ) {
            if( parental ) {
                showResults(name, parental);
            } 
        }
        else if( name === "education" ) {
            if( subject && level ) {
                showResults(name, subject, level);
            } 
        }
        else {
            showResults(name)
        }
        //props.handleClick(label, subject,level)
    }

    const showResults = (name, subject, level) => {
        axios
            .get("/resources/verified", {params: {section: name, subject: subject, level: level}})
            .then( (res) => {
                navigate("/search",{'state': {
                    'results' : res.data.data
                }});
            })
            .catch( (er) => {
                console.log(er);
            })
    }

    // Props: Label, handler y estilo final (color, size)
    useEffect( () =>{
        if(props) {
            //console.log(props.data)
        }
    },[]);

    return (
        <ul className={`grid ${props.styles} auto-rows-max w-full gap-6 md:grid-cols-3 `}>  
            {
                props.data &&
                props.data.map( (item, i) => {

                    return (
                        <li  className="w-full" key={i} onClick={(e) => handleClick(e, item)}>
           
            <input type="button"  id="react-option" className="hidden peer"/>
            <label htmlFor="react-option" className="inline-flex items-center justify-between w-full p-5 text-gray-500 bg-white border-2 border-gray-200 rounded-lg cursor-pointer dark:hover:text-gray-300 dark:border-gray-700 peer-checked:border-blue-600 hover:text-gray-600 dark:peer-checked:text-gray-300 peer-checked:text-gray-600 hover:bg-gray-50 dark:text-gray-400 dark:bg-gray-800 dark:hover:bg-gray-700">                           
                <div className="block" >
                
                    <svg className="mb-2 text-red-600 w-7 h-7" fill="currentColor" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M185.7 268.1h76.2l-38.1-91.6-38.1 91.6zM223.8 32L16 106.4l31.8 275.7 176 97.9 176-97.9 31.8-275.7zM354 373.8h-48.6l-26.2-65.4H168.6l-26.2 65.4H93.7L223.8 81.5z"/></svg>
                    <div name={item} className="w-full text-lg font-semibold text-gray-700">{item}</div>
                    <div className="w-full text-sm mb-3">{`Selected ${item} links`}</div>
                    {
                        item === 'parental' &&
                        <SelectOption
                            label={"age"}    
                            options={siteData.parental}
                            selected={parental} 
                            handleSelected={handleParental}
                        /> 
                    }
                    {
                        item === 'education' &&
                        <div className="flex space-x-3 items-center justify-center">
                            <SelectOption
                                divStyles=""
                                label={"subject"}    
                                options={siteData.subject}
                                selected={subject} 
                                handleSelected={handleSubject}
                            />
                            <SelectOption
                                label={"level"}    
                                options={siteData.education}
                                selected={subject} 
                                handleSelected={handleLevel}
                            />
                        </div>
                    }
                </div>
            </label>
      
            
        </li>
                    )
                })
            }
        </ul>
    )
}

