
export default function DescriptionList (props) {

    // En lugar de children, pasar el layout completo, reutilizable
    // props: 
    //props.main (destacar elemento principal)
    //props.body 
    // props.bottom ([] grupo a insertar al final)
    // tal vez se podría pasar solo la lista de configuración, y
    // modificar con clases tailwind, order,

    // estructura html
    // item main
    // items body
    // props.children : contenido extra o especial por funciones o formato
    // items bottom: siempre cierran la lista

    return (
        <dl className="text-gray-900 divide-y divide-gray-200 dark:text-white dark:divide-gray-700">
            {props.children}
        </dl>
    )
}



