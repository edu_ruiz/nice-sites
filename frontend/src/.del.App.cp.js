import React, {useState, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import axios from "axios";

import MainLayout from "./components/layouts/MainLayout";
import SearchLayout from './components/layouts/SearchLayout';
import HomePage from './components/pages/HomePage';
import SearchPage from './components/pages/SearchPage';
import ExportPage from "./components/pages/ExportPage";
import ResourcesLayout from './components/layouts/ResourcesLayout';
import Directory from './components/pages/DirectoryPage';
import ResourcePage from './components/pages/ResourcePage';
import FormsLayout from './components/layouts/FormsLayout';
import AddResource from "./components/forms/AddResource.fu";
import RateResource from "./components/forms/RateResource";
import EditResource from "./components/forms/EditResourceForm";

import UserContext from "./components/context/UserContext";
import {SiteDataContext} from "./components/context/SiteDataContext";
import {SearchContext} from "./components/context/SearchContext";
import {StepSearchContext} from "./components/context/StepSearchContext";
import {WishListContext} from "./components/context/WishListContext";
import SteppedForm from "./components/forms/SteppedForm";
import VerifyResource from "./components/forms/VerifyResource";
import VerifyPage from "./components/pages/VerifyPage";
import ResourceForm from "./components/forms/ResourceForm";

export default function App() {
  const [searchData, setSearchData] = useState({});
  const [siteData, setSiteData] = useState({});
  const [steppedSearch, setSteppedSearch] = useState({})
  const [wishList, setWishList] = useState([]);
//console.log(history.location)
  //const [siteData, setSiteData] = useState();
  const [user, setUser] = useState({});

  // Site data object: Field labels, categories...
  useEffect(() => {
    console.log("App mounted");
    axios.get("/index")
    .then((res) => {
      //console.log(res.data.data)
      setSiteData(res.data.data)

    })
    .catch( (e) => {
        console.log(e)
    }) // */
    // Esta acción irá ligada al login succesful
    axios.get("/users/65c151d6d19fbab1411b6b56")   //65c151d6d19fbab1411b6b69")
    .then((res) => {

        setUser(res.data.data)
        
        // SUSTITUIR!! obtener de siteData config
        // filtros importados desde user.profile
        const filterList = ['section','source', 'matchOption', 'searchBy', 'parental', 'quality'];
        setUser((prev) => ({ ...prev, ['filters']: filterList }));

    })
    .catch( (e) => {
      console.log(e.errors)
    }) // */

    axios.get(`/wishes/65cb9de81eddd3e6179d2ae4/list`)
    .then( (res) => {
      // INICIAR Mejor en carro vacío,
      // cargar solo a petición de usuario
      setWishList(res.data.data);
    })
    .catch( (e) => {
      console.log(e)
    })//*/
    
    const fieldList = ['section', 'source','suffix','extension','updated','quality','parental'];
    const stepObject = {};
    stepObject['fields'] = fieldList; // 'steppedFields':[], 'remainingFields': [0,1,2,3,4,5,6,7] };
    //stepObject.initFields = fieldList;
    //stepObject['stepped'] = { 'fields': ['sections', 'suffix','sources', 'extension'], 'values': ['news', 'org','blog','pdf']};
    stepObject['stepped'] = { 'fields': [], 'values': []};
    
    //stepObject['remaining'] = fieldList;//[0,1,2,3,4,5,6,7];
    stepObject['current'] = 0; // */
//console.log(stepObject)
    // guardar en siteData (categories) nuevos campos , hacer escalable,puede aumentar el número de steps
    setSteppedSearch(stepObject)

    // CONTINUAR rutas .../count   /mean  /level-mean para obtener número de votos

    // FALTA método para comprobar si user ya votó por ese recurso una vez en el mismo certificado

    // user by/65c151d6d19fbab1411b6b77
    axios.get(`/ratings/for/65d66076dfe502135da379ae/average`)
    .then( (res) => {

      console.log("ratings-for: " + JSON.stringify(res.data.data))
      
    })
    .catch( (e) => {
      console.log(e)
    })//*/

   /* axios.get(`/ratings/by/65c151d6d19fbab1411b6b71`)
    .then( (res) => {

      //console.log("ratings-by: " + JSON.stringify(res.data.data))
      
    })
    .catch( (e) => {
      console.log(e)
    })//*/

    return () => console.log('App unmounting...');
    
}, []); // <-- add this empty array here
  /*
 Falta

<Route path="users" element={<UsersLayout/>}>
            
</Route>
        // RESTO DE RUTAS
  */
  return (
    <UserContext.Provider value={user}>
    <SiteDataContext.Provider value={siteData}>
    <SearchContext.Provider value={searchData}>  
    <WishListContext.Provider value={wishList}>
    <StepSearchContext.Provider value={steppedSearch}>

      <Routes>
        <Route path="/" element={<MainLayout />}>

          <Route element={<SearchLayout/>}>
            <Route index element={<HomePage/>} />          
            <Route path="search" element={<SearchPage state= {
                {'results' : [],
                'formValues': {}}
            } />} />
          </Route>
          <Route element={<FormsLayout/>}>
            <Route path="add" element={<ResourceForm/>} />
            <Route path="verify" element={<VerifyPage/>} />
            <Route path="verify/:id" element={<VerifyResource />} />
            <Route path="edit" element={<EditResource/>} />
            <Route path="rate/:id" element={<RateResource/>} />
            <Route path="export" element={<ExportPage/>} />
          </Route>
          <Route path="resources" element={<ResourcesLayout/>}>
            <Route index element={<Directory/>} />
            <Route path=":id" element={<ResourcePage/>} />
          </Route>

        </Route>
      </Routes> 
    </StepSearchContext.Provider>  
    </WishListContext.Provider>
    </SearchContext.Provider>    
    </SiteDataContext.Provider>
    </UserContext.Provider>
  ); 
}
